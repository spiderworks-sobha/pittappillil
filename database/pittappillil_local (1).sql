-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 11, 2019 at 04:46 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pittappillil_local`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
CREATE TABLE IF NOT EXISTS `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `mobile_code` int(11) NOT NULL,
  `mobile_number` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `landmark` text,
  `city` varchar(200) NOT NULL,
  `state` varchar(200) NOT NULL,
  `country_id` int(11) NOT NULL,
  `pincode` int(11) NOT NULL,
  `type` char(1) NOT NULL COMMENT 'Home or Work',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `user_id`, `full_name`, `mobile_code`, `mobile_number`, `address`, `landmark`, `city`, `state`, `country_id`, `pincode`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Akhil', 0, '9496607954', 'aaaaaaaaaaaa', 'Near vattapinni temple', 'Kerala', 'Karnataka', 0, 680007, '', '2019-05-15 01:58:03', '2019-05-15 01:58:03', NULL),
(2, 1, 'Akhil', 0, '9496607954', 'aaaaaaaaaaaa', 'Near vattapinni temple', 'Kerala', 'Jharkhand', 0, 680007, '', '2019-05-15 03:01:41', '2019-05-15 03:01:41', NULL),
(3, 1, 'Akhil', 0, '9496607954', 'aaaaaaaaaaaa', 'Near vattapinni temple', 'Thrissur', 'Lakshadweep', 0, 680007, '', '2019-05-15 03:57:01', '2019-05-15 03:57:01', NULL),
(4, 1, 'Akhil', 0, '9496607954', 'aaaaaaaaaaaa', 'Near vattapinni temple', 'Kerala', 'Kerala', 0, 680007, '', '2019-05-15 04:41:45', '2019-05-15 04:41:45', NULL),
(5, 1, 'Akhil', 0, '9496607954', 'aaaaaaaaaaaa', 'Near vattapinni temple', 'Kerala', 'Maharashtra', 0, 680007, '', '2019-05-15 07:07:02', '2019-05-15 07:07:02', NULL),
(6, 1, 'Akhil', 0, '9496607954', 'aaaaaaaaaaaa', 'Near vattapinni temple', 'Thrissur', 'Jammu and Kashmir', 0, 680007, '', '2019-05-15 07:07:43', '2019-05-15 07:07:43', NULL),
(7, 6, 'Akhil Joy', 0, '9496607954', 'MANGALAM house nedupuzha p.o.', 'Vattapinni', 'Thrissur', 'Kerala', 0, 680007, '', '2019-05-17 06:52:25', '2019-05-17 06:52:25', NULL),
(8, 1, 'Akhil', 0, '9496607954', 'aaaaaaaaaaaa', 'Near vattapinni temple', 'Kerala', 'Lakshadweep', 0, 680007, '', '2019-05-18 05:29:54', '2019-05-18 05:29:54', NULL),
(9, 1, 'Sudeep s', 0, '9562543210', 'Spiderworks technologies pvt ltd, 219 mavelipuram zone 2', 'Near Adam star', 'Kakkanadu', 'Kerala', 0, 682030, '', '2019-05-18 05:33:09', '2019-05-18 05:33:09', NULL),
(10, 1, 'Akhil', 0, '9496607954', 'Spiderworks technologies pvt ltd, 219 mavelipuram zone 2', 'Near Adam star', 'Kakkanadu', 'Kerala', 0, 682030, '', '2019-05-18 05:33:54', '2019-05-18 05:33:54', NULL),
(11, 1, 'akhil', 0, '9496607954', 'dfsf', 'sdfdsfsf', 'dsfdsfds', 'Kerala', 0, 53453543, '', '2019-08-12 11:01:59', '2019-08-12 11:01:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_pages`
--

DROP TABLE IF EXISTS `admin_pages`;
CREATE TABLE IF NOT EXISTS `admin_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `slug` varchar(250) NOT NULL,
  `permission` varchar(250) NOT NULL,
  `target` varchar(10) DEFAULT NULL,
  `icon` varchar(50) NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `created_by` bigint(20) NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_pages`
--

INSERT INTO `admin_pages` (`id`, `title`, `slug`, `permission`, `target`, `icon`, `parent`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Dashboard', 'admin/dashboard', 'dashboard', NULL, 'fa fa-dashboard', 0, 1, 1, '2019-08-02 11:27:10', '2019-08-02 11:27:10'),
(2, 'User', '#', 'user-management', NULL, 'fa fa-user', 0, 1, 1, '2019-08-02 11:27:10', '2019-08-02 11:27:10'),
(3, 'Add User', 'admin/users/create', 'user-add', NULL, 'fa fa-user', 2, 1, 1, '2019-08-02 11:27:10', '2019-08-02 11:27:10'),
(4, 'Manage User', 'admin/users', 'user-management', NULL, 'fa fa-user', 2, 1, 1, '2019-08-02 11:27:10', '2019-08-02 11:27:10'),
(5, 'Settings', '#', 'settings', NULL, 'fa fa-dashboard', 0, 1, 1, '2019-08-02 11:27:10', '2019-08-02 11:27:10'),
(6, 'General Settings', 'admin/settings', 'settings', NULL, 'fa fa-dashboard', 5, 1, 1, '2019-08-02 11:27:10', '2019-08-02 11:27:10'),
(7, 'Image Settings', 'admin/image-settings', 'image-settings', NULL, 'fa fa-dashboard', 5, 1, 1, '2019-08-02 11:27:10', '2019-08-02 11:27:10'),
(8, 'Menu Settings', 'admin/menu', 'menu-settings', NULL, 'fa fa-dashboard', 5, 1, 1, '2019-08-02 11:27:10', '2019-08-02 11:27:10'),
(10, 'Manage Category', 'admin/category', 'category', NULL, 'fa fa-dashboard', 9, 1, 1, '2019-08-02 11:27:10', '2019-08-02 11:27:10'),
(15, 'Manage Attribute Groups', 'admin/category/attribute/groups', 'manage_attribute_groups', NULL, 'fa fa-dashboard', 9, 1, 1, '2019-08-02 11:27:10', '2019-08-02 11:27:10'),
(12, 'Products', '#', 'products', NULL, 'fa fa-dashboard', 0, 1, 1, '2019-08-02 11:27:10', '2019-08-02 11:27:10'),
(13, 'Manage Products', 'admin/products', 'products', NULL, 'fa fa-dashboard', 12, 1, 1, '2019-08-02 11:27:10', '2019-08-02 11:27:10'),
(14, 'Create New Product', 'admin/products/create', 'products_add', NULL, 'fa fa-dashboard', 12, 1, 1, '2019-08-02 11:27:10', '2019-08-02 11:27:10'),
(16, 'Offers', 'admin/offers', 'offers', NULL, 'fa fa-dashboard', 0, 1, 1, '2019-08-02 11:27:10', '2019-08-02 11:27:10'),
(17, 'Category', '#', 'category-management', NULL, '', 0, 1, 1, '2019-08-13 00:00:00', '2019-08-13 10:55:45'),
(18, 'Create category', 'admin/category/create', 'category-management', NULL, '', 17, 1, 1, '2019-08-13 00:00:00', '2019-08-13 11:29:31'),
(19, 'Manage categories', 'admin/category/', 'category-management', NULL, '', 17, 1, 1, '2019-08-13 00:00:00', '2019-08-13 11:29:31'),
(20, 'Vendor', '#', 'vendor-management', NULL, '', 0, 1, 1, '2019-08-13 00:00:00', '2019-08-13 11:44:40'),
(21, 'Create vendor', 'admin/vendor/create', 'vendor-management', NULL, '', 20, 1, 1, '2019-08-13 00:00:00', '2019-08-13 11:47:16'),
(22, 'Manage vendors', 'admin/vendor/', 'vendor-management', NULL, '', 20, 1, 1, '2019-08-13 00:00:00', '2019-08-13 11:47:16'),
(23, 'Brand', '#', 'brand-management', NULL, '', 0, 1, 1, '2019-08-13 00:00:00', '2019-08-13 12:06:11'),
(24, 'Create brand', 'admin/brand/create/', 'brand-management', NULL, '', 23, 1, 1, '2019-08-06 00:00:00', '2019-08-13 12:11:23'),
(25, 'Manage brands', 'admin/brand', 'brand-management', NULL, '', 23, 1, 1, '2019-08-13 00:00:00', '2019-08-13 12:11:23'),
(26, 'Branch', '#', 'branch-management', NULL, '', 0, 1, 1, '2019-08-13 00:00:00', '2019-08-13 14:04:55'),
(27, 'Create branch', 'admin/branch/create', 'branch-management', NULL, '', 26, 1, 1, '2019-08-13 00:00:00', '2019-08-13 14:06:31'),
(28, 'Manage branches', 'admin/branch', 'branch-management', NULL, '', 26, 1, 1, '2019-08-13 00:00:00', '2019-08-13 14:06:31'),
(29, 'Groups', '#', 'group-management', NULL, '', 0, 1, 1, '2019-08-13 00:00:00', '2019-08-13 12:06:11'),
(30, 'Create Groups', 'admin/groups/create', 'group-management', NULL, '', 29, 1, 1, '2019-08-06 00:00:00', '2019-08-13 12:11:23'),
(31, 'Manage Groups', 'admin/groups', 'group-management', NULL, '', 29, 1, 1, '2019-08-13 00:00:00', '2019-08-13 12:11:23'),
(32, 'Manage Orders', '#', 'orders', NULL, 'fa fa-dashboard', 0, 1, 1, '2019-08-02 11:27:10', '2019-08-02 11:27:10'),
(33, 'Orders in Progress', 'admin/orders', 'manage-progressing-orders', NULL, '', 32, 1, 1, '2019-08-13 00:00:00', '2019-08-13 12:11:23'),
(34, 'Completed Orders', 'admin/orders/4', 'manage-completed-orders', NULL, 'fa fa-dashboard', 32, 1, 1, '2019-08-02 11:27:10', '2019-08-02 11:27:10');

-- --------------------------------------------------------

--
-- Table structure for table `bans`
--

DROP TABLE IF EXISTS `bans`;
CREATE TABLE IF NOT EXISTS `bans` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bannable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bannable_id` bigint(20) UNSIGNED NOT NULL,
  `created_by_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by_id` bigint(20) UNSIGNED DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `expired_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

DROP TABLE IF EXISTS `branches`;
CREATE TABLE IF NOT EXISTS `branches` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `browser_title` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lattitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_code` bigint(11) DEFAULT NULL,
  `phone` bigint(11) DEFAULT NULL,
  `meta_description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_heading` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
CREATE TABLE IF NOT EXISTS `brands` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `browser_title` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(520) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_heading` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `brand_name`, `browser_title`, `meta_description`, `meta_keywords`, `page_heading`, `slug`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(5, 'Oppo', 'Oppo mobiles', 'Oppo mobiles and other electronic products', 'oppo,mobiles,f11', 'Oppo', 'oppo', NULL, NULL, '2019-09-04 04:16:19', '2019-09-04 04:16:19', NULL),
(6, 'Mi', 'Mi Store', 'Mi Mobile and Television store', 'Mi,Store,mobiles,Televisions', 'Mi Store', 'mi-store', NULL, NULL, '2019-09-04 04:57:17', '2019-09-04 04:57:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
CREATE TABLE IF NOT EXISTS `cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `user_id`, `product_id`, `quantity`, `created_at`, `updated_at`, `deleted_at`) VALUES
(7, 211908100124493, 101, 1, '2019-08-21 05:17:14', '2019-08-21 05:17:14', NULL),
(8, 221908043241779, 92, 1, '2019-08-21 23:03:05', '2019-08-21 23:03:05', NULL),
(9, 221908043241779, 93, 2, '2019-08-21 23:03:07', '2019-08-21 23:03:08', NULL),
(10, 221908043241779, 94, 1, '2019-08-21 23:03:09', '2019-08-21 23:03:09', NULL),
(11, 1, 15, 3, '2019-08-26 01:52:14', '2019-08-26 02:16:02', NULL),
(12, 1, 12, 1, '2019-08-26 02:22:46', '2019-08-26 02:22:46', NULL),
(13, 311908053812160, 15, 1, '2019-08-31 00:09:27', '2019-08-31 00:09:27', NULL),
(14, 21909053342841, 12, 1, '2019-09-02 02:07:00', '2019-09-02 02:07:00', NULL),
(15, 41909081047557, 3, 1, '2019-09-04 02:41:35', '2019-09-04 02:41:35', NULL),
(16, 1, 10, 8, '2019-09-04 03:58:56', '2019-09-04 03:58:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_category_id` int(11) DEFAULT NULL,
  `category_name` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `top_description` longtext,
  `bottom_description` longtext,
  `page_title` longtext,
  `browser_title` varchar(500) DEFAULT NULL,
  `meta_keywords` varchar(500) DEFAULT NULL,
  `meta_description` text,
  `tagline` text,
  `banner_image` int(11) DEFAULT NULL,
  `thumbnail_image` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_category_id`, `category_name`, `slug`, `top_description`, `bottom_description`, `page_title`, `browser_title`, `meta_keywords`, `meta_description`, `tagline`, `banner_image`, `thumbnail_image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(33, NULL, 'Televisions', 'television-store', NULL, NULL, 'TVs - Buy Television Online at Best Price in India', 'TVs - Buy Television Online at Best Price in India', 'electronics, apparels', 'Shop for electronics, apparels & more using our app Free shipping & COD', 'TVs - Buy Television Online at Best Price in India', NULL, NULL, '2019-09-04 04:23:40', '2019-09-04 04:23:40', NULL),
(34, NULL, 'Refrigerator', 'refrigerator', '<p>Refrigerator<br></p>', '<p>Refrigerator<br></p>', 'Refrigerator', 'Refrigerator', 'Refrigerator', 'Refrigerator', 'Refrigerator', 0, 0, '2019-10-27 23:23:53', '2019-10-27 23:23:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sortname` varchar(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  `phonecode` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `sortname`, `name`, `phonecode`) VALUES
(1, 'AF', 'Afghanistan', 93),
(2, 'AL', 'Albania', 355),
(3, 'DZ', 'Algeria', 213),
(4, 'AS', 'American Samoa', 1684),
(5, 'AD', 'Andorra', 376),
(6, 'AO', 'Angola', 244),
(7, 'AI', 'Anguilla', 1264),
(8, 'AQ', 'Antarctica', 0),
(9, 'AG', 'Antigua And Barbuda', 1268),
(10, 'AR', 'Argentina', 54),
(11, 'AM', 'Armenia', 374),
(12, 'AW', 'Aruba', 297),
(13, 'AU', 'Australia', 61),
(14, 'AT', 'Austria', 43),
(15, 'AZ', 'Azerbaijan', 994),
(16, 'BS', 'Bahamas The', 1242),
(17, 'BH', 'Bahrain', 973),
(18, 'BD', 'Bangladesh', 880),
(19, 'BB', 'Barbados', 1246),
(20, 'BY', 'Belarus', 375),
(21, 'BE', 'Belgium', 32),
(22, 'BZ', 'Belize', 501),
(23, 'BJ', 'Benin', 229),
(24, 'BM', 'Bermuda', 1441),
(25, 'BT', 'Bhutan', 975),
(26, 'BO', 'Bolivia', 591),
(27, 'BA', 'Bosnia and Herzegovina', 387),
(28, 'BW', 'Botswana', 267),
(29, 'BV', 'Bouvet Island', 0),
(30, 'BR', 'Brazil', 55),
(31, 'IO', 'British Indian Ocean Territory', 246),
(32, 'BN', 'Brunei', 673),
(33, 'BG', 'Bulgaria', 359),
(34, 'BF', 'Burkina Faso', 226),
(35, 'BI', 'Burundi', 257),
(36, 'KH', 'Cambodia', 855),
(37, 'CM', 'Cameroon', 237),
(38, 'CA', 'Canada', 1),
(39, 'CV', 'Cape Verde', 238),
(40, 'KY', 'Cayman Islands', 1345),
(41, 'CF', 'Central African Republic', 236),
(42, 'TD', 'Chad', 235),
(43, 'CL', 'Chile', 56),
(44, 'CN', 'China', 86),
(45, 'CX', 'Christmas Island', 61),
(46, 'CC', 'Cocos (Keeling) Islands', 672),
(47, 'CO', 'Colombia', 57),
(48, 'KM', 'Comoros', 269),
(49, 'CG', 'Republic Of The Congo', 242),
(50, 'CD', 'Democratic Republic Of The Congo', 242),
(51, 'CK', 'Cook Islands', 682),
(52, 'CR', 'Costa Rica', 506),
(53, 'CI', 'Cote D\'Ivoire (Ivory Coast)', 225),
(54, 'HR', 'Croatia (Hrvatska)', 385),
(55, 'CU', 'Cuba', 53),
(56, 'CY', 'Cyprus', 357),
(57, 'CZ', 'Czech Republic', 420),
(58, 'DK', 'Denmark', 45),
(59, 'DJ', 'Djibouti', 253),
(60, 'DM', 'Dominica', 1767),
(61, 'DO', 'Dominican Republic', 1809),
(62, 'TP', 'East Timor', 670),
(63, 'EC', 'Ecuador', 593),
(64, 'EG', 'Egypt', 20),
(65, 'SV', 'El Salvador', 503),
(66, 'GQ', 'Equatorial Guinea', 240),
(67, 'ER', 'Eritrea', 291),
(68, 'EE', 'Estonia', 372),
(69, 'ET', 'Ethiopia', 251),
(70, 'XA', 'External Territories of Australia', 61),
(71, 'FK', 'Falkland Islands', 500),
(72, 'FO', 'Faroe Islands', 298),
(73, 'FJ', 'Fiji Islands', 679),
(74, 'FI', 'Finland', 358),
(75, 'FR', 'France', 33),
(76, 'GF', 'French Guiana', 594),
(77, 'PF', 'French Polynesia', 689),
(78, 'TF', 'French Southern Territories', 0),
(79, 'GA', 'Gabon', 241),
(80, 'GM', 'Gambia The', 220),
(81, 'GE', 'Georgia', 995),
(82, 'DE', 'Germany', 49),
(83, 'GH', 'Ghana', 233),
(84, 'GI', 'Gibraltar', 350),
(85, 'GR', 'Greece', 30),
(86, 'GL', 'Greenland', 299),
(87, 'GD', 'Grenada', 1473),
(88, 'GP', 'Guadeloupe', 590),
(89, 'GU', 'Guam', 1671),
(90, 'GT', 'Guatemala', 502),
(91, 'XU', 'Guernsey and Alderney', 44),
(92, 'GN', 'Guinea', 224),
(93, 'GW', 'Guinea-Bissau', 245),
(94, 'GY', 'Guyana', 592),
(95, 'HT', 'Haiti', 509),
(96, 'HM', 'Heard and McDonald Islands', 0),
(97, 'HN', 'Honduras', 504),
(98, 'HK', 'Hong Kong S.A.R.', 852),
(99, 'HU', 'Hungary', 36),
(100, 'IS', 'Iceland', 354),
(101, 'IN', 'India', 91),
(102, 'ID', 'Indonesia', 62),
(103, 'IR', 'Iran', 98),
(104, 'IQ', 'Iraq', 964),
(105, 'IE', 'Ireland', 353),
(106, 'IL', 'Israel', 972),
(107, 'IT', 'Italy', 39),
(108, 'JM', 'Jamaica', 1876),
(109, 'JP', 'Japan', 81),
(110, 'XJ', 'Jersey', 44),
(111, 'JO', 'Jordan', 962),
(112, 'KZ', 'Kazakhstan', 7),
(113, 'KE', 'Kenya', 254),
(114, 'KI', 'Kiribati', 686),
(115, 'KP', 'Korea North', 850),
(116, 'KR', 'Korea South', 82),
(117, 'KW', 'Kuwait', 965),
(118, 'KG', 'Kyrgyzstan', 996),
(119, 'LA', 'Laos', 856),
(120, 'LV', 'Latvia', 371),
(121, 'LB', 'Lebanon', 961),
(122, 'LS', 'Lesotho', 266),
(123, 'LR', 'Liberia', 231),
(124, 'LY', 'Libya', 218),
(125, 'LI', 'Liechtenstein', 423),
(126, 'LT', 'Lithuania', 370),
(127, 'LU', 'Luxembourg', 352),
(128, 'MO', 'Macau S.A.R.', 853),
(129, 'MK', 'Macedonia', 389),
(130, 'MG', 'Madagascar', 261),
(131, 'MW', 'Malawi', 265),
(132, 'MY', 'Malaysia', 60),
(133, 'MV', 'Maldives', 960),
(134, 'ML', 'Mali', 223),
(135, 'MT', 'Malta', 356),
(136, 'XM', 'Man (Isle of)', 44),
(137, 'MH', 'Marshall Islands', 692),
(138, 'MQ', 'Martinique', 596),
(139, 'MR', 'Mauritania', 222),
(140, 'MU', 'Mauritius', 230),
(141, 'YT', 'Mayotte', 269),
(142, 'MX', 'Mexico', 52),
(143, 'FM', 'Micronesia', 691),
(144, 'MD', 'Moldova', 373),
(145, 'MC', 'Monaco', 377),
(146, 'MN', 'Mongolia', 976),
(147, 'MS', 'Montserrat', 1664),
(148, 'MA', 'Morocco', 212),
(149, 'MZ', 'Mozambique', 258),
(150, 'MM', 'Myanmar', 95),
(151, 'NA', 'Namibia', 264),
(152, 'NR', 'Nauru', 674),
(153, 'NP', 'Nepal', 977),
(154, 'AN', 'Netherlands Antilles', 599),
(155, 'NL', 'Netherlands The', 31),
(156, 'NC', 'New Caledonia', 687),
(157, 'NZ', 'New Zealand', 64),
(158, 'NI', 'Nicaragua', 505),
(159, 'NE', 'Niger', 227),
(160, 'NG', 'Nigeria', 234),
(161, 'NU', 'Niue', 683),
(162, 'NF', 'Norfolk Island', 672),
(163, 'MP', 'Northern Mariana Islands', 1670),
(164, 'NO', 'Norway', 47),
(165, 'OM', 'Oman', 968),
(166, 'PK', 'Pakistan', 92),
(167, 'PW', 'Palau', 680),
(168, 'PS', 'Palestinian Territory Occupied', 970),
(169, 'PA', 'Panama', 507),
(170, 'PG', 'Papua new Guinea', 675),
(171, 'PY', 'Paraguay', 595),
(172, 'PE', 'Peru', 51),
(173, 'PH', 'Philippines', 63),
(174, 'PN', 'Pitcairn Island', 0),
(175, 'PL', 'Poland', 48),
(176, 'PT', 'Portugal', 351),
(177, 'PR', 'Puerto Rico', 1787),
(178, 'QA', 'Qatar', 974),
(179, 'RE', 'Reunion', 262),
(180, 'RO', 'Romania', 40),
(181, 'RU', 'Russia', 70),
(182, 'RW', 'Rwanda', 250),
(183, 'SH', 'Saint Helena', 290),
(184, 'KN', 'Saint Kitts And Nevis', 1869),
(185, 'LC', 'Saint Lucia', 1758),
(186, 'PM', 'Saint Pierre and Miquelon', 508),
(187, 'VC', 'Saint Vincent And The Grenadines', 1784),
(188, 'WS', 'Samoa', 684),
(189, 'SM', 'San Marino', 378),
(190, 'ST', 'Sao Tome and Principe', 239),
(191, 'SA', 'Saudi Arabia', 966),
(192, 'SN', 'Senegal', 221),
(193, 'RS', 'Serbia', 381),
(194, 'SC', 'Seychelles', 248),
(195, 'SL', 'Sierra Leone', 232),
(196, 'SG', 'Singapore', 65),
(197, 'SK', 'Slovakia', 421),
(198, 'SI', 'Slovenia', 386),
(199, 'XG', 'Smaller Territories of the UK', 44),
(200, 'SB', 'Solomon Islands', 677),
(201, 'SO', 'Somalia', 252),
(202, 'ZA', 'South Africa', 27),
(203, 'GS', 'South Georgia', 0),
(204, 'SS', 'South Sudan', 211),
(205, 'ES', 'Spain', 34),
(206, 'LK', 'Sri Lanka', 94),
(207, 'SD', 'Sudan', 249),
(208, 'SR', 'Suriname', 597),
(209, 'SJ', 'Svalbard And Jan Mayen Islands', 47),
(210, 'SZ', 'Swaziland', 268),
(211, 'SE', 'Sweden', 46),
(212, 'CH', 'Switzerland', 41),
(213, 'SY', 'Syria', 963),
(214, 'TW', 'Taiwan', 886),
(215, 'TJ', 'Tajikistan', 992),
(216, 'TZ', 'Tanzania', 255),
(217, 'TH', 'Thailand', 66),
(218, 'TG', 'Togo', 228),
(219, 'TK', 'Tokelau', 690),
(220, 'TO', 'Tonga', 676),
(221, 'TT', 'Trinidad And Tobago', 1868),
(222, 'TN', 'Tunisia', 216),
(223, 'TR', 'Turkey', 90),
(224, 'TM', 'Turkmenistan', 7370),
(225, 'TC', 'Turks And Caicos Islands', 1649),
(226, 'TV', 'Tuvalu', 688),
(227, 'UG', 'Uganda', 256),
(228, 'UA', 'Ukraine', 380),
(229, 'AE', 'United Arab Emirates', 971),
(230, 'GB', 'United Kingdom', 44),
(231, 'US', 'United States', 1),
(232, 'UM', 'United States Minor Outlying Islands', 1),
(233, 'UY', 'Uruguay', 598),
(234, 'UZ', 'Uzbekistan', 998),
(235, 'VU', 'Vanuatu', 678),
(236, 'VA', 'Vatican City State (Holy See)', 39),
(237, 'VE', 'Venezuela', 58),
(238, 'VN', 'Vietnam', 84),
(239, 'VG', 'Virgin Islands (British)', 1284),
(240, 'VI', 'Virgin Islands (US)', 1340),
(241, 'WF', 'Wallis And Futuna Islands', 681),
(242, 'EH', 'Western Sahara', 212),
(243, 'YE', 'Yemen', 967),
(244, 'YU', 'Yugoslavia', 38),
(245, 'ZM', 'Zambia', 260),
(246, 'ZW', 'Zimbabwe', 263);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `group_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `group_name`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Group1', 1, 1, 1, '2019-09-19 01:18:57', '2019-09-19 01:18:57', NULL),
(2, 'Group2', 1, 1, 1, '2019-09-19 01:49:44', '2019-09-19 01:49:44', NULL),
(3, 'Group3', 1, 1, 1, '2019-09-19 01:50:45', '2019-09-19 01:50:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `group_products`
--

DROP TABLE IF EXISTS `group_products`;
CREATE TABLE IF NOT EXISTS `group_products` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `groups_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `group_products`
--

INSERT INTO `group_products` (`id`, `groups_id`, `products_id`, `created_at`, `updated_at`) VALUES
(4, 2, 20, '2019-09-19 01:49:44', '2019-09-19 01:49:44'),
(3, 1, 19, '2019-09-19 01:46:43', '2019-09-19 01:46:43'),
(5, 3, 20, '2019-09-19 01:50:45', '2019-09-19 01:50:45');

-- --------------------------------------------------------

--
-- Table structure for table `media_library`
--

DROP TABLE IF EXISTS `media_library`;
CREATE TABLE IF NOT EXISTS `media_library` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` text NOT NULL,
  `file_path` varchar(250) NOT NULL,
  `thumb_file_path` varchar(250) NOT NULL,
  `file_type` varchar(100) NOT NULL,
  `file_size` varchar(100) NOT NULL,
  `dimensions` varchar(50) DEFAULT NULL,
  `media_type` varchar(120) NOT NULL DEFAULT 'Image',
  `title` varchar(250) DEFAULT NULL,
  `description` mediumtext,
  `alt_text` varchar(250) DEFAULT NULL,
  `related_type` varchar(20) DEFAULT NULL,
  `related_id` int(11) DEFAULT NULL,
  `created_by` bigint(20) NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `media_library`
--

INSERT INTO `media_library` (`id`, `file_name`, `file_path`, `thumb_file_path`, `file_type`, `file_size`, `dimensions`, `media_type`, `title`, `description`, `alt_text`, `related_type`, `related_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'redminote7back-379x800-15598073112225d5d357ccabb5.jpeg', 'uploads/products/redminote7back-379x800-15598073112225d5d357ccabb5.jpeg', 'uploads/thumbnails/redminote7back-379x800-15598073112225d5d357ccabb5.jpeg', 'image/webp', '1702', '199 X 420', 'Image', NULL, NULL, NULL, 'Products', 3, 1, 1, '2019-08-21 12:13:49', '2019-08-21 12:13:49', NULL),
(2, '1551344291_635_redmi_note_75d5d357cc6ec5.jpg', 'uploads/products/1551344291_635_redmi_note_75d5d357cc6ec5.jpg', 'uploads/thumbnails/1551344291_635_redmi_note_75d5d357cc6ec5.jpg', 'image/webp', '19514', '560 X 420', 'Image', NULL, NULL, NULL, 'Products', 3, 1, 1, '2019-08-21 12:13:49', '2019-08-21 12:13:49', NULL),
(3, '1559042948_635_redmi_k205d5d357cc6ec5.jpg', 'uploads/products/1559042948_635_redmi_k205d5d357cc6ec5.jpg', 'uploads/thumbnails/1559042948_635_redmi_k205d5d357cc6ec5.jpg', 'image/webp', '9018', '560 X 420', 'Image', NULL, NULL, NULL, 'Products', 3, 1, 1, '2019-08-21 12:13:49', '2019-08-21 12:13:49', NULL),
(4, 'redminote7front-379x800-15598073112215d5d35ac64369.jpeg', 'uploads/products/redminote7front-379x800-15598073112215d5d35ac64369.jpeg', 'uploads/thumbnails/redminote7front-379x800-15598073112215d5d35ac64369.jpeg', 'image/webp', '24338', '199 X 420', 'Image', NULL, NULL, NULL, 'Products', 3, 1, 1, '2019-08-21 12:14:36', '2019-08-21 12:14:36', NULL),
(5, 'oppo-f11-cph1911-original-imafg4zzfnuecxf45d5e2eb2926b8.jpeg', 'uploads/products/oppo-f11-cph1911-original-imafg4zzfnuecxf45d5e2eb2926b8.jpeg', 'uploads/thumbnails/oppo-f11-cph1911-original-imafg4zzfnuecxf45d5e2eb2926b8.jpeg', 'image/jpeg', '6121', '148 X 416', 'Image', NULL, NULL, NULL, 'Products', 4, 1, 1, '2019-08-22 05:57:07', '2019-08-22 05:57:07', NULL),
(6, 'oppo-f11-cph1911-original-imafg4zzb7ngq99f5d5e2eb2926b8.jpeg', 'uploads/products/oppo-f11-cph1911-original-imafg4zzb7ngq99f5d5e2eb2926b8.jpeg', 'uploads/thumbnails/oppo-f11-cph1911-original-imafg4zzb7ngq99f5d5e2eb2926b8.jpeg', 'image/jpeg', '7110', '148 X 416', 'Image', NULL, NULL, NULL, 'Products', 4, 1, 1, '2019-08-22 05:57:07', '2019-08-22 05:57:07', NULL),
(7, 'oppo-f11-cph1911-original-imafg4zzayemzpgs5d5e2eb2926b8.jpeg', 'uploads/products/oppo-f11-cph1911-original-imafg4zzayemzpgs5d5e2eb2926b8.jpeg', 'uploads/thumbnails/oppo-f11-cph1911-original-imafg4zzayemzpgs5d5e2eb2926b8.jpeg', 'image/jpeg', '7224', '198 X 416', 'Image', NULL, NULL, NULL, 'Products', 4, 1, 1, '2019-08-22 05:57:07', '2019-08-22 05:57:07', NULL),
(8, 'oppo-f11-cph1911-original-imafg4zzxhftjah55d5e2eb2926b8.jpeg', 'uploads/products/oppo-f11-cph1911-original-imafg4zzxhftjah55d5e2eb2926b8.jpeg', 'uploads/thumbnails/oppo-f11-cph1911-original-imafg4zzxhftjah55d5e2eb2926b8.jpeg', 'image/jpeg', '2944', '23 X 416', 'Image', NULL, NULL, NULL, 'Products', 4, 1, 1, '2019-08-22 05:57:07', '2019-08-22 05:57:07', NULL),
(9, 'oppo-f11-cph1911-original-imafg4zzjz8mjjwr5d5e2eb2926b8.jpeg', 'uploads/products/oppo-f11-cph1911-original-imafg4zzjz8mjjwr5d5e2eb2926b8.jpeg', 'uploads/thumbnails/oppo-f11-cph1911-original-imafg4zzjz8mjjwr5d5e2eb2926b8.jpeg', 'image/jpeg', '8541', '198 X 416', 'Image', NULL, NULL, NULL, 'Products', 4, 1, 1, '2019-08-22 05:57:07', '2019-08-22 05:57:07', NULL),
(10, 'refrigerator5d5e7e07abeab.jpg', 'uploads/category/bannerrefrigerator5d5e7e07abeab.jpg', 'uploads/thumbnails/refrigerator5d5e7e07abeab.jpg', 'image/jpeg', '50811', '1600 X 1062', 'Image', NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-08-22 11:35:37', '2019-08-22 11:35:37', NULL),
(11, 'refrigerator5d5e7e0a93e50.jpg', 'uploads/category/primaryrefrigerator5d5e7e0a93e50.jpg', 'uploads/thumbnails/refrigerator5d5e7e0a93e50.jpg', 'image/jpeg', '50811', '1600 X 1062', 'Image', NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-08-22 11:35:39', '2019-08-22 11:35:39', NULL),
(12, 'refrigerator5d5fd3a24ada1.jpg', 'uploads/products/refrigerator5d5fd3a24ada1.jpg', 'uploads/thumbnails/refrigerator5d5fd3a24ada1.jpg', 'image/jpeg', '50811', '1600 X 1062', 'Image', NULL, NULL, NULL, 'Products', 6, 1, 1, '2019-08-23 11:53:07', '2019-08-23 11:53:07', NULL),
(13, '209533_pjpeg5d6362a7e2623.jpg', 'uploads/products/209533_pjpeg5d6362a7e2623.jpg', 'uploads/thumbnails/209533_pjpeg5d6362a7e2623.jpg', 'image/jpeg', '17030', '800 X 800', 'Image', NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-08-26 04:40:09', '2019-08-26 04:40:09', NULL),
(14, 'uususzjyssrclldjivxh_800x5d6362a87b294.jpg', 'uploads/products/uususzjyssrclldjivxh_800x5d6362a87b294.jpg', 'uploads/thumbnails/uususzjyssrclldjivxh_800x5d6362a87b294.jpg', 'image/webp', '65752', '708 X 1500', 'Image', NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-08-26 04:40:09', '2019-08-26 04:40:09', NULL),
(15, 'ABB1924BRM_Additional_1175X1290_P130601_235d6362a7e2623.jpg', 'uploads/products/ABB1924BRM_Additional_1175X1290_P130601_235d6362a7e2623.jpg', 'uploads/thumbnails/ABB1924BRM_Additional_1175X1290_P130601_235d6362a7e2623.jpg', 'image/jpeg', '64303', '1175 X 1290', 'Image', NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-08-26 04:40:09', '2019-08-26 04:40:09', NULL),
(16, 'refrigerator5d6362a7e2623.jpg', 'uploads/products/refrigerator5d6362a7e2623.jpg', 'uploads/thumbnails/refrigerator5d6362a7e2623.jpg', 'image/jpeg', '50811', '1600 X 1062', 'Image', NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-08-26 04:40:10', '2019-08-26 04:40:10', NULL),
(17, 'index5d636422ae58d.jpg', 'uploads/products/index5d636422ae58d.jpg', 'uploads/thumbnails/index5d636422ae58d.jpg', 'image/jpeg', '5784', '173 X 291', 'Image', NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-08-26 04:46:26', '2019-08-26 04:46:26', NULL),
(18, 'Whirlpool-205-ICEMAGIC-POWERCOOL-ROY-4S-205-Ltr-Single-Door-Refrigerator5d63663539cdf.jpg', 'uploads/products/Whirlpool-205-ICEMAGIC-POWERCOOL-ROY-4S-205-Ltr-Single-Door-Refrigerator5d63663539cdf.jpg', 'uploads/thumbnails/Whirlpool-205-ICEMAGIC-POWERCOOL-ROY-4S-205-Ltr-Single-Door-Refrigerator5d63663539cdf.jpg', 'image/jpeg', '5271', '220 X 293', 'Image', NULL, NULL, NULL, 'Products', 6, 1, 1, '2019-08-26 04:55:17', '2019-08-26 04:55:17', NULL),
(19, 'whirlpool-215-imfresh-prm-5-500x5005d63667b427fe.jpg', 'uploads/products/whirlpool-215-imfresh-prm-5-500x5005d63667b427fe.jpg', 'uploads/thumbnails/whirlpool-215-imfresh-prm-5-500x5005d63667b427fe.jpg', 'image/jpeg', '16956', '500 X 500', 'Image', NULL, NULL, NULL, 'Products', 6, 1, 1, '2019-08-26 04:56:27', '2019-08-26 04:56:27', NULL),
(20, 'wa62m4100hy-tl-01-samsung-original-imafhdtupmqznzym5d636dc7b1d03.jpeg', 'uploads/category/bannerwa62m4100hy-tl-01-samsung-original-imafhdtupmqznzym5d636dc7b1d03.jpeg', 'uploads/thumbnails/wa62m4100hy-tl-01-samsung-original-imafhdtupmqznzym5d636dc7b1d03.jpeg', 'image/jpeg', '15407', '416 X 409', 'Image', NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-08-26 05:27:36', '2019-08-26 05:27:36', NULL),
(21, 'wa62m4100hy-tl-01-samsung-original-imafhdtupmqznzym5d636dc82fee3.jpeg', 'uploads/category/primarywa62m4100hy-tl-01-samsung-original-imafhdtupmqznzym5d636dc82fee3.jpeg', 'uploads/thumbnails/wa62m4100hy-tl-01-samsung-original-imafhdtupmqznzym5d636dc82fee3.jpeg', 'image/jpeg', '15407', '416 X 409', 'Image', NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-08-26 05:27:36', '2019-08-26 05:27:36', NULL),
(22, 'wa62m4100hy-tl-01-samsung-original-imafhdtupmqznzym5d636f873e528.jpeg', 'uploads/products/wa62m4100hy-tl-01-samsung-original-imafhdtupmqznzym5d636f873e528.jpeg', 'uploads/thumbnails/wa62m4100hy-tl-01-samsung-original-imafhdtupmqznzym5d636f873e528.jpeg', 'image/jpeg', '15407', '416 X 409', 'Image', NULL, NULL, NULL, 'Products', 7, 1, 1, '2019-08-26 05:35:03', '2019-08-26 05:35:03', NULL),
(23, '209533_pjpeg5d639cd96638f.jpg', 'uploads/category/banner209533_pjpeg5d639cd96638f.jpg', 'uploads/thumbnails/209533_pjpeg5d639cd96638f.jpg', 'image/jpeg', '17030', '800 X 800', 'Image', NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-08-26 08:48:26', '2019-08-26 08:48:26', NULL),
(24, 'oppo-f11-cph1911-original-imafg4zzjz8mjjwr5d63a37c7a6c8.jpeg', 'uploads/category/banneroppo-f11-cph1911-original-imafg4zzjz8mjjwr5d63a37c7a6c8.jpeg', 'uploads/thumbnails/oppo-f11-cph1911-original-imafg4zzjz8mjjwr5d63a37c7a6c8.jpeg', 'image/jpeg', '8541', '198 X 416', 'Image', NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-08-26 09:16:44', '2019-08-26 09:16:44', NULL),
(25, 'oppo-f11-cph1911-original-imafg4zzayemzpgs5d63a6fa1ff5d.jpeg', 'uploads/products/oppo-f11-cph1911-original-imafg4zzayemzpgs5d63a6fa1ff5d.jpeg', 'uploads/thumbnails/oppo-f11-cph1911-original-imafg4zzayemzpgs5d63a6fa1ff5d.jpeg', 'image/jpeg', '7224', '198 X 416', 'Image', NULL, NULL, NULL, 'Products', 8, 1, 1, '2019-08-26 09:31:38', '2019-08-26 09:31:38', NULL),
(26, 'oppo-f11-cph1911-original-imafg4zzxhftjah55d63a7c4adb7b.jpeg', 'uploads/products/oppo-f11-cph1911-original-imafg4zzxhftjah55d63a7c4adb7b.jpeg', 'uploads/thumbnails/oppo-f11-cph1911-original-imafg4zzxhftjah55d63a7c4adb7b.jpeg', 'image/jpeg', '2944', '23 X 416', 'Image', NULL, NULL, NULL, 'Products', 8, 1, 1, '2019-08-26 09:35:00', '2019-08-26 09:35:00', NULL),
(27, 'mi-l32m5-al-original-imafj2cbhhsn3zje5d6f92cb71347.jpeg', 'uploads/products/mi-l32m5-al-original-imafj2cbhhsn3zje5d6f92cb71347.jpeg', 'uploads/thumbnails/mi-l32m5-al-original-imafj2cbhhsn3zje5d6f92cb71347.jpeg', 'image/jpeg', '25614', '416 X 335', 'Image', NULL, NULL, NULL, 'Products', 9, 1, 1, '2019-09-04 10:32:44', '2019-09-04 10:32:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `media_settings`
--

DROP TABLE IF EXISTS `media_settings`;
CREATE TABLE IF NOT EXISTS `media_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media_settings`
--

INSERT INTO `media_settings` (`id`, `type_id`, `width`, `height`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 25, 25, 1, 1, '2019-08-12 08:35:13', '2019-08-12 08:35:13', NULL),
(2, 2, 100, 100, 1, 1, '2019-08-12 08:35:14', '2019-08-12 08:42:11', '2019-08-12 08:42:11'),
(3, 2, 100, 100, 1, 1, '2019-08-12 08:42:49', '2019-08-12 08:42:49', NULL),
(4, 1, 100, 250, 1, 1, '2019-08-12 08:42:49', '2019-08-12 08:42:49', NULL),
(5, 3, 20, 20, 1, 1, '2019-08-12 08:59:24', '2019-08-12 08:59:24', NULL),
(6, 1, 128, 128, 1, 1, '2019-08-12 08:42:49', '2019-08-12 08:42:49', NULL),
(7, 3, 200, 200, 1, 1, '2019-08-12 08:59:24', '2019-08-12 08:59:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `media_types`
--

DROP TABLE IF EXISTS `media_types`;
CREATE TABLE IF NOT EXISTS `media_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  `path` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media_types`
--

INSERT INTO `media_types` (`id`, `type`, `path`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Products', 'public/uploads/products', '2019-08-12 07:31:33', '2019-08-12 07:31:33', NULL),
(2, 'Pages', 'public/uploads/pages', '2019-08-12 07:31:33', '2019-08-12 07:31:33', NULL),
(3, 'Thumbnails', 'public/uploads/thumbnails', '2019-08-12 07:31:33', '2019-08-12 07:31:33', NULL),
(4, 'Users', 'public/uploads/users', '2019-08-12 07:31:33', '2019-08-12 07:31:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'Main Menu', '2019-08-12 06:28:28', '2019-08-12 06:28:28');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
CREATE TABLE IF NOT EXISTS `menu_items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  `class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu` int(10) UNSIGNED NOT NULL,
  `depth` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `label`, `link`, `parent`, `sort`, `class`, `menu`, `depth`, `created_at`, `updated_at`) VALUES
(1, 'Home', 'http://www.google.com', 0, 1, NULL, 2, 0, '2019-08-12 06:28:48', '2019-08-12 06:28:48');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_01_110807_create_permission_tables', 1),
(4, '2017_03_04_000000_create_bans_table', 2),
(5, '2017_08_11_073824_create_menus_wp_table', 2),
(6, '2017_08_11_074006_create_menu_items_wp_table', 2),
(7, '2019_08_13_040442_create_products_table', 3),
(8, '2019_08_13_044816_create_product_category_attributes_table', 3),
(9, '2019_08_16_114530_create_product_varients_table', 4),
(0, '2019_09_17_072140_create_address_table', 0),
(0, '2019_09_17_073559_create_address_table', 0),
(0, '2019_09_17_073559_create_admin_pages_table', 0),
(0, '2019_09_17_073559_create_bans_table', 0),
(0, '2019_09_17_073559_create_branches_table', 0),
(0, '2019_09_17_073559_create_brands_table', 0),
(0, '2019_09_17_073559_create_cart_table', 0),
(0, '2019_09_17_073559_create_categories_table', 0),
(0, '2019_09_17_073559_create_countries_table', 0),
(0, '2019_09_17_073559_create_media_library_table', 0),
(0, '2019_09_17_073559_create_media_settings_table', 0),
(0, '2019_09_17_073559_create_media_types_table', 0),
(0, '2019_09_17_073559_create_menu_items_table', 0),
(0, '2019_09_17_073559_create_menus_table', 0),
(0, '2019_09_17_073559_create_model_has_permissions_table', 0),
(0, '2019_09_17_073559_create_model_has_roles_table', 0),
(0, '2019_09_17_073559_create_offer_categories_table', 0),
(0, '2019_09_17_073559_create_offer_combo_free_products_table', 0),
(0, '2019_09_17_073559_create_offer_combo_products_table', 0),
(0, '2019_09_17_073559_create_offer_groups_table', 0),
(0, '2019_09_17_073559_create_offer_price_products_table', 0),
(0, '2019_09_17_073559_create_offers_table', 0),
(0, '2019_09_17_073559_create_pages_table', 0),
(0, '2019_09_17_073559_create_password_resets_table', 0),
(0, '2019_09_17_073559_create_permissions_table', 0),
(0, '2019_09_17_073559_create_product_attributes_table', 0),
(0, '2019_09_17_073559_create_product_cateory_attribute_groups_table', 0),
(0, '2019_09_17_073559_create_product_cateory_attribute_values_table', 0),
(0, '2019_09_17_073559_create_product_cateory_attributes_table', 0),
(0, '2019_09_17_073559_create_product_variant_images_table', 0),
(0, '2019_09_17_073559_create_product_variants_table', 0),
(0, '2019_09_17_073559_create_products_table', 0),
(0, '2019_09_17_073559_create_role_has_permissions_table', 0),
(0, '2019_09_17_073559_create_role_users_table', 0),
(0, '2019_09_17_073559_create_roles_table', 0),
(0, '2019_09_17_073559_create_settings_table', 0),
(0, '2019_09_17_073559_create_states_table', 0),
(0, '2019_09_17_073559_create_users_table', 0),
(0, '2019_09_17_073559_create_vendors_table', 0);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
CREATE TABLE IF NOT EXISTS `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
CREATE TABLE IF NOT EXISTS `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

DROP TABLE IF EXISTS `offers`;
CREATE TABLE IF NOT EXISTS `offers` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vendor_id` int(11) NOT NULL DEFAULT '1',
  `offer_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` char(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Price, Combo, Free or group',
  `validity_start_date` date NOT NULL,
  `validity_end_date` date NOT NULL,
  `applicable_for_full_order` tinyint(1) NOT NULL DEFAULT '0',
  `discount_type` char(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Fixed Price, Discount Percentage, Discount Fixed',
  `amount` double(10,2) DEFAULT NULL,
  `percentage` int(11) DEFAULT NULL,
  `min_purchase_amount` double(10,2) DEFAULT NULL,
  `max_discount_amount` double(10,2) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `browser_title` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(520) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `vendor_id`, `offer_name`, `type`, `validity_start_date`, `validity_end_date`, `applicable_for_full_order`, `discount_type`, `amount`, `percentage`, `min_purchase_amount`, `max_discount_amount`, `is_active`, `browser_title`, `meta_description`, `meta_keywords`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Onam offer 2019', 'Price', '2019-09-25', '2019-09-30', 0, 'Discount Percentage', NULL, 10, 500.00, 250.00, 0, NULL, NULL, NULL, 1, 1, '2019-09-09 02:28:22', '2019-09-17 03:37:48', NULL),
(13, 1, 'Onam buy one get second 50% off', 'Combo', '2019-09-23', '2019-09-28', 0, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, 1, '2019-09-17 05:41:33', '2019-09-17 05:41:33', NULL),
(3, 1, 'Onam offer 2019 - Free', 'Free', '2019-09-16', '2019-09-30', 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, 1, '2019-09-12 06:17:47', '2019-09-17 08:58:43', NULL),
(4, 1, 'Onam offer 2019 - Combo', 'Combo', '2019-09-17', '2019-09-28', 0, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, 1, '2019-09-12 06:33:16', '2019-09-17 08:58:43', NULL),
(5, 1, 'Onam offer 2019 - Price all', 'Price', '2019-09-19', '2019-09-30', 1, 'Discount Percentage', NULL, 20, 1000.00, 1000.00, 0, NULL, NULL, NULL, 1, 1, '2019-09-16 22:34:49', '2019-09-17 03:37:57', NULL),
(6, 1, 'Onam offer 2019 - Price category', 'Price', '2019-09-18', '2019-09-30', 0, 'Discount Price', 100.00, NULL, 1000.00, 1000.00, 1, NULL, NULL, NULL, 1, 1, '2019-09-16 22:45:26', '2019-09-17 08:58:43', NULL),
(7, 1, 'Onam offer 2019 - Combo price', 'Combo', '2019-09-25', '2019-09-30', 0, 'Discount Percentage', NULL, 50, NULL, 1000.00, 1, NULL, NULL, NULL, 1, 1, '2019-09-16 23:16:19', '2019-09-17 08:58:43', NULL),
(8, 1, 'Onam offer 2019 - Combo another product', 'Combo', '2019-09-20', '2019-09-30', 0, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, 1, '2019-09-16 23:25:11', '2019-09-17 08:58:43', NULL),
(9, 1, 'Onam offer 2019 - Free all', 'Free', '2019-09-24', '2019-09-30', 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, 1, '2019-09-16 23:37:23', '2019-09-17 08:58:43', NULL),
(10, 1, 'Onam offer 2019 - Free all', 'Free', '2019-09-24', '2019-09-30', 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, 1, '2019-09-16 23:39:18', '2019-09-17 08:58:43', NULL),
(11, 1, 'Onam offer 2019 - Free category', 'Free', '2019-09-24', '2019-09-30', 0, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, 1, '2019-09-16 23:45:10', '2019-09-17 08:58:43', NULL),
(12, 1, 'Onam offer 2019 - Free product', 'Free', '2019-09-18', '2019-09-30', 0, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, 1, '2019-09-17 00:29:48', '2019-09-17 08:58:43', NULL),
(14, 1, 'Onam offer 2019-Group', 'Group', '2019-09-24', '2019-09-30', 0, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, 1, '2019-09-19 02:15:36', '2019-09-19 02:16:45', '2019-09-19 02:16:45'),
(15, 1, 'Onam offer 2019-Group', 'Group', '2019-09-24', '2019-09-30', 0, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, 1, '2019-09-19 02:16:07', '2019-09-19 02:16:35', '2019-09-19 02:16:35'),
(16, 1, 'Onam offer 2019-Group', 'Group', '2019-09-25', '2019-09-30', 0, 'Discount Percentage', NULL, 10, NULL, 1000.00, 1, NULL, NULL, NULL, 1, 1, '2019-09-19 02:17:17', '2019-09-19 04:13:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `offer_categories`
--

DROP TABLE IF EXISTS `offer_categories`;
CREATE TABLE IF NOT EXISTS `offer_categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `categories_id` int(11) NOT NULL,
  `offers_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offer_categories`
--

INSERT INTO `offer_categories` (`id`, `categories_id`, `offers_id`, `created_at`, `updated_at`) VALUES
(1, 33, 6, '2019-09-16 22:45:26', '2019-09-16 22:45:26'),
(3, 33, 11, '2019-09-17 00:11:48', '2019-09-17 00:11:48'),
(4, 33, 12, '2019-09-17 00:29:48', '2019-09-17 00:29:48');

-- --------------------------------------------------------

--
-- Table structure for table `offer_combo_free_products`
--

DROP TABLE IF EXISTS `offer_combo_free_products`;
CREATE TABLE IF NOT EXISTS `offer_combo_free_products` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `products_id` int(11) DEFAULT NULL,
  `offers_id` int(11) NOT NULL,
  `type` char(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Free, Fixed Price, Discount Percentage, Discount Fixed, Combo discount fixed, combo discount percentage',
  `fixed_price` double(10,2) DEFAULT NULL,
  `discount_amount` double(10,2) DEFAULT NULL,
  `discount_percentage` int(11) DEFAULT NULL,
  `max_discount_amount` double(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offer_combo_free_products`
--

INSERT INTO `offer_combo_free_products` (`id`, `products_id`, `offers_id`, `type`, `fixed_price`, `discount_amount`, `discount_percentage`, `max_discount_amount`, `created_at`, `updated_at`) VALUES
(8, 20, 3, 'Free', NULL, NULL, NULL, NULL, '2019-09-16 07:30:07', '2019-09-16 07:30:07'),
(2, 20, 4, 'Discount Percentage', NULL, NULL, 10, 100.00, '2019-09-12 06:33:16', '2019-09-12 06:33:16'),
(10, 20, 8, 'Discount Percentage', NULL, NULL, 10, 1000.00, '2019-09-16 23:35:59', '2019-09-16 23:35:59'),
(11, 19, 10, 'Free', NULL, NULL, NULL, NULL, '2019-09-16 23:39:18', '2019-09-16 23:39:18'),
(13, 20, 11, 'Free', NULL, NULL, NULL, NULL, '2019-09-17 00:11:48', '2019-09-17 00:11:48'),
(14, 19, 12, 'Free', NULL, NULL, NULL, NULL, '2019-09-17 00:29:48', '2019-09-17 00:29:48'),
(15, 20, 13, 'Discount Percentage', NULL, NULL, 50, 500.00, '2019-09-17 05:41:33', '2019-09-17 05:41:33');

-- --------------------------------------------------------

--
-- Table structure for table `offer_combo_products`
--

DROP TABLE IF EXISTS `offer_combo_products`;
CREATE TABLE IF NOT EXISTS `offer_combo_products` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL,
  `offers_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offer_combo_products`
--

INSERT INTO `offer_combo_products` (`id`, `products_id`, `offers_id`, `created_at`, `updated_at`) VALUES
(6, 19, 7, '2019-09-16 23:16:19', '2019-09-16 23:16:19'),
(2, 19, 4, '2019-09-12 06:33:16', '2019-09-12 06:33:16'),
(7, 20, 7, '2019-09-16 23:16:19', '2019-09-16 23:16:19'),
(9, 19, 8, '2019-09-16 23:35:59', '2019-09-16 23:35:59'),
(10, 19, 13, '2019-09-17 05:41:33', '2019-09-17 05:41:33');

-- --------------------------------------------------------

--
-- Table structure for table `offer_groups`
--

DROP TABLE IF EXISTS `offer_groups`;
CREATE TABLE IF NOT EXISTS `offer_groups` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `groups_id` int(11) NOT NULL,
  `offers_id` int(11) NOT NULL,
  `how_many_to_buy` int(11) NOT NULL,
  `how_many_to_get_free` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offer_groups`
--

INSERT INTO `offer_groups` (`id`, `groups_id`, `offers_id`, `how_many_to_buy`, `how_many_to_get_free`, `created_at`, `updated_at`) VALUES
(1, 1, 16, 3, NULL, '2019-09-19 04:13:38', '2019-09-19 04:13:38');

-- --------------------------------------------------------

--
-- Table structure for table `offer_price_products`
--

DROP TABLE IF EXISTS `offer_price_products`;
CREATE TABLE IF NOT EXISTS `offer_price_products` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL,
  `offers_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offer_price_products`
--

INSERT INTO `offer_price_products` (`id`, `products_id`, `offers_id`, `created_at`, `updated_at`) VALUES
(1, 19, 1, '2019-09-09 02:28:22', '2019-09-09 02:28:22'),
(2, 20, 1, '2019-09-09 02:28:22', '2019-09-09 02:28:22');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `transaction_id` varchar(150) NOT NULL,
  `order_reference_number` varchar(150) NOT NULL,
  `total_mrp` double(10,2) NOT NULL,
  `total_discount` double(10,2) NOT NULL,
  `total_sale_price` double(10,2) NOT NULL,
  `payment_method` varchar(250) NOT NULL,
  `payment_status` tinyint(1) NOT NULL DEFAULT '0',
  `delivery_address_id` int(11) NOT NULL,
  `is_cancelled` tinyint(1) NOT NULL DEFAULT '0',
  `cancelled_reason` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `users_id`, `transaction_id`, `order_reference_number`, `total_mrp`, `total_discount`, `total_sale_price`, `payment_method`, `payment_status`, `delivery_address_id`, `is_cancelled`, `cancelled_reason`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'o5u634AZNcFBUVWJzAhTsAeQJHIj3Nm1zjzR2Dmv', 'o5u634AZNcFBUVWJzAhTsAeQJHIj3Nm1zjzR2Dmv', 20000.00, 1000.00, 19000.00, 'Net Banking', 1, 1, 0, NULL, '2019-10-30 11:39:09', '2019-10-30 11:39:09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

DROP TABLE IF EXISTS `order_details`;
CREATE TABLE IF NOT EXISTS `order_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `mrp` double(10,2) NOT NULL,
  `quantity` int(4) NOT NULL,
  `sale_price` double(10,2) NOT NULL,
  `discount` double(10,2) NOT NULL,
  `expected_delivery_date` date NOT NULL,
  `customer_instructions` text,
  `is_returned` tinyint(1) NOT NULL DEFAULT '0',
  `returned_reason` text,
  `is_cancelled` tinyint(1) NOT NULL DEFAULT '0',
  `cancelled_reason` text,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `products_id` (`products_id`),
  KEY `products_id_2` (`products_id`),
  KEY `orders_id` (`orders_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `orders_id`, `products_id`, `mrp`, `quantity`, `sale_price`, `discount`, `expected_delivery_date`, `customer_instructions`, `is_returned`, `returned_reason`, `is_cancelled`, `cancelled_reason`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 19, 20000.00, 1, 19000.00, 1000.00, '2019-11-08', NULL, 0, NULL, 0, NULL, 4, '2019-10-30 11:41:07', '2019-10-31 07:07:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_status_labels_master`
--

DROP TABLE IF EXISTS `order_status_labels_master`;
CREATE TABLE IF NOT EXISTS `order_status_labels_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `display_order` int(11) NOT NULL,
  `color_code` varchar(20) DEFAULT NULL,
  `type` char(1) NOT NULL DEFAULT 'N' COMMENT 'N- normal, R-return',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_status_labels_master`
--

INSERT INTO `order_status_labels_master` (`id`, `name`, `display_order`, `color_code`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Processing', 1, NULL, 'N', '2019-10-30 11:42:33', '2019-10-30 11:42:33', NULL),
(2, 'Shipping', 2, NULL, 'N', '2019-10-30 11:42:33', '2019-10-30 11:42:33', NULL),
(3, 'Delivered', 3, NULL, 'N', '2019-10-30 11:43:06', '2019-10-30 11:43:06', NULL),
(4, 'Return Requested', 4, NULL, 'R', '2019-10-30 11:42:33', '2019-10-30 11:42:33', NULL),
(5, 'Return Accepted', 5, NULL, 'R', '2019-10-30 11:42:33', '2019-10-30 11:42:33', NULL),
(6, 'Returned', 6, NULL, 'R', '2019-10-30 11:43:06', '2019-10-30 11:43:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_tracking`
--

DROP TABLE IF EXISTS `order_tracking`;
CREATE TABLE IF NOT EXISTS `order_tracking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_details_id` int(11) NOT NULL,
  `order_status_labels_master_id` int(11) NOT NULL,
  `notes` tinytext,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_tracking`
--

INSERT INTO `order_tracking` (`id`, `order_details_id`, `order_status_labels_master_id`, `notes`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, NULL, 1, 1, '2019-10-30 11:43:37', '2019-10-30 11:43:37', NULL),
(2, 1, 2, 'Testing', 1, 1, '2019-10-31 06:08:37', '2019-10-31 06:08:37', NULL),
(3, 1, 2, 'vxcvxcvxcvcx', 1, 1, '2019-10-31 06:10:39', '2019-10-31 06:10:39', NULL),
(4, 1, 2, 'zCzxczxczxczxc', 1, 1, '2019-10-31 06:14:16', '2019-10-31 06:14:16', NULL),
(6, 1, 1, 'sczczxczx', 1, 1, '2019-10-31 06:17:44', '2019-10-31 06:17:44', NULL),
(7, 1, 3, 'cxzczxczxczx', 1, 1, '2019-10-31 06:17:53', '2019-10-31 06:17:53', NULL),
(8, 1, 4, 'czxczxvxzvxz', 1, 1, '2019-10-31 07:07:29', '2019-10-31 07:07:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(250) NOT NULL,
  `title` varchar(250) NOT NULL,
  `description` mediumtext,
  `browser_title` varchar(250) DEFAULT NULL,
  `meta_description` text,
  `meta_keywords` text,
  `media_id` int(11) DEFAULT NULL,
  `video_id` int(11) DEFAULT NULL,
  `youtube_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `apply_button_top` tinyint(1) NOT NULL DEFAULT '0',
  `apply_button_bottom` tinyint(1) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `product_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tagline` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `summary` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `top_description` text COLLATE utf8mb4_unicode_ci,
  `bottom_description` text COLLATE utf8mb4_unicode_ci,
  `quantity` int(11) DEFAULT NULL,
  `mrp` double(10,2) DEFAULT NULL,
  `sale_price` double(10,2) DEFAULT NULL,
  `is_featured_in_home_page` tinyint(1) NOT NULL DEFAULT '0',
  `is_featured_in_category` tinyint(1) NOT NULL DEFAULT '0',
  `is_new` tinyint(1) NOT NULL DEFAULT '0',
  `is_top_seller` tinyint(1) NOT NULL DEFAULT '0',
  `is_today_deal` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_completed` tinyint(1) NOT NULL DEFAULT '0',
  `default_image_id` int(11) DEFAULT NULL,
  `page_heading` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `browser_title` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `product_name`, `slug`, `tagline`, `brand_id`, `vendor_id`, `summary`, `top_description`, `bottom_description`, `quantity`, `mrp`, `sale_price`, `is_featured_in_home_page`, `is_featured_in_category`, `is_new`, `is_top_seller`, `is_today_deal`, `is_active`, `is_completed`, `default_image_id`, `page_heading`, `browser_title`, `meta_keywords`, `meta_description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(9, 33, 'Mi LED Smart TV 4A Pro 108 cm (43) with Android', 'mi-led-smart-tv-4a-pro', 'Full High Definition', NULL, NULL, '<p style=\"margin: 0px; padding: 0px 0px 8px 16px; list-style: none; position: relative;\">1920 x 1080 Full HD<br>60 Hz<br>3 x HDMI</p>', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(33, 33, 33); font-family: Roboto, Arial, sans-serif; font-size: 14px; letter-spacing: normal;\"><li class=\"_2-riNZ\" style=\"padding: 0px 0px 8px 16px; margin: 0px; list-style: none; position: relative;\">Supported Apps: Hotstar|Youtube<br>1920 x 1080 Full HD - Watch Blu-ray movies at their highest level of detail<br>60 Hz : Standard refresh rate for blur-free picture quality<br>3 x HDMI : For set top box, consoles and Blu-ray players<br>3 x USB : Easily connect your digital camera, camcorder or USB device</li></ul>', '<div class=\"_1QrSNG\" style=\"margin: 24px 0px 0px; padding: 24px; border-radius: 2px 2px 0px 0px; border-top: 1px solid rgb(240, 240, 240); border-right: 1px solid rgb(240, 240, 240); border-left: 1px solid rgb(240, 240, 240); border-image: initial; border-bottom: none; color: rgb(33, 33, 33); font-family: Roboto, Arial, sans-serif; font-size: 14px; letter-spacing: normal;\"><div class=\"_2HVvN7\" style=\"margin: 0px; padding: 0px; font-size: 24px; line-height: 1;\">Product Description</div><div class=\"_3u-uqB\" style=\"text-align: justify; margin: 0px; padding: 16px 0px 0px; line-height: 1.29;\">Enjoy television viewing like never before with this 108-cm (43) Full HD LED TV from Mi. Apart from featuring 1 GB of RAM and 8 GB of storage space, this TV also comes with cinematic sound quality, a rich range of colours, and multiple connectivity options. Also, with PatchWall and Google Voice Search, navigating and finding content to watch is simple and convenient.</div></div><div class=\"_38NXIU\" style=\"margin: 0px; padding: 0px; color: rgb(33, 33, 33); font-family: Roboto, Arial, sans-serif; font-size: 14px; letter-spacing: normal;\"><div class=\"_3LyGPp _2briKY\" style=\"margin: -1px 0px 0px; padding: 24px; border: 1px solid rgb(240, 240, 240);\"><div style=\"margin: 0px; padding: 0px;\"><div style=\"margin: 0px; padding: 0px;\"><div class=\"Pk2voS _2XGBfy _2SuWq2\" style=\"margin: 0px auto 5px 0px; padding: 0px 0px 0px 32px; text-align: center; float: right; overflow: hidden; width: 200px;\"><img src=\"https://rukminim1.flixcart.com/image/200/200/jqmnv680/television/c/m/e/mi-l43m5-an-original-imafchwyhwntqpcd.jpeg?q=90\" style=\"margin: 0px; padding: 0px; color: inherit; border-width: initial; border-color: initial; border-image: initial; outline: none; max-width: 100%;\"></div><div style=\"margin: 0px; padding: 0px;\"><div class=\"_2THx53\" style=\"margin: 0px; padding: 0px 0px 10px; font-size: 20px;\">Full High Definition</div><div class=\"_1aK10F\" style=\"margin: 0px; padding: 0px; line-height: 1.29;\"><p style=\"text-align: justify; margin-bottom: 0px; padding: 0px;\">Enjoy watching your favourite movies and TV shows in stunning picture quality, thanks to its 1080p Full HD screen and the powerful 7th generation picture quality engine.</p><p style=\"text-align: justify; margin-bottom: 0px; padding: 0px;\"><br></p></div></div></div></div></div></div><div class=\"_38NXIU\" style=\"margin: 0px; padding: 0px; color: rgb(33, 33, 33); font-family: Roboto, Arial, sans-serif; font-size: 14px; letter-spacing: normal;\"><div class=\"_3LyGPp _2briKY\" style=\"margin: -1px 0px 0px; padding: 24px; border: 1px solid rgb(240, 240, 240);\"><div style=\"margin: 0px; padding: 0px;\"><div style=\"margin: 0px; padding: 0px;\"><div class=\"Pk2voS _8Uh6-B _2SuWq2\" style=\"margin: 0px auto 5px 0px; padding: 0px 32px 0px 0px; text-align: center; float: left; overflow: hidden; width: 200px;\"><img src=\"https://rukminim1.flixcart.com/image/200/200/jqmnv680/television/c/m/e/mi-l43m5-an-original-imafchwyzycqke7f.jpeg?q=90\" style=\"margin: 0px; padding: 0px; color: inherit; border-width: initial; border-color: initial; border-image: initial; outline: none; max-width: 100%;\"></div><div style=\"margin: 0px; padding: 0px;\"><div class=\"_2THx53\" style=\"margin: 0px; padding: 0px 0px 10px; font-size: 20px;\">Gorgeous Visuals</div><div class=\"_1aK10F\" style=\"margin: 0px; padding: 0px; line-height: 1.29;\"><p style=\"margin-bottom: 0px; padding: 0px;\">It features a 7th generation Advanced Imaging Engine which reduces noise intelligently. Discover every detail with rich colour and enhanced brightness.</p><p style=\"margin-bottom: 0px; padding: 0px;\"><br></p></div></div></div></div></div></div><div class=\"_38NXIU\" style=\"margin: 0px; padding: 0px; color: rgb(33, 33, 33); font-family: Roboto, Arial, sans-serif; font-size: 14px; letter-spacing: normal;\"><div class=\"_3LyGPp _2briKY\" style=\"margin: -1px 0px 0px; padding: 24px; border: 1px solid rgb(240, 240, 240);\"><div style=\"margin: 0px; padding: 0px;\"><div style=\"margin: 0px; padding: 0px;\"><div class=\"Pk2voS _2XGBfy _2SuWq2\" style=\"margin: 0px auto 5px 0px; padding: 0px 0px 0px 32px; text-align: center; float: right; overflow: hidden; width: 200px;\"><img src=\"https://rukminim1.flixcart.com/image/200/200/jqmnv680/television/c/m/e/mi-l43m5-an-original-imafchwynhhq3w6y.jpeg?q=90\" style=\"margin: 0px; padding: 0px; color: inherit; border-width: initial; border-color: initial; border-image: initial; outline: none; max-width: 100%;\"></div><div style=\"margin: 0px; padding: 0px;\"><div class=\"_2THx53\" style=\"margin: 0px; padding: 0px 0px 10px; font-size: 20px;\">No Limit on Entertainment</div><div class=\"_1aK10F\" style=\"margin: 0px; padding: 0px; line-height: 1.29;\"><p style=\"margin-bottom: 0px; padding: 0px;\">This Mi TV ships with Patchwall with Android TV. With Patchwall you get access to over 700,000+ hours of content from 14 content partners. You also get deep integration with Set Top Box, so you can search and watch all your favourite content with a single remote.</p><p style=\"margin-bottom: 0px; padding: 0px;\"><br></p></div></div></div></div></div></div><div class=\"_38NXIU\" style=\"margin: 0px; padding: 0px; color: rgb(33, 33, 33); font-family: Roboto, Arial, sans-serif; font-size: 14px; letter-spacing: normal;\"><div class=\"_3LyGPp _2briKY\" style=\"margin: -1px 0px 0px; padding: 24px; border: 1px solid rgb(240, 240, 240);\"><div style=\"margin: 0px; padding: 0px;\"><div style=\"margin: 0px; padding: 0px;\"><div class=\"Pk2voS _8Uh6-B _2SuWq2\" style=\"margin: 0px auto 5px 0px; padding: 0px 32px 0px 0px; text-align: center; float: left; overflow: hidden; width: 200px;\"><img src=\"https://rukminim1.flixcart.com/image/200/200/jqmnv680/television/c/m/e/mi-l43m5-an-original-imafchwyyyqgu4en.jpeg?q=90\" style=\"margin: 0px; padding: 0px; color: inherit; border-width: initial; border-color: initial; border-image: initial; outline: none; max-width: 100%;\"></div><div style=\"margin: 0px; padding: 0px;\"><div class=\"_2THx53\" style=\"margin: 0px; padding: 0px 0px 10px; font-size: 20px;\">Google Android TV</div><div class=\"_1aK10F\" style=\"margin: 0px; padding: 0px; line-height: 1.29;\"><p style=\"margin-bottom: 0px; padding: 0px;\">Patchwall with Android TV brings the best of both worlds. With Android TV, you get access to Chromecast, Google Voice Search, Google Play store & YouTube.</p><p style=\"margin-bottom: 0px; padding: 0px;\"><br></p></div></div></div></div></div></div><div class=\"_38NXIU\" style=\"margin: 0px; padding: 0px; color: rgb(33, 33, 33); font-family: Roboto, Arial, sans-serif; font-size: 14px; letter-spacing: normal;\"><div class=\"_3LyGPp _2briKY\" style=\"margin: -1px 0px 0px; padding: 24px; border: 1px solid rgb(240, 240, 240);\"><div style=\"margin: 0px; padding: 0px;\"><div style=\"margin: 0px; padding: 0px;\"><div class=\"Pk2voS _2XGBfy _2SuWq2\" style=\"margin: 0px auto 5px 0px; padding: 0px 0px 0px 32px; text-align: center; float: right; overflow: hidden; width: 200px;\"><img src=\"https://rukminim1.flixcart.com/image/200/200/jqmnv680/television/c/m/e/mi-l43m5-an-original-imafchwytybzq6jg.jpeg?q=90\" style=\"margin: 0px; padding: 0px; color: inherit; border-width: initial; border-color: initial; border-image: initial; outline: none; max-width: 100%;\"></div><div style=\"margin: 0px; padding: 0px;\"><div class=\"_2THx53\" style=\"margin: 0px; padding: 0px 0px 10px; font-size: 20px;\">Find Content with Ease</div><div class=\"_1aK10F\" style=\"margin: 0px; padding: 0px; line-height: 1.29;\"><p style=\"margin-bottom: 0px; padding: 0px;\">While PatchWall curates and recommends content for you, Universal Search helps you find the content you wish to see. Also, the One Remote and the One Interface model enhances your STB and SmartTV experience.</p></div></div></div></div></div></div><div class=\"_38NXIU\" style=\"margin: 0px; padding: 0px; color: rgb(33, 33, 33); font-family: Roboto, Arial, sans-serif; font-size: 14px; letter-spacing: normal;\"><div class=\"_3LyGPp _2briKY\" style=\"margin: -1px 0px 0px; padding: 24px; border: 1px solid rgb(240, 240, 240);\"><div style=\"margin: 0px; padding: 0px;\"><div style=\"margin: 0px; padding: 0px;\"><div class=\"Pk2voS _8Uh6-B _2SuWq2\" style=\"margin: 0px auto 5px 0px; padding: 0px 32px 0px 0px; text-align: center; float: left; overflow: hidden; width: 200px;\"><img src=\"https://rukminim1.flixcart.com/image/200/200/jqmnv680/television/c/m/e/mi-l43m5-an-original-imafchwygmk2c8aw.jpeg?q=90\" style=\"margin: 0px; padding: 0px; color: inherit; border-width: initial; border-color: initial; border-image: initial; outline: none; max-width: 100%;\"></div><div style=\"margin: 0px; padding: 0px;\"><div class=\"_2THx53\" style=\"margin: 0px; padding: 0px 0px 10px; font-size: 20px;\">Chromecast</div><div class=\"_1aK10F\" style=\"margin: 0px; padding: 0px; line-height: 1.29;\"><p style=\"margin-bottom: 0px; padding: 0px;\">With Chromecast built-in, you can cast thousands of applications directly from your phone to Mi TV over Wi-Fi.</p><p style=\"margin-bottom: 0px; padding: 0px;\"><br></p></div></div></div></div></div></div><div class=\"_38NXIU\" style=\"margin: 0px; padding: 0px; color: rgb(33, 33, 33); font-family: Roboto, Arial, sans-serif; font-size: 14px; letter-spacing: normal;\"><div class=\"_3LyGPp _2briKY\" style=\"margin: -1px 0px 0px; padding: 24px; border: 1px solid rgb(240, 240, 240);\"><div style=\"margin: 0px; padding: 0px;\"><div style=\"margin: 0px; padding: 0px;\"><div class=\"Pk2voS _2XGBfy _2SuWq2\" style=\"margin: 0px auto 5px 0px; padding: 0px 0px 0px 32px; text-align: center; float: right; overflow: hidden; width: 200px;\"><img src=\"https://rukminim1.flixcart.com/image/200/200/jqmnv680/television/c/m/e/mi-l43m5-an-original-imafchwybsjuhhkk.jpeg?q=90\" style=\"margin: 0px; padding: 0px; color: inherit; border-width: initial; border-color: initial; border-image: initial; outline: none; max-width: 100%;\"></div><div style=\"margin: 0px; padding: 0px;\"><div class=\"_2THx53\" style=\"margin: 0px; padding: 0px 0px 10px; font-size: 20px;\">Loud and Clear Sound</div><div class=\"_1aK10F\" style=\"margin: 0px; padding: 0px; line-height: 1.29;\"><p style=\"margin-bottom: 0px; padding: 0px;\">You\'ll not miss out even on the subtlest of sounds, thanks to the two powerful 20 W (2x10W) stereo speakers featured in this TV.</p><p style=\"margin-bottom: 0px; padding: 0px;\"><br></p></div></div></div></div></div></div><div class=\"_38NXIU\" style=\"margin: 0px; padding: 0px; color: rgb(33, 33, 33); font-family: Roboto, Arial, sans-serif; font-size: 14px; letter-spacing: normal;\"><div class=\"_3LyGPp _2briKY\" style=\"margin: -1px 0px 0px; padding: 24px; border: 1px solid rgb(240, 240, 240);\"><div style=\"margin: 0px; padding: 0px;\"><div style=\"margin: 0px; padding: 0px;\"><div class=\"Pk2voS _8Uh6-B _2SuWq2\" style=\"margin: 0px auto 5px 0px; padding: 0px 32px 0px 0px; text-align: center; float: left; overflow: hidden; width: 200px;\"><img src=\"https://rukminim1.flixcart.com/image/200/200/jqmnv680/television/c/m/e/mi-l43m5-an-original-imafchwyahneupx7.jpeg?q=90\" style=\"margin: 0px; padding: 0px; color: inherit; border-width: initial; border-color: initial; border-image: initial; outline: none; max-width: 100%;\"></div><div style=\"margin: 0px; padding: 0px;\"><div class=\"_2THx53\" style=\"margin: 0px; padding: 0px 0px 10px; font-size: 20px;\">Fluid Performance</div><div class=\"_1aK10F\" style=\"margin: 0px; padding: 0px; line-height: 1.29;\"><p style=\"margin-bottom: 0px; padding: 0px;\">Powered by a 64-bit Quad-core processor and 1 GB of RAM, this Mi TV lets you experience a lag-free and seamless performance. With the 8GB onboard storage, you can install all your favourite apps from Google Play store.</p></div></div></div></div></div></div><div class=\"_38NXIU\" style=\"margin: 0px; padding: 0px; color: rgb(33, 33, 33); font-family: Roboto, Arial, sans-serif; font-size: 14px; letter-spacing: normal;\"><div class=\"_3LyGPp _2briKY\" style=\"margin: -1px 0px 0px; padding: 24px; border: 1px solid rgb(240, 240, 240);\"><div style=\"margin: 0px; padding: 0px;\"><div style=\"margin: 0px; padding: 0px;\"><div class=\"Pk2voS _2XGBfy _2SuWq2\" style=\"margin: 0px auto 5px 0px; padding: 0px 0px 0px 32px; text-align: center; float: right; overflow: hidden; width: 200px;\"><img src=\"https://rukminim1.flixcart.com/image/200/200/jqmnv680/television/c/m/e/mi-l43m5-an-original-imafchwy4mz5vrj9.jpeg?q=90\" style=\"margin: 0px; padding: 0px; color: inherit; border-width: initial; border-color: initial; border-image: initial; outline: none; max-width: 100%;\"></div><div style=\"margin: 0px; padding: 0px;\"><div class=\"_2THx53\" style=\"margin: 0px; padding: 0px 0px 10px; font-size: 20px;\">Easy Connectivity</div><div class=\"_1aK10F\" style=\"margin: 0px; padding: 0px; line-height: 1.29;\"><p style=\"margin-bottom: 0px; padding: 0px;\">You have a wide range of connectivity options available with this Mi TV - there are 3 HDMI (1 ARC), 3 USB, Bluetooth 4.2, Dual Band WiFi, S/PDIF (Coax), 3.5mm Out, AV & Antenna to cater to all your connectivity requirement.</p><p style=\"margin-bottom: 0px; padding: 0px;\"><br></p></div></div></div></div></div></div>', 1000, 20000.00, 20000.00, 1, 1, 1, 0, 0, 1, 1, NULL, 'Mi LED Smart TV 4A Pro 108 cm (43) with Android', NULL, NULL, NULL, 1, 1, '2019-09-04 04:59:10', '2019-10-27 23:17:23', NULL),
(10, 33, 'Mi LED Smart TV 4A Pro 108 cm (43) with Android Copy', 'mi-led-smart-tv-4a-pro-copy', 'Full High Definition', NULL, NULL, '<p style=\"margin: 0px; padding: 0px 0px 8px 16px; list-style: none; position: relative;\">1920 x 1080 Full HD<br>60 Hz<br>3 x HDMI</p>', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(33, 33, 33); font-family: Roboto, Arial, sans-serif; font-size: 14px; letter-spacing: normal;\"><li class=\"_2-riNZ\" style=\"padding: 0px 0px 8px 16px; margin: 0px; list-style: none; position: relative;\">Supported Apps: Hotstar|Youtube<br>1920 x 1080 Full HD - Watch Blu-ray movies at their highest level of detail<br>60 Hz : Standard refresh rate for blur-free picture quality<br>3 x HDMI : For set top box, consoles and Blu-ray players<br>3 x USB : Easily connect your digital camera, camcorder or USB device</li></ul>', '<div class=\"_1QrSNG\" style=\"margin: 24px 0px 0px; padding: 24px; border-radius: 2px 2px 0px 0px; border-top: 1px solid rgb(240, 240, 240); border-right: 1px solid rgb(240, 240, 240); border-left: 1px solid rgb(240, 240, 240); border-image: initial; border-bottom: none; color: rgb(33, 33, 33); font-family: Roboto, Arial, sans-serif; font-size: 14px; letter-spacing: normal;\"><div class=\"_2HVvN7\" style=\"margin: 0px; padding: 0px; font-size: 24px; line-height: 1;\">Product Description</div><div class=\"_3u-uqB\" style=\"text-align: justify; margin: 0px; padding: 16px 0px 0px; line-height: 1.29;\">Enjoy television viewing like never before with this 108-cm (43) Full HD LED TV from Mi. Apart from featuring 1 GB of RAM and 8 GB of storage space, this TV also comes with cinematic sound quality, a rich range of colours, and multiple connectivity options. Also, with PatchWall and Google Voice Search, navigating and finding content to watch is simple and convenient.</div></div><div class=\"_38NXIU\" style=\"margin: 0px; padding: 0px; color: rgb(33, 33, 33); font-family: Roboto, Arial, sans-serif; font-size: 14px; letter-spacing: normal;\"><div class=\"_3LyGPp _2briKY\" style=\"margin: -1px 0px 0px; padding: 24px; border: 1px solid rgb(240, 240, 240);\"><div style=\"margin: 0px; padding: 0px;\"><div style=\"margin: 0px; padding: 0px;\"><div class=\"Pk2voS _2XGBfy _2SuWq2\" style=\"margin: 0px auto 5px 0px; padding: 0px 0px 0px 32px; text-align: center; float: right; overflow: hidden; width: 200px;\"><img src=\"https://rukminim1.flixcart.com/image/200/200/jqmnv680/television/c/m/e/mi-l43m5-an-original-imafchwyhwntqpcd.jpeg?q=90\" style=\"margin: 0px; padding: 0px; color: inherit; border-width: initial; border-color: initial; border-image: initial; outline: none; max-width: 100%;\"></div><div style=\"margin: 0px; padding: 0px;\"><div class=\"_2THx53\" style=\"margin: 0px; padding: 0px 0px 10px; font-size: 20px;\">Full High Definition</div><div class=\"_1aK10F\" style=\"margin: 0px; padding: 0px; line-height: 1.29;\"><p style=\"text-align: justify; margin-bottom: 0px; padding: 0px;\">Enjoy watching your favourite movies and TV shows in stunning picture quality, thanks to its 1080p Full HD screen and the powerful 7th generation picture quality engine.</p><p style=\"text-align: justify; margin-bottom: 0px; padding: 0px;\"><br></p></div></div></div></div></div></div><div class=\"_38NXIU\" style=\"margin: 0px; padding: 0px; color: rgb(33, 33, 33); font-family: Roboto, Arial, sans-serif; font-size: 14px; letter-spacing: normal;\"><div class=\"_3LyGPp _2briKY\" style=\"margin: -1px 0px 0px; padding: 24px; border: 1px solid rgb(240, 240, 240);\"><div style=\"margin: 0px; padding: 0px;\"><div style=\"margin: 0px; padding: 0px;\"><div class=\"Pk2voS _8Uh6-B _2SuWq2\" style=\"margin: 0px auto 5px 0px; padding: 0px 32px 0px 0px; text-align: center; float: left; overflow: hidden; width: 200px;\"><img src=\"https://rukminim1.flixcart.com/image/200/200/jqmnv680/television/c/m/e/mi-l43m5-an-original-imafchwyzycqke7f.jpeg?q=90\" style=\"margin: 0px; padding: 0px; color: inherit; border-width: initial; border-color: initial; border-image: initial; outline: none; max-width: 100%;\"></div><div style=\"margin: 0px; padding: 0px;\"><div class=\"_2THx53\" style=\"margin: 0px; padding: 0px 0px 10px; font-size: 20px;\">Gorgeous Visuals</div><div class=\"_1aK10F\" style=\"margin: 0px; padding: 0px; line-height: 1.29;\"><p style=\"margin-bottom: 0px; padding: 0px;\">It features a 7th generation Advanced Imaging Engine which reduces noise intelligently. Discover every detail with rich colour and enhanced brightness.</p><p style=\"margin-bottom: 0px; padding: 0px;\"><br></p></div></div></div></div></div></div><div class=\"_38NXIU\" style=\"margin: 0px; padding: 0px; color: rgb(33, 33, 33); font-family: Roboto, Arial, sans-serif; font-size: 14px; letter-spacing: normal;\"><div class=\"_3LyGPp _2briKY\" style=\"margin: -1px 0px 0px; padding: 24px; border: 1px solid rgb(240, 240, 240);\"><div style=\"margin: 0px; padding: 0px;\"><div style=\"margin: 0px; padding: 0px;\"><div class=\"Pk2voS _2XGBfy _2SuWq2\" style=\"margin: 0px auto 5px 0px; padding: 0px 0px 0px 32px; text-align: center; float: right; overflow: hidden; width: 200px;\"><img src=\"https://rukminim1.flixcart.com/image/200/200/jqmnv680/television/c/m/e/mi-l43m5-an-original-imafchwynhhq3w6y.jpeg?q=90\" style=\"margin: 0px; padding: 0px; color: inherit; border-width: initial; border-color: initial; border-image: initial; outline: none; max-width: 100%;\"></div><div style=\"margin: 0px; padding: 0px;\"><div class=\"_2THx53\" style=\"margin: 0px; padding: 0px 0px 10px; font-size: 20px;\">No Limit on Entertainment</div><div class=\"_1aK10F\" style=\"margin: 0px; padding: 0px; line-height: 1.29;\"><p style=\"margin-bottom: 0px; padding: 0px;\">This Mi TV ships with Patchwall with Android TV. With Patchwall you get access to over 700,000+ hours of content from 14 content partners. You also get deep integration with Set Top Box, so you can search and watch all your favourite content with a single remote.</p><p style=\"margin-bottom: 0px; padding: 0px;\"><br></p></div></div></div></div></div></div><div class=\"_38NXIU\" style=\"margin: 0px; padding: 0px; color: rgb(33, 33, 33); font-family: Roboto, Arial, sans-serif; font-size: 14px; letter-spacing: normal;\"><div class=\"_3LyGPp _2briKY\" style=\"margin: -1px 0px 0px; padding: 24px; border: 1px solid rgb(240, 240, 240);\"><div style=\"margin: 0px; padding: 0px;\"><div style=\"margin: 0px; padding: 0px;\"><div class=\"Pk2voS _8Uh6-B _2SuWq2\" style=\"margin: 0px auto 5px 0px; padding: 0px 32px 0px 0px; text-align: center; float: left; overflow: hidden; width: 200px;\"><img src=\"https://rukminim1.flixcart.com/image/200/200/jqmnv680/television/c/m/e/mi-l43m5-an-original-imafchwyyyqgu4en.jpeg?q=90\" style=\"margin: 0px; padding: 0px; color: inherit; border-width: initial; border-color: initial; border-image: initial; outline: none; max-width: 100%;\"></div><div style=\"margin: 0px; padding: 0px;\"><div class=\"_2THx53\" style=\"margin: 0px; padding: 0px 0px 10px; font-size: 20px;\">Google Android TV</div><div class=\"_1aK10F\" style=\"margin: 0px; padding: 0px; line-height: 1.29;\"><p style=\"margin-bottom: 0px; padding: 0px;\">Patchwall with Android TV brings the best of both worlds. With Android TV, you get access to Chromecast, Google Voice Search, Google Play store & YouTube.</p><p style=\"margin-bottom: 0px; padding: 0px;\"><br></p></div></div></div></div></div></div><div class=\"_38NXIU\" style=\"margin: 0px; padding: 0px; color: rgb(33, 33, 33); font-family: Roboto, Arial, sans-serif; font-size: 14px; letter-spacing: normal;\"><div class=\"_3LyGPp _2briKY\" style=\"margin: -1px 0px 0px; padding: 24px; border: 1px solid rgb(240, 240, 240);\"><div style=\"margin: 0px; padding: 0px;\"><div style=\"margin: 0px; padding: 0px;\"><div class=\"Pk2voS _2XGBfy _2SuWq2\" style=\"margin: 0px auto 5px 0px; padding: 0px 0px 0px 32px; text-align: center; float: right; overflow: hidden; width: 200px;\"><img src=\"https://rukminim1.flixcart.com/image/200/200/jqmnv680/television/c/m/e/mi-l43m5-an-original-imafchwytybzq6jg.jpeg?q=90\" style=\"margin: 0px; padding: 0px; color: inherit; border-width: initial; border-color: initial; border-image: initial; outline: none; max-width: 100%;\"></div><div style=\"margin: 0px; padding: 0px;\"><div class=\"_2THx53\" style=\"margin: 0px; padding: 0px 0px 10px; font-size: 20px;\">Find Content with Ease</div><div class=\"_1aK10F\" style=\"margin: 0px; padding: 0px; line-height: 1.29;\"><p style=\"margin-bottom: 0px; padding: 0px;\">While PatchWall curates and recommends content for you, Universal Search helps you find the content you wish to see. Also, the One Remote and the One Interface model enhances your STB and SmartTV experience.</p></div></div></div></div></div></div><div class=\"_38NXIU\" style=\"margin: 0px; padding: 0px; color: rgb(33, 33, 33); font-family: Roboto, Arial, sans-serif; font-size: 14px; letter-spacing: normal;\"><div class=\"_3LyGPp _2briKY\" style=\"margin: -1px 0px 0px; padding: 24px; border: 1px solid rgb(240, 240, 240);\"><div style=\"margin: 0px; padding: 0px;\"><div style=\"margin: 0px; padding: 0px;\"><div class=\"Pk2voS _8Uh6-B _2SuWq2\" style=\"margin: 0px auto 5px 0px; padding: 0px 32px 0px 0px; text-align: center; float: left; overflow: hidden; width: 200px;\"><img src=\"https://rukminim1.flixcart.com/image/200/200/jqmnv680/television/c/m/e/mi-l43m5-an-original-imafchwygmk2c8aw.jpeg?q=90\" style=\"margin: 0px; padding: 0px; color: inherit; border-width: initial; border-color: initial; border-image: initial; outline: none; max-width: 100%;\"></div><div style=\"margin: 0px; padding: 0px;\"><div class=\"_2THx53\" style=\"margin: 0px; padding: 0px 0px 10px; font-size: 20px;\">Chromecast</div><div class=\"_1aK10F\" style=\"margin: 0px; padding: 0px; line-height: 1.29;\"><p style=\"margin-bottom: 0px; padding: 0px;\">With Chromecast built-in, you can cast thousands of applications directly from your phone to Mi TV over Wi-Fi.</p><p style=\"margin-bottom: 0px; padding: 0px;\"><br></p></div></div></div></div></div></div><div class=\"_38NXIU\" style=\"margin: 0px; padding: 0px; color: rgb(33, 33, 33); font-family: Roboto, Arial, sans-serif; font-size: 14px; letter-spacing: normal;\"><div class=\"_3LyGPp _2briKY\" style=\"margin: -1px 0px 0px; padding: 24px; border: 1px solid rgb(240, 240, 240);\"><div style=\"margin: 0px; padding: 0px;\"><div style=\"margin: 0px; padding: 0px;\"><div class=\"Pk2voS _2XGBfy _2SuWq2\" style=\"margin: 0px auto 5px 0px; padding: 0px 0px 0px 32px; text-align: center; float: right; overflow: hidden; width: 200px;\"><img src=\"https://rukminim1.flixcart.com/image/200/200/jqmnv680/television/c/m/e/mi-l43m5-an-original-imafchwybsjuhhkk.jpeg?q=90\" style=\"margin: 0px; padding: 0px; color: inherit; border-width: initial; border-color: initial; border-image: initial; outline: none; max-width: 100%;\"></div><div style=\"margin: 0px; padding: 0px;\"><div class=\"_2THx53\" style=\"margin: 0px; padding: 0px 0px 10px; font-size: 20px;\">Loud and Clear Sound</div><div class=\"_1aK10F\" style=\"margin: 0px; padding: 0px; line-height: 1.29;\"><p style=\"margin-bottom: 0px; padding: 0px;\">You\'ll not miss out even on the subtlest of sounds, thanks to the two powerful 20 W (2x10W) stereo speakers featured in this TV.</p><p style=\"margin-bottom: 0px; padding: 0px;\"><br></p></div></div></div></div></div></div><div class=\"_38NXIU\" style=\"margin: 0px; padding: 0px; color: rgb(33, 33, 33); font-family: Roboto, Arial, sans-serif; font-size: 14px; letter-spacing: normal;\"><div class=\"_3LyGPp _2briKY\" style=\"margin: -1px 0px 0px; padding: 24px; border: 1px solid rgb(240, 240, 240);\"><div style=\"margin: 0px; padding: 0px;\"><div style=\"margin: 0px; padding: 0px;\"><div class=\"Pk2voS _8Uh6-B _2SuWq2\" style=\"margin: 0px auto 5px 0px; padding: 0px 32px 0px 0px; text-align: center; float: left; overflow: hidden; width: 200px;\"><img src=\"https://rukminim1.flixcart.com/image/200/200/jqmnv680/television/c/m/e/mi-l43m5-an-original-imafchwyahneupx7.jpeg?q=90\" style=\"margin: 0px; padding: 0px; color: inherit; border-width: initial; border-color: initial; border-image: initial; outline: none; max-width: 100%;\"></div><div style=\"margin: 0px; padding: 0px;\"><div class=\"_2THx53\" style=\"margin: 0px; padding: 0px 0px 10px; font-size: 20px;\">Fluid Performance</div><div class=\"_1aK10F\" style=\"margin: 0px; padding: 0px; line-height: 1.29;\"><p style=\"margin-bottom: 0px; padding: 0px;\">Powered by a 64-bit Quad-core processor and 1 GB of RAM, this Mi TV lets you experience a lag-free and seamless performance. With the 8GB onboard storage, you can install all your favourite apps from Google Play store.</p></div></div></div></div></div></div><div class=\"_38NXIU\" style=\"margin: 0px; padding: 0px; color: rgb(33, 33, 33); font-family: Roboto, Arial, sans-serif; font-size: 14px; letter-spacing: normal;\"><div class=\"_3LyGPp _2briKY\" style=\"margin: -1px 0px 0px; padding: 24px; border: 1px solid rgb(240, 240, 240);\"><div style=\"margin: 0px; padding: 0px;\"><div style=\"margin: 0px; padding: 0px;\"><div class=\"Pk2voS _2XGBfy _2SuWq2\" style=\"margin: 0px auto 5px 0px; padding: 0px 0px 0px 32px; text-align: center; float: right; overflow: hidden; width: 200px;\"><img src=\"https://rukminim1.flixcart.com/image/200/200/jqmnv680/television/c/m/e/mi-l43m5-an-original-imafchwy4mz5vrj9.jpeg?q=90\" style=\"margin: 0px; padding: 0px; color: inherit; border-width: initial; border-color: initial; border-image: initial; outline: none; max-width: 100%;\"></div><div style=\"margin: 0px; padding: 0px;\"><div class=\"_2THx53\" style=\"margin: 0px; padding: 0px 0px 10px; font-size: 20px;\">Easy Connectivity</div><div class=\"_1aK10F\" style=\"margin: 0px; padding: 0px; line-height: 1.29;\"><p style=\"margin-bottom: 0px; padding: 0px;\">You have a wide range of connectivity options available with this Mi TV - there are 3 HDMI (1 ARC), 3 USB, Bluetooth 4.2, Dual Band WiFi, S/PDIF (Coax), 3.5mm Out, AV & Antenna to cater to all your connectivity requirement.</p><p style=\"margin-bottom: 0px; padding: 0px;\"><br></p></div></div></div></div></div></div>', 10, 25999.00, 22999.00, 1, 1, 1, 0, 0, 1, 1, NULL, 'Mi LED Smart TV 4A Pro 108 cm (43) with Android', NULL, NULL, NULL, 1, 1, '2019-09-04 04:59:10', '2019-09-04 05:03:18', NULL),
(11, 34, 'Whirlpool 265 L Frost Free Double Door 4 Star Convertible Refrigerator', 'whirlpool-265-l-frost-free-double-door-4-star-convertible-refrigerator', 'Whirlpool 265 L Frost Free Double Door 4 Star Convertible Refrigerator', NULL, NULL, '<p>Whirlpool 265 L Frost Free Double Door 4 Star Convertible Refrigerator<br></p>', '<p>Whirlpool 265 L Frost Free Double Door 4 Star Convertible Refrigerator<br></p>', '<p>Whirlpool 265 L Frost Free Double Door 4 Star Convertible Refrigerator<br></p>', 1000, 30000.00, 24500.00, 1, 1, 1, 1, 1, 1, 1, NULL, 'Whirlpool 265 L Frost Free Double Door 4 Star Convertible Refrigerator', 'Whirlpool 265 L Frost Free Double Door 4 Star Convertible Refrigerator', 'Whirlpool 265 L Frost Free Double Door 4 Star Convertible Refrigerator', 'Whirlpool 265 L Frost Free Double Door 4 Star Convertible Refrigerator', 1, 1, '2019-10-27 23:26:12', '2019-10-27 23:31:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_attributes`
--

DROP TABLE IF EXISTS `product_attributes`;
CREATE TABLE IF NOT EXISTS `product_attributes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `attribute_value_id` int(11) DEFAULT NULL,
  `attribute_value` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_attributes`
--

INSERT INTO `product_attributes` (`id`, `products_id`, `attribute_id`, `attribute_value_id`, `attribute_value`, `created_at`, `updated_at`) VALUES
(2, 11, 28, NULL, '1 Refrigerator Unit Manual Warranty Card', '2019-10-27 23:55:22', '2019-10-27 23:55:22'),
(3, 11, 30, 49, 'Top Mount', '2019-10-27 23:55:22', '2019-10-27 23:55:22');

-- --------------------------------------------------------

--
-- Table structure for table `product_cateory_attributes`
--

DROP TABLE IF EXISTS `product_cateory_attributes`;
CREATE TABLE IF NOT EXISTS `product_cateory_attributes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `attribute_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `show_as_variant` tinyint(2) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_cateory_attributes`
--

INSERT INTO `product_cateory_attributes` (`id`, `category_id`, `attribute_name`, `attribute_type`, `group_id`, `show_as_variant`, `created_at`, `updated_at`) VALUES
(27, 33, 'Screen Size', 'Selectable', NULL, 1, '2019-09-04 04:49:20', '2019-09-04 04:49:20'),
(28, 34, 'In The Box', 'Running Text', NULL, 0, '2019-10-27 23:34:51', '2019-10-27 23:34:51'),
(29, 34, 'Type', 'Selectable', NULL, 1, '2019-10-27 23:53:55', '2019-10-27 23:53:55'),
(30, 34, 'Refrigerator Type', 'Selectable', NULL, 0, '2019-10-27 23:54:32', '2019-10-27 23:54:32');

-- --------------------------------------------------------

--
-- Table structure for table `product_cateory_attribute_groups`
--

DROP TABLE IF EXISTS `product_cateory_attribute_groups`;
CREATE TABLE IF NOT EXISTS `product_cateory_attribute_groups` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `group_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_cateory_attribute_values`
--

DROP TABLE IF EXISTS `product_cateory_attribute_values`;
CREATE TABLE IF NOT EXISTS `product_cateory_attribute_values` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_cateory_attribute_values`
--

INSERT INTO `product_cateory_attribute_values` (`id`, `attribute_id`, `value`, `created_at`, `updated_at`) VALUES
(44, 27, '40', '2019-09-04 04:49:20', '2019-09-04 04:49:20'),
(43, 27, '41', '2019-09-04 04:49:20', '2019-09-04 04:49:20'),
(42, 27, '56', '2019-09-04 04:49:20', '2019-09-04 04:49:20'),
(41, 27, '24', '2019-09-04 04:49:20', '2019-09-04 04:49:20'),
(40, 27, '32', '2019-09-04 04:49:20', '2019-09-04 04:49:20'),
(47, 29, 'Single Door', '2019-10-27 23:53:55', '2019-10-27 23:53:55'),
(48, 29, 'Double Door', '2019-10-27 23:53:55', '2019-10-27 23:53:55'),
(49, 30, 'Top Mount', '2019-10-27 23:54:32', '2019-10-27 23:54:32'),
(50, 30, 'Bottom Mount', '2019-10-27 23:54:32', '2019-10-27 23:54:32');

-- --------------------------------------------------------

--
-- Table structure for table `product_inventory_by_vendor`
--

DROP TABLE IF EXISTS `product_inventory_by_vendor`;
CREATE TABLE IF NOT EXISTS `product_inventory_by_vendor` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vendor_id` int(11) NOT NULL DEFAULT '1',
  `variant_id` int(11) NOT NULL,
  `barcode` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `retail_price` double(10,2) DEFAULT NULL,
  `sale_price` double(10,2) DEFAULT NULL,
  `landing_price` double(10,2) DEFAULT NULL,
  `available_quantity` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_inventory_by_vendor`
--

INSERT INTO `product_inventory_by_vendor` (`id`, `vendor_id`, `variant_id`, `barcode`, `retail_price`, `sale_price`, `landing_price`, `available_quantity`, `created_at`, `updated_at`) VALUES
(4, 1, 21, NULL, 30000.00, 24500.00, 30000.00, 1000, '2019-10-27 23:29:08', '2019-10-27 23:29:08'),
(3, 1, 19, NULL, 20000.00, 20000.00, 10000.00, 1000, '2019-10-27 23:16:54', '2019-10-27 23:17:23'),
(5, 1, 22, NULL, 20000.00, 20000.00, 20000.00, 1000, '2019-10-27 23:59:59', '2019-10-27 23:59:59');

-- --------------------------------------------------------

--
-- Table structure for table `product_variants`
--

DROP TABLE IF EXISTS `product_variants`;
CREATE TABLE IF NOT EXISTS `product_variants` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level1_attribute_value_id` int(11) DEFAULT NULL,
  `level2_attribute_value_id` int(11) DEFAULT NULL,
  `level3_attribute_value_id` int(11) DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `sku` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `image_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_variants`
--

INSERT INTO `product_variants` (`id`, `products_id`, `name`, `slug`, `level1_attribute_value_id`, `level2_attribute_value_id`, `level3_attribute_value_id`, `is_default`, `sku`, `short_description`, `image_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(19, 9, 'Mi LED Smart TV 4A Pro 108 cm (43) with Android', 'mi-led-smart-tv-4a-pro', NULL, NULL, NULL, 1, '00000001', '<p style=\"margin: 0px; padding: 0px 0px 8px 16px; list-style: none; position: relative;\">1920 x 1080 Full HD<br>60 Hz<br>3 x HDMI</p>', 27, 1, 1, '2019-09-04 04:59:11', '2019-09-04 05:03:18', NULL),
(20, 10, 'Mi LED Smart TV 4A Pro 108 cm (43) with Android Copy', 'mi-led-smart-tv-4a-pro-copy', NULL, NULL, NULL, 1, '00000001', '<p style=\"margin: 0px; padding: 0px 0px 8px 16px; list-style: none; position: relative;\">1920 x 1080 Full HD<br>60 Hz<br>3 x HDMI</p>', 27, 1, 1, '2019-09-04 04:59:11', '2019-09-04 05:03:18', NULL),
(21, 11, 'Whirlpool 265 L Frost Free Double Door 4 Star Convertible Refrigerator', 'whirlpool-265-l-frost-free-double-door-4-star-convertible-refrigerator', NULL, NULL, NULL, 1, 'SKU009WL', NULL, NULL, 1, 1, '2019-10-27 23:26:12', '2019-10-27 23:29:08', NULL),
(22, 11, 'Whirlpool 265 L Frost Free Double Door 4 Star Convertible Refrigerator (Type - Double Door)', 'whirlpool-265-l-frost-free-double-door-4-star-convertible-refrigerator-double-door', 48, NULL, NULL, 0, 'SKU000WBDD', '<p>Whirlpool 265 L Frost Free Double Door 4 Star Convertible Refrigerator (Type - Double Door)<br></p>', NULL, 1, 1, '2019-10-27 23:59:59', '2019-10-27 23:59:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_variant_images`
--

DROP TABLE IF EXISTS `product_variant_images`;
CREATE TABLE IF NOT EXISTS `product_variant_images` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `variant_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alt` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_common` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `status`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 1, 'web', '2019-08-02 00:00:39', '2019-08-02 00:00:39'),
(2, 'Staff', 1, 'web', '2019-08-16 04:12:54', '2019-08-16 04:12:54');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
CREATE TABLE IF NOT EXISTS `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_users`
--

DROP TABLE IF EXISTS `role_users`;
CREATE TABLE IF NOT EXISTS `role_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_users`
--

INSERT INTO `role_users` (`user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-08-01 23:55:40', '2019-08-01 23:55:40'),
(2, 1, '2019-08-06 07:22:48', '2019-08-06 07:22:48'),
(3, 1, '2019-08-07 00:03:15', '2019-08-07 00:03:15'),
(4, 1, '2019-08-14 06:16:56', '2019-08-14 06:16:56');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(250) NOT NULL,
  `value` varchar(250) DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL,
  `type` varchar(20) NOT NULL,
  `page` varchar(250) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `code`, `value`, `media_id`, `type`, `page`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'title', 'Pittappillil', NULL, 'Text', NULL, 1, 1, '2019-08-10 07:14:34', '2019-08-12 05:27:30', NULL),
(2, 'logo', 'geethu1.jpg', 6, 'Image', NULL, 1, 1, '2019-08-10 07:14:35', '2019-08-12 09:04:37', NULL),
(3, 'Footer-text1', 'Footer Text1', NULL, 'Text', NULL, 1, 1, '2019-08-12 05:28:30', '2019-08-12 05:28:30', NULL),
(4, 'Footer-text2', 'Footer Text2 Editted', NULL, 'Text', NULL, 1, 1, '2019-08-12 05:28:30', '2019-08-12 05:30:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

DROP TABLE IF EXISTS `states`;
CREATE TABLE IF NOT EXISTS `states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CountryID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `CountryID`, `name`) VALUES
(36, 101, 'ANDHRA PRADESH'),
(37, 101, 'ASSAM'),
(38, 101, 'ARUNACHAL PRADESH'),
(39, 101, 'GUJRAT'),
(40, 101, 'BIHAR'),
(41, 101, 'HARYANA'),
(42, 101, 'HIMACHAL PRADESH'),
(43, 101, 'JAMMU & KASHMIR'),
(44, 101, 'KARNATAKA'),
(45, 101, 'KERALA'),
(46, 101, 'MADHYA PRADESH'),
(47, 101, 'MAHARASHTRA'),
(48, 101, 'MANIPUR'),
(49, 101, 'MEGHALAYA'),
(50, 101, 'MIZORAM'),
(51, 101, 'NAGALAND'),
(52, 101, 'ORISSA'),
(53, 101, 'PUNJAB'),
(54, 101, 'RAJASTHAN'),
(55, 101, 'SIKKIM'),
(56, 101, 'TAMIL NADU'),
(57, 101, 'TRIPURA'),
(58, 101, 'UTTAR PRADESH'),
(59, 101, 'WEST BENGAL'),
(60, 101, 'DELHI'),
(61, 101, 'GOA'),
(62, 101, 'PONDICHERY'),
(63, 101, 'LAKSHDWEEP'),
(64, 101, 'DAMAN & DIU'),
(65, 101, 'DADRA & NAGAR'),
(66, 101, 'CHANDIGARH'),
(67, 101, 'ANDAMAN & NICOBAR'),
(68, 101, 'UTTARANCHAL'),
(69, 101, 'JHARKHAND'),
(70, 101, 'CHATTISGARH');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` int(11) DEFAULT NULL,
  `phone_number` bigint(100) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `pin_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `first_name`, `last_name`, `email`, `email_verified_at`, `password`, `remember_token`, `country_code`, `phone_number`, `country_id`, `state_id`, `pin_code`, `address`, `banned_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'spider-admin', 'Sobha', 'Sudheesh', 'sobha@spiderworks.in', NULL, '$2y$10$Qum8cKcyuGu/AcZ9rFbBIuFgok0oDTTPCmyNlxGh93eGxqmYckxd2', 'Qsg7U0ce0yX7OV8D6oa2tKbNf7zWbik6wBgSvULeWEbrdNAGeJTNtCviBdht', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-01 06:12:16', '2019-10-25 07:11:18', NULL),
(2, 'test1.last@gmail.com', 'Test1', 'Last', 'test1.last@gmail.com', NULL, '$2y$10$QsNtNcSoFynwlvG6eEXexeCXxCq15jb4xfoG.Gz7YfY8au6V/eyyK', NULL, NULL, NULL, 101, 45, '6878787', 'Test Address', NULL, '2019-08-06 07:22:46', '2019-08-07 08:58:26', NULL),
(3, 'test2.last2@gmail.com', 'Test2', 'Last2', 'test2.last2@gmail.com', NULL, '$2y$10$NnwT4iqX1yFxycdgLlndSeQlerPZ4Mmaq6mJ45KJ62OlinSOtPf/y', NULL, 91, 9898989898, 101, 45, '683517', 'Test2 address editted', NULL, '2019-08-07 00:03:15', '2019-08-07 08:59:36', NULL),
(4, 'test3.last3@gmail.com', 'Test3', 'Last3', 'test3.last3@gmail.com', NULL, '$2y$10$kPwVuHQfexBoriNOZw6jlOUCfdxz5Q7TX4OptSQ1sQuUZH8dd2quG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-14 06:16:56', '2019-08-14 06:16:56', NULL),
(5, NULL, NULL, NULL, 'ghost19940309@gmail.com', NULL, '$2y$10$3BKgj9mOWW5Ih53SU5ej/OEAZbnD2fXL/Hr.A..KB8pIFFsOxR3OS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-04 05:34:58', '2019-09-04 05:34:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

DROP TABLE IF EXISTS `vendors`;
CREATE TABLE IF NOT EXISTS `vendors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_name` varchar(250) NOT NULL,
  `page_heading` varchar(250) NOT NULL,
  `slug` varchar(250) NOT NULL,
  `contact_name` varchar(250) DEFAULT NULL,
  `phone_code` int(11) DEFAULT NULL,
  `phone` bigint(11) DEFAULT NULL,
  `address` text,
  `email` varchar(250) DEFAULT NULL,
  `description` text,
  `browser_title` varchar(250) DEFAULT NULL,
  `meta_description` text,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
