<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Offers;
use App\Models\Offers\OfferView;
use Carbon\Carbon as Carbon;

class OfferController extends Controller
{
    public function home(){
        $offers =  Offers::whereDate('validity_end_date','>',Carbon::now())->get();
        return view('client.pages.offer',compact('offers'));
    }

    public function offer_details($slug){
        $offer = Offers::where('slug',$slug)->first();
        $products = OfferView::where('offer',$offer->id)->paginate(12);
        return view('client/pages/offer_details',compact('products','offer'));
    }


}
