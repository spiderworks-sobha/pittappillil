<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Admin\ResourceTrait;
use App\Models\Products, Input, Request, View, Redirect, DB, Datatables, Sentinel, Mail, Validator, Image;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request as Reqst;
use App\Models\Products\Variants;
use App\Models\Category\CategoryAttributes;
use App\Models\Products\Attributes;
use App\Models\Category\CategoryAttributeValues;
use App\Models\Products\Variants\Inventory;
use App\Exports\ProductsExport;
use App\Imports\ProductsImports;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Category;
use App\Models\Brand;

class ProductsController extends BaseController
{
    use ResourceTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->model = new Products;

        $this->route .= '.products';
        $this->views .= '.products';
        $this->url = "admin/products/";

        $this->resourceConstruct();

    }

    protected function getCollection() {
        $collection = DB::table('products')->select('products.id', 'products.product_name', 'products.page_heading', 'products.is_active', 'products.is_completed', 'categories.category_name', 'products.use_psp', 'products.updated_at')->join('categories', 'products.category_id', '=', 'categories.id')->where('is_warranty_product', 0)->whereNull('products.deleted_at');

        return $collection; 
    }

    protected function setDTData($collection) {
        $route = $this->route;
        return $this->initDTData($collection)
            ->editColumn('updated_at', function($obj) { return date('m/d/Y H:i:s', strtotime($obj->updated_at)); })
            ->editColumn('is_active', function($obj) {
                if($obj->is_active)
                    return '<span><i class="fa fa-check-circle text-success"></i></span>';
                else
                    return '<span><i class="fa fa-times-circle text-danger"></i></span>';
            })
            ->editColumn('use_psp', function($obj) use ($route) {
                $id =  $obj->id;
                if($obj->use_psp == 0)
                    return '<a href="' . url('admin/products/set-psp-as-price', [encrypt($id)]) . '" class="btn btn-danger"><i class="fa fa-times"></i></a>';
                else
                   return '<a href="' . url('admin/products/set-psp-as-price', [encrypt($id)]) . '" class="btn btn-success" ><i class="fa fa-check"></i></a>'; 
            })
            ->setRowClass(function ($obj) {
                    return ($obj->is_completed==0 ? 'bg-warning' : ' ');
            })
            ->rawColumns(['is_active', 'action_edit', 'action_delete', 'use_psp']);
    }

    public function create()
    {
        $gallery = [];
        $varient = new Variants;
        return view($this->views . '.form')->with('obj', $this->model)->with('gallery', $gallery)->with('varient', $varient);
    }

    public function edit($id, $tab=null) {
        $id = decrypt($id);
        if($obj = $this->model->find($id)){
            $varients = Variants::where('products_id', $obj->id)->count();

            $parent_varient = Variants::where('products_id', $obj->id)->orderBy('id', 'asc')->first();
            $parent_variant_id = $parent_varient->id;
            $gallery = $this->getVariantGallery($parent_variant_id);

            $attributes = CategoryAttributes::where('category_id', $obj->category_id)->where('show_as_variant', 0)->get();

            return view($this->views . '.edit')->with('obj', $obj)->with('gallery', $gallery)->with('varients', $varients)->with('attributes', $attributes)->with('tab', $tab)->with('parent_varient', $parent_varient);
        } else {
            return $this->redirect('notfound');
        }
    }

    public function store(Reqst $r)
    {
        $data = $r->all();
        $this->model->validate();
        $data['is_featured_in_home_page'] = isset($data['is_featured_in_home_page'])?1:0;
        $data['is_featured_in_category'] = isset($data['is_featured_in_category'])?1:0;
        $data['is_new'] = isset($data['is_new'])?1:0;
        $data['is_top_seller'] = isset($data['is_top_seller'])?1:0;
        $data['is_today_deal'] = isset($data['is_today_deal'])?1:0;
        $this->model->fill($data);
        if($this->model->save())
        {
            $varient_data = ['name'=>$data['product_name'], 'slug'=>$data['slug'], 'products_id'=>$this->model->id, 'is_default'=>1];
            $varient = new Variants;
            $varient->fill($varient_data);
            $varient->save();
        }
        return Redirect::to(url('admin/products/edit', array('id'=>encrypt($this->model->id))))->withSuccess('Products successfully saved!'); 
    }

    public function update(Reqst $r)
    {
        $data = $r->all();
        $id = decrypt($data['id']);
        $this->model->validate($data, $id);
        if($obj = $this->model->find($id)){
            $data['is_featured_in_home_page'] = isset($data['is_featured_in_home_page'])?1:0;
            $data['is_featured_in_category'] = isset($data['is_featured_in_category'])?1:0;
            $data['is_new'] = isset($data['is_new'])?1:0;
            $data['is_top_seller'] = isset($data['is_top_seller'])?1:0;
            $data['is_today_deal'] = isset($data['is_today_deal'])?1:0;
            $data['mrp'] = isset($data['retail_price'])?$data['retail_price']:$obj->mrp;
            $data['sale_price'] = isset($data['sale_price'])?$data['sale_price']:$obj->sale_price;
            $data['quantity'] = isset($data['quantity'])?$data['quantity']:$obj->quantity;
            if(isset($data['retail_price']) && $data['retail_price']!="" && isset($data['sale_price']) && $data['sale_price']!="" && isset($data['quantity']) && $data['quantity']!="" && isset($data['sku']) && $data['sku']!="")
                $data['is_completed'] = 1;
            $obj->update($data);

            $data['name'] = $data['product_name'];
            $parent_varient = Variants::where('products_id', $obj->id)->orderBy('id', 'asc')->first();
            $parent_variant_id = $parent_varient->id;

            //$parent_varient->image_id = $data['image_id'];
            if($parent_varient->update($data))
            {
                $inventrory = new Inventory;
                $inventrory->saveVariantByInventory($parent_varient->id, $data);
            }

            Attributes::where('products_id', $obj->id)->delete();
            $attributes = CategoryAttributes::where('category_id', $obj->category_id)->get();
            foreach ($attributes as $key => $attribute) {
                if(isset($data['attribute_'.$attribute->id]))
                {
                    $product_attribute = new Attributes;
                    $product_attribute->products_id = $obj->id;
                    $product_attribute->attribute_id = $attribute->id;
                    
                    if($attribute->attribute_type == 'Selectable')
                    {
                        $product_attribute->attribute_value_id = $data['attribute_'.$attribute->id];
                        $selected_value = CategoryAttributeValues::find($data['attribute_'.$attribute->id]);
                        $product_attribute->attribute_value = $selected_value->value;
                    }
                    else{
                        $product_attribute->attribute_value = $data['attribute_'.$attribute->id];
                    }
                    $product_attribute->save();
                }
            }

            if(isset($data['media_id']))
                $this->saveGallery($parent_variant_id, $data, 'edit');

            return Redirect::to(url('admin/products/edit', array('id'=>encrypt($obj->id))))->withSuccess('Product details successfully updated!');
        } else {
            return Redirect::back()
                        ->withErrors("Ooops..Something wrong happend.Please try again.") // send back all errors to the login form
                        ->withInput(Input::all());
        }
    }

    public function destroy($id) {
        $id = decrypt($id);
        $obj = $this->model->find($id);
        if ($obj) {
            $obj->delete();
            return response()->json(['success'=>'Product successfully deleted']);
        }
        return response()->json(['error'=>'Oops!! something went wrong...Please try again.']);
    }

    public function mediaSave(Reqst $r)
    {
        $data = $r->all();
        $files = $data['files'];
        $json = array(
            'files' => array()
        );
        foreach ($files as $key=> $file) {
            $upload = $this->uploadFile($file, $this->model->uploadPath['products'], ['Products']);
            if($upload['success']) {
                if(isset($data['related_type']) && $data['related_type']!='' && isset($data['related_id']) && $data['related_id']!='')
                {
                    $upload['related_type'] = $data['related_type'];
                    $upload['related_id'] = $data['related_id'];
                }
                $result = $this->saveMedia($upload);
                $json['files'][] = array(
                        'name' => $result['name'],
                        'size' => $result['size'],
                        'url' => \URL::to('').'/public/'.$result['url'],
                        'id' => $result['id'],
                        'original_file' => $result['original_file'],
                        'type' => $result['type'],
                );
            }
        }
        return response()->json($json);
    }

    public function export()
    {
        return Excel::download(new ProductsExport, 'products.xlsx');
    }

    public function import()
    {
        return view($this->views . '.import');
    }

    public function import_save(Reqst $request)
    {
        ini_set('max_execution_time', '0');
        $this->validate($request, [
          'excel_file'  => 'required|mimes:xls,xlsx'
        ]);
        
        $path = $request->file('excel_file')->getRealPath();
        $rows = Excel::toArray(new ProductsImports, $path, null, \Maatwebsite\Excel\Excel::XLSX)[0];

        foreach ($rows as $key => $product) {
            $save_product = new Products;
            $save_product->product_code = $product['product_as_in_wings'];
            $save_product->slug = $product['product_as_in_wings'];
            $save_product->page_heading = $product['product_as_in_wings'];

            $category_id = 0;
            if(isset($product['category_as_in_wings']))
            {
                $old_category = DB::table('old_categories')->where('category_code', $product['category'])->first();
                if($old_category)
                {
                    $category = Category::where('category_code', $product['category'])->first();
                    if($category)
                    {
                        $category_id = $category->id;
                    }
                    else{
                        $new_category = (array)$old_category;

                        $category = new Category;
                        $category->fill($new_category);
                        $category->save();
                        
                        $category_id = $category->id;
                    }
                }
                else{
                    $category = Category::where('category_code', $product['category'])->first();
                    if($category)
                    {
                        $category_id = $category->id;
                    }
                    else{
                        $category = new Category;
                        $category->category_code = $product['category'];
                        $category->category_name = $product['category_as_in_wings'];
                        $category->slug = $product['category_as_in_wings'];
                        $category->status = 0;
                        $category->save();
                        $category_id = $category->id;
                    }
                }
            }
            $brand_id = null;
            if(isset($product['brand_as_in_wings']))
            {
                $old_brand = DB::table('old_brands')->where('brand_name', $product['brand_as_in_wings'])->first();
                if($old_brand)
                {
                    $brand = Brand::where('brand_code', $product['brand_as_in_wings'])->first();
                    if($brand)
                        $brand_id = $brand->id;
                    else{

                        $new_brand = (array)$old_brand;
                        $new_brand['brand_code'] = $product['brand_as_in_wings'];

                        $brand = new Brand;
                        $brand->fill($new_brand);
                        $brand->save();
                        $brand_id = $brand->id;
                    }
                }
                else{
                    $brand = Brand::where('brand_code', $product['brand_as_in_wings'])->first();
                    if($brand)
                        $brand_id = $brand->id;
                    else{
                        $brand = new Brand;
                        $brand->brand_code = $product['brand_as_in_wings'];
                        $brand->brand_name = $product['brand_as_in_wings'];
                        $brand->slug = $product['brand_as_in_wings'];
                        $brand->status = 0;
                        $brand->save();
                    }
                }
            }
            $save_product->brand_id = $brand_id;
            $save_product->category_id = $category_id;
            $save_product->product_name = ($product['title'])?$product['title']:$product['product_as_in_wings'];
            $save_product->summary = $product['description'];
            $save_product->top_description = $product['description'];
            $save_product->hsn_code = $product['hsn'];
            $save_product->cgst = $product['cgst']*100;
            $save_product->sgst = $product['sgst']*100;
            if($save_product->save())
            {
                $varient_data = ['name'=>$save_product->product_name, 'slug'=>$save_product->slug, 'products_id'=>$save_product->id, 'is_default'=>1];
                $varient = new Variants;
                $varient->fill($varient_data);
                if($varient->save())
                {
                    $inventory_data = ['retail_price'=>$product['mrp_as_in_wings'], 'sale_price'=>$product['sale_price_psp'], 'landing_price'=>$product['mrp_as_in_wings'], 'quantity'=>$product['stock_as_of_30_jan']];
                    $inventrory = new Inventory;
                    $inventrory->saveVariantByInventory($varient->id, $inventory_data);
                }
            }
        }
        return redirect()->back()->withSuccess('Excel successfully imported!');
    }

    public function setPriceAsPsp($id)
    {
        $id =  decrypt($id);
        if($obj = $this->model->find($id))
        {
            $set_status = ($obj->use_psp == 1)?0:1;
            $obj->use_psp = $set_status;
            $obj->save();
            $message = ($obj->use_psp == 0)?"disabled":"enabled";
            return Redirect::route($this->route . '.index')->withSuccess($this->entity.' price successfully '.$message.'!');
        }

        return response()->json(['error'=>'Oops!! something went wrong...Please try again.']);
    }
}
