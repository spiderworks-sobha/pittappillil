<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Branch;
use App\Models\District;
use App\Models\BranchLandmark;
use DB, Image;

class BranchController extends Controller
{
    
    public function index(Request $request)
    {
    	$data = $request->all();
    	$branches = Branch::where('status', 1);
    	$selected_location = null;
    	$selected_district = null;
    	$locations = null;
    	if($data)
    	{
    		if(isset($data['location']) && $data['location'] != '')
    		{
    			$selected_location = $data['location'];
    			$branches->where('landmark_id', $selected_location);
    		}
    		if(isset($data['district']) && $data['district'] != '')
    		{
    			$selected_district = $data['district'];
    			$locations = BranchLandmark::where('district_id', $selected_district)->get();
    			if(!$selected_location)
    			{
	    			$branches->whereHas('location', function($query) use($selected_district){
	    				$query->where('district_id', $selected_district);
	    			});
	    		}
    		}
    	}
    	$branches = $branches->get();

    	$branch_marker = [];
    	foreach ($branches as $key => $branch) {
    		if($branch->lattitude && $branch->longitude)
    		{
	    		$branch_marker[$key]['title'] = $branch->page_heading;
	    		$branch_marker[$key]['lat'] = $branch->lattitude;
	    		$branch_marker[$key]['lng'] = $branch->longitude;
	    		$address = nl2br($branch->address);
	    		$html = $branch->page_heading;
	    		$html .= '<br/>';
	    		if($branch->media)
	    		{
	    			$html .= '<img src="'.asset($branch->media->thumb_file_path).'">';
	    			$html .= "<br/>";
	    		}
	    		$html .= $address;
	    		$html .= '<br/>';
	    		if($branch->landline_number || $branch->mobile_number)
	    		{
	    			$html .= '<br/>Contact No : ';
	    			$contact_nums = [];
	    			if($branch->landline_number)
	    				$contact_nums[] = $branch->landline_number;
	    			if($branch->mobile_number)
	    				$contact_nums[] = $branch->mobile_number;
	    			$html .= implode(', ', $contact_nums);
	    		}
	    		if($branch->contact_person)
	    		{
	    			$html .= '<br/>Contact Person : '.$branch->contact_person;
	    			if($branch->contact_person_number)
	    				$html .= ", ".$branch->contact_person_number;
	    		}
	    		$url_address = urlencode($branch->branch_name.', '.$branch->address);
	    		$html .="<br><a target=_blank href=https://www.google.co.in/maps/search/".$url_address."/@'+ '".$branch->lattitude."' +','+ '".$branch->longitude."' +',15z>Locate</a>";

	    		$branch_marker[$key]['description'] = $html;
	    	}
    	}
    	$response = json_encode($branch_marker);
    	$districts = District::all();
        return view('client/branches/index', ['branches'=>$branches, 'branches_json'=>$response, 'districts'=>$districts, 'selected_location'=>$selected_location, 'selected_district'=>$selected_district, 'locations'=>$locations]);
    }

    public function view($branch)
    {
    	$obj = Branch::where('status', 1)->where('slug', $branch)->first();
    	if($obj)
    	{
    		$branch_marker = [];
    		if($obj->lattitude && $obj->longitude)
    		{
	    		$branch_marker['title'] = $obj->page_heading;
	    		$branch_marker['lat'] = $obj->lattitude;
	    		$branch_marker['lng'] = $obj->longitude;
	    		$address = nl2br($obj->address);
	    		$html = $obj->page_heading;
	    		$html .= '<br/>';
	    		if($obj->media)
	    		{
	    			$html .= '<img src="'.asset($obj->media->thumb_file_path).'">';
	    			$html .= "<br/>";
	    		}
	    		$html .= $address;
	    		$html .= '<br/>';
	    		if($obj->landline_number || $obj->mobile_number)
	    		{
	    			$html .= '<br/>Contact No : ';
	    			$contact_nums = [];
	    			if($obj->landline_number)
	    				$contact_nums[] = $obj->landline_number;
	    			if($obj->mobile_number)
	    				$contact_nums[] = $obj->mobile_number;
	    			$html .= implode(', ', $contact_nums);
	    		}
	    		if($obj->contact_person)
	    		{
	    			$html .= '<br/>Contact Person : '.$obj->contact_person;
	    			if($obj->contact_person_number)
	    				$html .= ", ".$obj->contact_person_number;
	    		}
	    		$url_address = urlencode($obj->branch_name.', '.$obj->address);
	    		$html .="<br><a target=_blank href=https://www.google.co.in/maps/search/".$url_address."/@'+ '".$obj->lattitude."' +','+ '".$obj->longitude."' +',15z>Locate</a>";

	    		$branch_marker['description'] = $html;
	    	}
	    	$response = json_encode($branch_marker);
    		return view('client/branches/view', ['obj'=>$obj, 'branches_json'=>$response]);
    	}
    	else
    		abort('404');
    }
}
