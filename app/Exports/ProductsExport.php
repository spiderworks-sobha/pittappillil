<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class ProductsExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table('products_tb')->select("product_code", "sub_category_code", "title", DB::raw("'' AS short_description"), 'description', 'price AS mrp', DB::raw("'' AS sale_price"), 'quantity')->where('is_active', 1)->get();
    }

    public function headings(): array
    {
        return [
            'Code',
            'Category',
            'Title',
            'Short Description',
            'Description',
            'MRP',
            'Sale Price',
            'Quantity'
        ];
    }
}
