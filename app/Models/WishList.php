<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WishList extends Model
{
    protected $table = 'wishlist';

    public function product_details(){
        return $this->belongsTo('App\Models\Products\Variants','variant_id');
    }

    public function offer_details(){
        return $this->hasOne('App\User','user_id');
    }

}
