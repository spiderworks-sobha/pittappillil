<?php

namespace App\Models\Products\Variants;

use App\Models\BaseModel, App\Models\ValidationTrait, DB;

class Inventory extends BaseModel
{
	use ValidationTrait {
        ValidationTrait::validate as private parent_validate;
    }
    
    public function __construct() {
        
        parent::__construct();
        $this->__validationConstruct();
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_inventory_by_vendor';


    protected $fillable = array('vendor_id', 'variant_id', 'barcode', 'retail_price', 'sale_price', 'landing_price', 'available_quantity');

    protected $dates = ['created_at','updated_at'];


    protected function setRules() {

        $this->val_rules = array(
        );
    }

    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }

    public function varient()
    {
        return $this->belongsTo('App\Models\Products\Variants', 'variant_id');
    }

    public function vendor()
    {
        return $this->belongsTo('App\Models\Vendor', 'vendor_id');
    }

    public function saveVariantByInventory($id, $data)
    {
        if(isset($data['inventory']))
            $inventories = $data['inventory'];
        else
            $inventories = [1];

        if($inventories)
        {
            foreach ($inventories as $key => $value) {
                $inventrory = static::updateOrCreate([
                    'vendor_id' => $value,
                    'variant_id' => $id,
                ],[
                    'vendor_id' => $value,
                    'variant_id' => $id,
                    'barcode' => isset($data['barcode'])?$data['barcode']:null,
                    'retail_price' =>$data['retail_price'],
                    'sale_price' =>$data['sale_price'],
                    'landing_price' => isset($data['landing_price'])?$data['landing_price']:null,
                    'available_quantity' => isset($data['quantity'])?$data['quantity']:null,
                ]);
            }
        }
        return true;
    }

}