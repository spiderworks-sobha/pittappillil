<?php

namespace App\Models;
use App\Models\Settings;
use Illuminate\Database\Eloquent\Model;

class Key extends Model
{
    public static function get($key){
        $value = Settings::where('code',$key)->first();
        if(!$value){return 'NA';}else{
            if($value->type == 'Image'){
                if($value->media){
                    return asset($value->media->file_path);
                }
            }else{
                return $value->value;
            }
        }return 'NA';
    }
}