<div class="col-md-6">

                                    <ul class="list-group list-group-horizontal mb-2">
                                        @foreach($variants as $key=> $item)
                                            @if($key == 0)
                                                <li class="list-group-item attribute-sec">{{$item->attribute_name}}</li>
                                            @endif
                                            @if($level == 1)
                                                <li class="list-group-item img-attr @if(!in_array($item->value_id, $available_attribute)) disabled-attribute @endif">
                                                    <a href="javascript:void(0)" data-id="{{$product->products_id}}" data-attribute-value="{{$item->value_id}}" data-level="{{$level}}" class="product-attribute-link  @if($selected_attribute == $item->value_id) selected-attribute @endif">
                                                        @if(isset($item->file_name) && is_file(public_path('uploads/products/128X128/'.$item->file_name))) 
                                                            <img src="{{asset('public/uploads/products/128X128/'.$item->file_name)}}" class="" alt="...">
                                                        @else
                                                            <span>{{$item->attribute_value}}</span>
                                                        @endif
                                                    </a>
                                                </li>
                                            @else
                                                <li class="list-group-item  @if(!in_array($item->value_id, $available_attribute)) disabled-attribute @endif">
                                                    <a href="javascript:void(0)" data-id="{{$product->products_id}}" data-attribute-value="{{$item->value_id}}" data-level="{{$level}}" class="product-attribute-link @if($selected_attribute == $item->value_id) selected-attribute @endif"><span>{{$item->attribute_value}}</span></a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>