<html>
    <head>
        <title>Ecommerce by pittppillil</title>
        <meta id="_token" content="{{csrf_token()}}">
        <meta id="_user" content="{{session('guest')}}">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
        <link  href="{{URL::asset('public/assets/css/front.css')}}" rel="stylesheet" type="text/css" />
        <script
                src="https://code.jquery.com/jquery-3.4.1.min.js"
                integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
        <script src="https://kit.fontawesome.com/041979cf48.js"></script>
        <script>const baseUrl = '{{URL::to('')}}';const token = '{{csrf_token()}}'</script>
        <style>
            a{
                color: black;
            }
            a:hover{
                color: black;
                text-decoration: none;
            }
            .pointer{
                cursor: pointer;
            }

            .selected-attribute{
                border: 1px dashed blue;
                padding: 10px;
            }
        </style>
        @section('top')
            @show
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12" style="padding-top: 10px;">
                    @include('vanila.common.nav')

                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                @section('content')
                @show
            </div>
        </div>

        <script src="{{asset('public/js/alerts.js')}}"></script>
        <script src="{{asset('public/js/auth.js')}}"></script>
        <script src="{{asset('public/js/cart.js')}}"></script>
        <script>
            function numberWithCommas(x) {
                return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }
            cart.cartcount();
            cart.carttotal();
            $('.add-to-cart').click(function () {
                cart.add($(this).data('id'),1)
            });
            $('.add-to-cart-single').click(function () {
                cart.add($(this).data('id'),$('#qty').val())
            });


            function login(){
                $.confirm({
                    title: false,
                    content: 'url:{{URL::to('popup/login')}}',
                    contentLoaded: function(data, status, xhr){
                    },
                    buttons: {
                        login: {
                            text: 'Login',
                            btnClass: 'btn btn-success',
                            keys: ['enter', 'shift'],
                            action: function(){
                                auth.signin()
                            }
                        }
                    }
                });
            }

            function register() {
                $.confirm({
                    title: false,
                    content: 'url:{{URL::to('popup/register')}}',
                    contentLoaded: function(data, status, xhr){
                    },
                    buttons: {
                        login: {
                            text: 'register',
                            btnClass: 'btn btn-success',
                            keys: ['enter', 'shift'],
                            action: function(){
                                auth.signup()
                            }
                        }
                    }
                });
            }

        </script>

        @section('bottom')
            @show



    </body>
</html>