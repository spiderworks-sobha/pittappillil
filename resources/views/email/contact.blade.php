@extends('email.base')
@section('content')
        <div style="width: 100%; margin:5px 0 15px; ">
            <h2 style="font-size: 24px; color: #333; text-align: center; font-weight: 600; ">Pittappillil Support Request</h2>
            <div style="clear: both;"></div>
        </div>
        <div style="width: 100%; margin:15px 0; text-align: center; ">
            <table>
			    <tbody>
			    	<tr>
			            <td>Name: </td>
			            <td>{{$data->name}}</td>
			        </tr>
			        <tr>
			            <td>Email: </td>
			            <td>{{$data->email}}</td>
			        </tr>
			        <tr>
			            <td>Subject: </td>
			            <td>{{$data->subject}}</td>
			        </tr>
			        <tr>
			            <td>Message: </td>
			            <td>{{$data->message}}</td>
			        </tr>
			    </tbody>
			</table>
        </div>
@endsection
