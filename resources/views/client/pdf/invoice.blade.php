<!DOCTYPE html>
<html>
<head>
    <title>{{$order->order_reference_number}}</title>
    <style type="text/css">
        th, td {
            border-right: solid 1px #777;
            text-align: center;
            font-size: 12px;
            padding: 5px;
        }
        thead,tbody{

            border-top: solid 1px #777;
        }
        tbody{
            border-bottom: solid 1px #777;
        }
        td:nth-child(9), th:nth-child(9)  {
            border-right:none;
        }
        td:nth-child(0), th:nth-child(0)  {
            border-left:none;
        }

        p{
            font-size: 12px;
        }
    </style>
</head>
<body style="border: 1px solid black">

<div style="width: 100%;display: inline-block;">

    <div style="float: left">
    <img src="{{ base_path() }}/public/logo.jpg" style="width: 120px;margin: 5px;"><br>
    <p style="margin: 5px;">
        <b> Pittappillil Agencies</b> <br>
        ADMINISTRATIVE OFFICE<br>
        CATTLE MARKET ROAD<br>
        PERUMBAVOOR<br>
        PIN: 683 542<br>
        PH: 0484 2594803, +91 8606966806<br><br>

        <b>GSTIN : 32AADFP6144R1ZV</b><br>
    </p>
    </div>

    <div style="float: right">
        <h1 style="margin-right: 10px">TAX INVOICE</h1>
        <p style="margin: 5px;">
            Invoice No : {{$order->order_reference_number}}<br>
            Invoice Date : {{\Carbon\Carbon::parse($order->created_at)->format('d-F-Y')}}
        </p>
    </div>

</div><br><br>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<div style="width: 100%;display: inline-block;">

    <div style="float: left;width: 50%" >
        <p style="margin: 5px;padding:5px 0px;background: grey;color:#FFF;width:100%;text-align: center">
            <b>Billing Address</b>
        </p>
        <p style="margin: 5px 20px;">
            <b>Customer Code : {{$order->delivery_address->id}}<br>{{$order->delivery_address->full_name}}</b>
            <br>{{$order->delivery_address->address1}}
            <br>{{$order->delivery_address->address2}}
            <br>Pin : {{$order->delivery_address->pincode}}
            <br>City : {{$order->delivery_address->city}}
        </p>
    </div>

    <div style="float: right;width: 50%">
        <p style="margin: 5px;padding:5px 0px;background: grey;color:#FFF;width:100%;text-align: center">
            <b>Shipping Address</b>
        <p style="margin: 5px 20px;">
            <b>Customer Code : {{$order->delivery_address->id}}<br>{{$order->delivery_address->full_name}}</b>
            <br>{{$order->delivery_address->address1}}
            <br>{{$order->delivery_address->address2}}
            <br>Pin : {{$order->delivery_address->pincode}}
            <br>City : {{$order->delivery_address->city}}
        </p>
    </div>

</div>
<br><br><br><br><br><br><br>
<div style="width: 100%;display: inline-block;">
    <table width="100%" style=" border-spacing: 0px;">
        <thead style="background: #e4e4e4;">
            <td>S No</td>
            <td>Description of Goods</td>
            <td>Qty</td>
            <td>Unit Rate</td>
            <td>Taxable value</td>
            <td>CGST</td>
            <td>SGST</td>
            <td>IGST</td>
            <td>Total</td>
        </thead>
        <tbody> @php $i=1;  @endphp
        @foreach($order->details as $obj)
            @php $count = count($order->details); if($count == $i){ $hgt = 350 - (50*$count); if($hgt<0){$hgt=0;}    $pad=$hgt.'px';}else{$pad='10px';}

            $qty = $obj->quantity;
            $unit_price = $obj->price/$obj->quantity;
            $row_total = $obj->price;

            if($obj->parent->cgst) {$cgst = $obj->parent->cgst;}else{$cgst=0;}
            if($obj->parent->sgst) {$sgst = $obj->parent->sgst;}else{$sgst=0;}
            $total_gst = $cgst + $sgst;

            $taxable_value =(($unit_price*100)/(100+$total_gst))*$obj->quantity;

            $cgst_value = $taxable_value * ($cgst/100);
            $sgst_value = $taxable_value * ($sgst/100);



            @endphp

            <tr>
                <td style="padding-bottom:{{$pad}};">{{$i++}} </td>
                <td style="padding-bottom:{{$pad}};">{{$obj->product_variants->name}}</td>
                <td style="padding-bottom:{{$pad}};">{{$qty}}</td>
                <td style="padding-bottom:{{$pad}};">{{number_format($unit_price,2)}}</td>
                <td style="padding-bottom:{{$pad}};">{{number_format($taxable_value,2)}}</td>
                <td style="padding-bottom:{{$pad}};">{{number_format($cgst_value,2)}} {{'('.$cgst.'%)'}}</td>
                <td style="padding-bottom:{{$pad}};">{{number_format($sgst_value,2)}} {{'('.$sgst.'%)'}}</td>
                <td style="padding-bottom:{{$pad}};">{{number_format(0,2)}}</td>
                <td style="padding-bottom:{{$pad}};">{{number_format($row_total,2)}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
<br>
<div style="width: 100%;display: inline-block;">
<p style="margin: 10px;text-transform: capitalize;float: left;font-weight: bold;">{{strtolower('In Words : '.$ordertotalinwords)}}</p>
<p style="margin: 10px;float: right;font-size: 14px;font-weight: bold;">{{'Total Invoice Value : '.number_format($order->total_sale_price,2)}}</p>
</div>
<br>
<br>
<div style="width: 100%;display: inline-block;">
    <p style="margin: 10px;"><b>Payment Method : </b>{{$order->payment_method}}</p>

</div>
<br>
<br>

<div style="width: 100%;display: inline-block;border: 0.5px solid grey;border-bottom:0.5px solid grey; ">

    <div class="" style="border-bottom:1px solid grey;">
    <div style="width: 66.66%;float: left;display: inline">
        <p style="margin: 10px;">
            This is a computer generated invoice. No signature required. <br>
            "Keep this invoice for warranty purposes." <br>
            Returns Policy : Please bring the original brand box/price tag, original packing and invoice for refund. <br>
            WECARE policy : WECARE warranty is valid only for domestic and personal customer use only, <br>
            not valid on products intended for commercial, rental or profit generation purposes excluding computing products for small office/home office use
        </p>
    </div>

    <div style="width: 33.33%;float: left;display: inline">
        <p style="margin: 10px;text-align: center">
        Terms and conditions apply <br>
        E&OE <br>
        Subjected to Perumbavoor Jurisdiction
        </p>
    </div>

    </div>



</div>

</body>
