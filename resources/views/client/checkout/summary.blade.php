@extends('client.base')

@section('content')

    <div class="page-header">
        <div class="page-header__container container">
            <div class="page-header__title"><br>

            </div>
        </div>
    </div>
    <div class="block">
        <div class="container">
            <div class="row">
                <div class="col-12 offset-lg-3 col-lg-6 col-xl-6 mt-4 mt-lg-0">
                    <div class="card mb-0">
                        <div class="card-body">
                            <h3 class="card-title">Your Order</h3>
                            <table class="checkout__totals">
                                <thead class="checkout__totals-header">
                                <tr>
                                    <th>Product</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody class="checkout__totals-products">@php $total_amount = 0; @endphp
                                @foreach($cart as $o)
                                <tr>
                                    <td>{{$o->product->name}} × {{$o->quantity}}</td>
                                    <td>@if($o->price == 0) FREE @else {{'₹ '.number_format($o->price * $o->quantity)}} @endif</td>
                                </tr>  @php $total_amount = $total_amount + $o->price * $o->quantity; @endphp
                                @endforeach
                                </tbody>
                                <tbody class="checkout__totals-subtotals">
                                <tr>
                                    <th>Subtotal</th>
                                    <td>₹{{number_format($total_amount)}}</td>
                                </tr>
                                <tr>
                                    <th>Shipping</th>
                                    <td>₹0.00</td>
                                </tr>
                                @if($total['coupon_discount'])
                                    <tr>
                                        <th>Coupon discount</th>
                                        <td>- ₹{{$total['coupon_discount']}}</td>
                                    </tr>
                                @endif
                                </tbody>
                                <tfoot class="checkout__totals-footer">
                                <tr>
                                    <th>Total</th>
                                    <td>₹{{number_format($total['final_total'])}}</td>
                                </tr>
                                </tfoot>
                            </table>
                            <form action="{{url('complete')}}" method="post">
                                {{csrf_field()}}
                                <div class="payment-methods">
                                    <ul class="payment-methods__list">

                                        <li class="payment-methods__item active">
                                            <div class="payment-methods__item-header row">
                                                <div class="col-md-6">
                                                    <span class="payment-methods__item-radio input-radio"><span class="input-radio__body"><input class="input-radio__input" name="checkout_payment_method" type="radio" value="cash"> <span class="input-radio__circle"></span> </span></span><span class="payment-methods__item-title">Cash on delivery</span>
                                                </div>
                                                <div class="col-md-6">
                                                    <span class="payment-methods__item-radio input-radio"><span class="input-radio__body"><input class="input-radio__input" name="checkout_payment_method" type="radio" checked value="online"> <span class="input-radio__circle"></span> </span></span><span class="payment-methods__item-title">Pay online</span>
                                                </div>
                                            </div>
                                            <div class="payment-methods__item-container">
                                                <div class="payment-methods__item-description text-muted">Pay with cash upon delivery.</div>
                                            </div>
                                        </li>

                                    </ul>
                                </div>
                                <input type="hidden"name="amount" value="{{encrypt($total)}}">
                                <button type="submit" class="btn btn-primary btn-xl btn-block">Place Order</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('bottom')
    <script>


    </script>
@endsection


@section('head')
    <style>
        .modal-content{
            width: 100% !important;
        }
        @media (min-width: 1200px) {
            .addresses-list__item {
                max-width: unset;
                flex-basis: unset;
            }
        }
    </style>
@endsection