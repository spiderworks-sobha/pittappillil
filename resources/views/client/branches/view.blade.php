@extends('client.base')

@section('head')
<style>
    #googleMap{
            width:100%;
            height:100%;
    }
</style>
@endsection
@section('content')
    <div class="page-header">
                <div class="page-header__container container">
                    <div class="page-header__breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="{{url('/')}}">Home</a>
                                    <svg class="breadcrumb-arrow" width="6px" height="9px">
                                        <use xlink:href="{{asset('client')}}/images/sprite.svg#arrow-rounded-right-6x9"></use>
                                    </svg>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="{{url('store-locator')}}">Store Locator</a>
                                    <svg class="breadcrumb-arrow" width="6px" height="9px">
                                        <use xlink:href="{{asset('client')}}/images/sprite.svg#arrow-rounded-right-6x9"></use>
                                    </svg>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">{{$obj->branch_name}}</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="page-header__title">
                        <h1>{{$obj->page_heading}}</h1>
                    </div>
                </div>
            </div>
    @php
      $no_image = asset('images/no_image_banner.jpg');
      $no_image_profile = asset('images/default.png');
    @endphp
    <div class="block-map block " style="position: relative;">
       <div class="block " >
          <div class="container" >
             <div class="row">
                <div class="col-12 col-lg-5 mb-4">
                   <div class="card mb-0" >
                      <div class="card-body contact-us">
                         <div class="contact-us__container">
                            <div class="row">
                               <div class="col-12 col-lg-12 pb-4 pb-lg-0">
                                  <div class="contact-us__address store-det">
                                     <img src="@if($obj->contact_person_photo) {{ asset($obj->contact_person_photo->file_path) }} @else {{$no_image_profile}} @endif" onerror="this.onerror=null;this.src='{{$no_image_profile}}'" class="img-fluid" />

                                     @if($obj->description)
                                     <p>
                                        {{$obj->description}}
                                     </p>
                                     @endif
                                     <p>{{ nl2br($obj->address) }}</p>
                                     @if($obj->email)
                                     <p>
                                        Email : {{$obj->email}}
                                     </p>
                                     @endif
                                      @php
                                        $phone_numbers = [];
                                        if($obj->landline_number)
                                            $phone_numbers[] = $obj->landline_number;
                                        if($obj->mobile_number)
                                            $phone_numbers[] = $obj->mobile_number;
                                    @endphp
                                     @if($obj->phone_numbers)
                                     <p>
                                        Phone(s) : {{implode(', ', $phone_numbers)}}
                                     </p>
                                     @endif
                                     <p>
                                         @if($obj->sunday_open ==0 )Mon-Sat @endif {{date('h:iA', strtotime($obj->opening_time))}} - {{date('h:iA', strtotime($obj->closing_time))}}
                                             @if($obj->contact_person_number) Mobile : <a  href="tel:{{$obj->contact_person_number}}">{{$obj->contact_person_number}}</a> @endif

                                     </p><br>
                                      <a class="btn btn-primary" href="tel:{{$obj->contact_person_number}}"><i class="fa fa-phone" style="margin-right: 10px"></i> {{$obj->contact_person_number}}</a>

                                  </div>
                               </div>
                            </div>
                         </div>
                      </div>
                   </div>
                </div>
                <div class="col-12 col-lg-7 mb-4">
                   <div class="block-map__body locator-det-mp">
                      <div id="googleMap"></div>
                   </div>
                </div>
             </div>
          </div>
       </div>
    </div>
@endsection
@section('bottom')
@parent
<script>
    var markers = @json($branches_json);
    markers = jQuery.parseJSON(markers);

    function initMap()
    {
        var mapOptions = {
            zoom: 8,
            center: new google.maps.LatLng(10.850516, 76.271080),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);
        var infoWindow = new google.maps.InfoWindow();
        var lat_lng = new Array();
        var latlngbounds = new google.maps.LatLngBounds();
          
        var myLatlng = new google.maps.LatLng(markers.lat, markers.lng);
            lat_lng.push(myLatlng);
            var marker = new google.maps.Marker({
                            position: myLatlng,
                            map: map,
                            title: markers.title,

            });

            latlngbounds.extend(marker.position);
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent(markers.description);
                    infoWindow.open(map, marker);
                });
            })(marker, marker);

        //map.setCenter(latlngbounds.getCenter());
        //map.fitBounds(latlngbounds);
            
    }

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKNmMGKJYRzfsMF5Jom1Rj5-VPIoyLbak&libraries=places&callback=initMap" type="text/javascript"></script>
@endsection

