@extends('client.base')

@section('head')
<style>
    #googleMap{
            width:100%;
            height:100%;
    }
    .site-footer {
        margin-top: 0px;
    }
    .block {
        margin-bottom: 0px;
    }
</style>
@endsection
@section('content')

    <div class="page-header">
        <div class="page-header__container container">
            <div class="page-header__breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{url('/')}}">Home</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="{{asset('client')}}/images/sprite.svg#arrow-rounded-right-6x9"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Store Locator</li>
                    </ol>
                </nav>
            </div>
            <div class="page-header__title">
                <h1>Store Locator</h1>
            </div>
        </div>
    </div>
    <div class="block-map block " style="position: relative;">
   <div class="block-map__body locator-img">
      <div id="googleMap"></div>
   </div>
   <div class="block locator-list" >
      <div class="container" >
         <div class="card mb-0" >
            <div class="card-body contact-us">
               <div class="contact-us__container">
                  <div class="row">
                     <div class="col-12 col-lg-12 mb-4">
                        <form action="{{url('store-locator')}}" class="m-3" method="POST">
                            @csrf
                            <div class="form-row">
                              <div class="form-group col-md-12">
                                 <select class="form-control" placeholder="Select Districts" id="searchDistrict" name="district">
                                    <option value="">Select District</option>
                                    @if(count($districts)>0)
                                        @foreach($districts as $district)
                                            <option value="{{$district->id}}" @if($district->id == $selected_district) selected="selected" @endif>{{$district->district_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                              </div>
                            </div>
                            <div class="form-row">
                              <div class="form-group col-md-12">
                                    <select class="form-control" placeholder="Select City" id="searchLocation" name="location">
                                        <option value="">Select Location</option>
                                        @if($locations)
                                            @foreach($locations as $location)
                                                <option value="{{$location->id}}" @if($location->id == $selected_location) selected="selected" @endif>{{$location->landmark}}</option>
                                            @endforeach
                                        @else
                                            <option value="">[Select a district first]</option>
                                        @endif
                                    </select>
                              </div>
                           </div>
                           <button type="submit" class="btn btn-primary float-right">Search</button>
                        </form>
                     </div>
                     <div class="col-12 col-lg-12 pb-4 pb-lg-0">
                        <div class="contact-us__address address-list">
                            @if(count($branches)>0)
                                @foreach($branches as $key=> $branch)
                                    <div>
                                        <a href="{{url('store-locator', [$branch->slug])}}">
                                            <span> {{$key+1}} </span>
                                            <p>
                                                <strong class="title">{{$branch->page_heading}}</strong><br/>
                                                {!! nl2br($branch->address) !!}
                                             </p>
                                        </a>
                                    </div>
                                    @if(count($branches) != $key+1)
                                        <hr/>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('bottom')
@parent
<script>
    var markers = @json($branches_json);
    markers = jQuery.parseJSON(markers);

    function initMap()
    {
        var mapOptions = {
            zoom: 9,
            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);
        var infoWindow = new google.maps.InfoWindow();
        var lat_lng = new Array();
        var latlngbounds = new google.maps.LatLngBounds();
          
        for (i = 0; i < markers.length; i++) {
            var data = markers[i];
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            lat_lng.push(myLatlng);
            var marker = new google.maps.Marker({
                            position: myLatlng,
                            map: map,
                            title: data.title,

            });

            latlngbounds.extend(marker.position);
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent(data.description);
                    infoWindow.open(map, marker);
                });
            })(marker, data);
        }

        map.setCenter(latlngbounds.getCenter());
        map.fitBounds(latlngbounds);
            
    }

    $(function(){
        $(document).on('change', '#searchDistrict', function(){
            var type_id = $(this).val();
            var target = $('#searchLocation');
            var options = '<option value="">[ Select a district first ]</option>';
            if(parseInt(type_id)>0){
                target.html('<option value="">Loading...</option>');
                $.getJSON(baseUrl  + '/ajax/locations/' + type_id, function(jsonData){
                    if ( jsonData.length != 0 ) {
                        options = '<option value="">--- Select a location ---</option>';
                        $.each(jsonData, function(i,data) {
                            options +='<option value="'+data.id+'">'+data.text+'</option>';
                        });
                    } else {
                        options = '<option value="">[No locations listed in this district]</option>';
                    }
                    target.html(options).change();
                });
            } else {
                target.html(options).change();
            }
        })
    })

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKNmMGKJYRzfsMF5Jom1Rj5-VPIoyLbak&libraries=places&callback=initMap" type="text/javascript"></script>
@endsection
