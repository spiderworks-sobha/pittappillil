<div class="mobilemenu__content">
    <ul class="mobile-links mobile-links--level--0" data-collapse data-collapse-opened-class="mobile-links__item--open">
        @widget('MobileMenu', ['menu_position' => 'Main Menu'])
    </ul>
</div>