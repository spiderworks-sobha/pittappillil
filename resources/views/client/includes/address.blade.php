@if($address)
   @if($address->is_default == 1 && $from == 'home')
   <div class="address-card__badge">Default Address</div>
   @elseif($address->is_default == 1)
   <div class="address-card__badge">Default</div>
   @endif
   <div class="address-card__body" id="card-body-{{$address->id}}">
      <div class="address-card__name">{{$address->full_name}}</div>
      <div class="address-card__row">
         {{$address->address1}}
         <br/>
         @if($address->address2)
            {{$address->address2}}
            <br/>
         @endif
         @if($address->landmark)
            {{$address->landmark}}
            <br/>
         @endif
         {{$address->city}}, 
         {{$address->state_name}},
         {{$address->pincode}}
      </div>
      <div class="address-card__row">
         <div class="address-card__row-title">Phone Number</div>
         <div class="address-card__row-content">{{$address->phone}}</div>
      </div>
      <div class="address-card__row">
         <div class="address-card__row-title">Address Type</div>
         @if($address->type == 1)
            <div class="address-card__row-content">Home</div>
         @else
            <div class="address-card__row-content">Work</div>
         @endif
      </div>
      <div class="address-card__row" @if($from != 'cart') style="display:none" @endif>
         <a href="{{url('account/address/add-delivery-instructions', [$address->id])}}" data-target="#common-modal" class="address-deliver show-modal text-secondary" style="text-decoration:underline;" id="address-deliver-{{$address->id}}" data-id="{{$address->id}}" >Add delivery instructions</a>
      </div>
      <div class="address-card__footer">
         @if($from == 'home')
            <a href="{{url('account/address', ['home', $address->id])}}" class="show-modal" data-target="#common-modal">Edit Address</a>
         @else
            <a href="{{url('account/address', ['address', $address->id])}}" class="show-modal" data-target="#common-modal">Edit</a>
            <a href="javascript:void(0)" class="address-list-remove" id="address-list-remove-{{$address->id}}" data-id="{{$address->id}}" @if($address->is_default == 1) style="display:none" @endif>&nbsp;|&nbsp;Remove</a>
            <a href="javascript:void(0)" class="address-list-default" id="address-list-default-{{$address->id}}" data-id="{{$address->id}}" @if($address->is_default == 1) style="display:none" @endif>&nbsp;|&nbsp;Make Default</a>
            
         @endif
            @if($from == 'cart')
            <div align="center" onclick="deliverHere({{$address->id}})" style="cursor:pointer;background: rgb(136, 111, 168); width: 100%; padding: 10px; color: white; font-weight: bold;">
               Deliver Here
            </div>
            @endif

      </div>
   </div>
@endif