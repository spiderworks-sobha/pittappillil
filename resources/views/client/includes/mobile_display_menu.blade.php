@foreach($items as $key=>$item)
	<li class="mobile-links__item" data-collapse-item>
            <div class="mobile-links__item-title">
                <a href="{{$item->slug}}" class="mobile-links__item-link">{{$item->title}}</a>
                @if(isset($item->children))
                <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
                    <svg class="mobile-links__item-arrow" width="12px" height="7px">
                        <use xlink:href="{{URL::asset('client')}}/images/sprite.svg#arrow-rounded-down-12x7"></use>
                    </svg>
                    </svg>
                </button>
                @endif
            </div>
            @if(isset($item->children))
            <div class="mobile-links__item-sub-links" data-collapse-content>
                <ul class="mobile-links mobile-links--level--1">
                    @include('client.includes.mobile_display_menu', ['items'=>$item->children, 'depth'=>++$depth])
                </ul>
            </div>
            @endif
        </li>
@endforeach