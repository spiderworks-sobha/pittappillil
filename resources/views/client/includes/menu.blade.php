@foreach($items as $key=>$item)
	<li @if($depth == 0) class="nav-links__item @if(isset($item->children)) nav-links__item--has-submenu @endif" @else class="menu__item" @endif>

		@if($depth == 0)
			<a class="nav-links__item-link" href="{{$item->slug}}">
                <div class="nav-links__item-body">
                    {{$item->title}}
                    @if(isset($item->children))
                        <svg class="nav-links__item-arrow" width="9px" height="6px">
                            <use xlink:href="{{URL::asset('client')}}/images/sprite.svg#arrow-rounded-down-9x6"></use>
                        </svg>
                    @endif
                </div>
            </a>
        @else
        	<div class="menu__item-submenu-offset"></div>
            <a class="menu__item-link" href="{{$item->slug}}">
                {{$item->title}}
                @if(isset($item->children))
	                <svg class="menu__item-arrow" width="6px" height="9px">
	                    <use xlink:href="{{URL::asset('client')}}/images/sprite.svg#arrow-rounded-right-6x9"></use>
	                </svg>
	            @endif
            </a>
		@endif
        @if(isset($item->children))
        	@if($type == 1 && $depth == 0)
        		<div class="nav-links__submenu nav-links__submenu--type--megamenu nav-links__submenu--size--nl">
                                        <!-- .megamenu -->
                                        <div class="megamenu ">
                                            <div class="megamenu__body">
                                                <div class="row">
                                                	@foreach($item->children as $key=>$megamenu_root_child)
                                                		@if($key == 0 || round(count($item->children)/2)== $key)
                                                			<div class="col-6">
                                                				<ul class="megamenu__links megamenu__links--level--0">
                                                		@endif
                                                			<li class="megamenu__item  megamenu__item--with-submenu ">
                                                                <a href="{{$megamenu_root_child->slug}}">{{$megamenu_root_child->title}}</a>
                                                                @if(isset($megamenu_root_child->children))
                                                                <ul class="megamenu__links megamenu__links--level--1">
                                                                	@foreach($megamenu_root_child->children as $megamenu_child)
                                                                    	<li class="megamenu__item"><a href="{{$megamenu_child->slug}}">{{$megamenu_child->title}}</a></li>
                                                                    @endforeach
                                                                </ul>
                                                                @endif
                                                            </li>
                                                		@if($key+1 == count($item->children) || round(count($item->children)/2)== $key+1)
                                                				</ul>
                                                			</div>
                                                		@endif
                                                	@endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <!-- .megamenu / end -->
                                    </div>
        	@else
	        	<div @if($depth == 0) class="nav-links__submenu nav-links__submenu--type--menu" @else class="menu__submenu" @endif>
	                <!-- .menu -->
	                <div class="menu menu--layout--classic ">
	                    <div class="menu__submenus-container"></div>
	                    <ul class="menu__list">
	                    	@include('client.includes.menu', ['items'=>$item->children, 'depth'=>++$depth, 'type'=>$type])
	                    </ul>
	                </div>
	            </div>
            @endif
        @endif
	<li>
@endforeach
