<footer class="site__footer">
    <div class="site-footer">
        <div class="container">
            <div class="site-footer__widgets">
                <div class="row">
                    @if(isset($common_settings['footer-contact']))
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="site-footer__widget footer-contacts">
                            <h5 class="footer-contacts__title">{{ $common_settings['footer-contact']->title }}</h5>
                            {!! $common_settings['footer-contact']->contact_details !!}
                        </div>
                    </div>
                    @endif
                    <div class="col-6 col-md-3 col-lg-2">
                        <div class="site-footer__widget footer-links">
                            <h5 class="footer-links__title">{{$common_settings['footer-links-group1']}}</h5>
                            <ul class="footer-links__list">
                                @widget('BottomMenu', ['name'=>'footer1'])
                            </ul>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 col-lg-2">
                        <div class="site-footer__widget footer-links">
                            <h5 class="footer-links__title">{{$common_settings['footer-links-group2']}}</h5>
                            <ul class="footer-links__list">
                                @widget('BottomMenu', ['name'=>'footer2'])
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-4">
                        <div class="site-footer__widget footer-newsletter">
                            <h5 class="footer-newsletter__title">{{$common_settings['newsletter-head']}}</h5>
                            <div class="footer-newsletter__text">
                                {{$common_settings['newsletter-content']}}
                            </div>
                            <form action="{{url('newsletter/save')}}" method="POST" class="footer-newsletter__form" id="newsletterFrm">
                                @csrf
                                <label class="sr-only" for="footer-newsletter-address">Email Address</label>
                                <input type="text" name="email" class="footer-newsletter__form-input form-control" id="footer-newsletter-address" placeholder="Email Address...">
                                <button class="footer-newsletter__form-button btn btn-primary" id="subscribe-btn">Subscribe</button>
                            </form>
                            <span class="text-danger" id="email_error"></span>
                            @if(isset($common_settings['footer-contact']))
                                <div class="footer-newsletter__text footer-newsletter__text--social">
                                    {{ $common_settings['social-media']->title }}
                                </div>
                                <ul class="footer-newsletter__social-links">
                                    @foreach($common_settings['social-media']->social_media as $key => $link)
                                        @if($key == 'facebook' && $link != '')
                                            <li class="footer-newsletter__social-link footer-newsletter__social-link--facebook"><a href="{{$link}}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                        @endif
                                        @if($key == 'twitter' && $link != '')
                                        <li class="footer-newsletter__social-link footer-newsletter__social-link--twitter"><a href="{{$link}}" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                        @endif
                                        @if($key == 'youtube' && $link != '')
                                            <li class="footer-newsletter__social-link footer-newsletter__social-link--youtube"><a href="{{$link}}" target="_blank"><i class="fab fa-youtube"></i></a></li>
                                        @endif
                                        @if($key == 'instagram' && $link != '')
                                            <li class="footer-newsletter__social-link footer-newsletter__social-link--instagram" ><a href="{{$link}}" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                        @endif
                                        @if($key == 'linkedin' && $link != '')
                                            <li class="footer-newsletter__social-link footer-newsletter__social-link--linkedin"><a href="{{$link}}" target="_blank" style="background: #007bb6"><i class="fab fa-linkedin-in"></i></a></li>
                                        @endif
                                        @if($key == 'pinterest' && $link != '')
                                        <li class="footer-newsletter__social-link footer-newsletter__social-link--pinterest"><a href="{{$link}}" target="_blank" style="background: #bb081d"><i class="fab fa-pinterest-p"></i></a></li>
                                        @endif
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="site-footer__bottom">
                <div class="site-footer__copyright">
                    <p style="color: #868686">Site Designed &amp; Developed by <a href="https://www.spiderworks.in/" style="color: #868686" target="_blank">SpiderWorks</a> All Rights Reserved | Copyright <a href="http://dev.pittappillilonline.com" style="color: #868686" target="_blank">Pittappillil Agencies</a> {{date('Y')}} </p>
                </div>

            </div>
        </div>
    </div>
</footer>