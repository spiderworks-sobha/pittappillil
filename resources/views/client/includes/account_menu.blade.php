<ul>
                                    <li class="account-nav__item  @if(Route::current()->getName() == 'account') account-nav__item--active @endif">
                                        <a href="{{route('account')}}">Dashboard</a>
                                    </li>
                                    <li class="account-nav__item  @if(Route::current()->getName() == 'account.edit-profile') account-nav__item--active @endif">
                                        <a href="{{route('account.edit-profile')}}">Edit Profile</a>
                                    </li>
                                    <li class="account-nav__item  @if(Route::current()->getName() == 'account.orders' || Route::current()->getName() == 'account.orders-details') account-nav__item--active @endif">
                                        <a href="{{route('account.orders')}}">Orders</a>
                                    </li>
                                    <li class="account-nav__item  @if(Route::current()->getName() == 'account.addresses') account-nav__item--active @endif">
                                        <a href="{{route('account.addresses')}}">Addresses</a>
                                    </li>
                                    <li class="account-nav__item">
                                        <a href="javascript::void(0);" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                           @csrf
                                        </form>
                                    </li>
                                </ul>