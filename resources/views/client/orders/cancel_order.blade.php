<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Cancel Order</h5>
    <a  class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </a>
</div>
<div class="modal-body">
	<form action="{{url('account/cancel-order/save')}}" id="CancelFrm" method="post" class="form-horizontal style-form" enctype="multipart/form-data">
	    @csrf
	    <input type="hidden" name="id" value="{{encrypt($order_id)}}">
	    <div class="form-group">
	      	<label>Cancel Type</label>
	        <select class="form-control select2_input w-100" data-placeholder="Select Cancel Reason" name="cancel_reason" id="cancelReasonInput" data-parent="#common-modal .modal-body">
	        	@if(count($cancel_reasons)>0)
	        		@foreach($cancel_reasons as $reason)
	        			<option value="{{$reason->title}}">{{$reason->title}}</option>
	        		@endforeach
	        	@endif
	        	<option value="Other">Other</option>
	        </select>
	        <span id="type_error"></span>
	    </div>
	    <div class="form-group" style="display: none;" id="specify-reason-section">
	      	<label>Specify Reason</label>
	        <textarea class="form-control" name="other_reason" id="other_reason"></textarea>
	        <span id="address1_error"></span>
	    </div>
	    <div class="form-group">
            <button class="btn btn-primary" type="button" id="cancel-reason-save-btn" data-id="{{$order_id}}">Save</button>
       	</div>
	</form>
</div>