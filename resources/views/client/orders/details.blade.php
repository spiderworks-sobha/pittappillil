@extends('client.base')

@section('head')
    <link rel="stylesheet" href="{{asset('assets/css/progress-tracker.css')}}">
    <style type="text/css">
        .display_notes{
            border: 1px solid #dad7d7;
        }

        .progress-step {
            flex: 1 1 11%;
        }


        .progress-text {
            margin-left: -21px !important;

        }

    </style>
@endsection

@section('content')
@php
  $no_image = asset('images/no_image.jpeg');
@endphp
    <div class="page-header">
                <div class="page-header__container container">
                    <div class="page-header__breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="{{url('/')}}">Home</a>
                                    <svg class="breadcrumb-arrow" width="6px" height="9px">
                                        <use xlink:href="{{asset('client')}}/images/sprite.svg#arrow-rounded-right-6x9"></use>
                                    </svg>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="{{url('account/dashboard')}}">My Account</a>
                                    <svg class="breadcrumb-arrow" width="6px" height="9px">
                                        <use xlink:href="{{asset('client')}}/images/sprite.svg#arrow-rounded-right-6x9"></use>
                                    </svg>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="{{url('account/orders')}}">Orders</a>
                                    <svg class="breadcrumb-arrow" width="6px" height="9px">
                                        <use xlink:href="{{asset('client')}}/images/sprite.svg#arrow-rounded-right-6x9"></use>
                                    </svg>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">{{$order->order_reference_number}}</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="page-header__title">
                        <h1>Track Order</h1>
                    </div>
                </div>
            </div>
            <div class="block">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-lg-3 d-flex">
                            <div class="account-nav flex-grow-1">
                                <h4 class="account-nav__title">Navigation</h4>
                                @include('client.includes.account_menu')
                            </div>
                        </div>
                        <div class="col-12 col-lg-9 mt-4 mt-lg-0">
                            <div class="card mb-2">
                              <div class="card-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <h5>Delivery Address</h5>
                                        <h6>{{$order->delivery_address->full_name}}</h6>
                                        <p>
                                            {{$order->delivery_address->address1}}, 
                                            @if($order->delivery_address->address2)
                                                {{$order->delivery_address->address2}}, 
                                             @endif
                                             @if($order->delivery_address->landmark)
                                                {{$order->delivery_address->landmark}}, 
                                             @endif
                                             {{$order->delivery_address->city}}, 
                                             {{$order->delivery_address->state_details->name}},
                                             {{$order->delivery_address->pincode}}
                                        </p>
                                        <p>Phone Number {{$order->delivery_address->phone}}</p>
                                        @if($order->delivery_instructions)
                                            <small><b>Delivery Instructions: </b>{{$order->delivery_instructions}}</small>
                                        @endif
                                    </div>
                                    <div class="col-md-4">
                                        <h6>{{$order->order_reference_number}}</h6>
                                        <p>Order Placed on {{date('d M, Y h:i A', strtotime($order->created_at))}}</p>
                                    </div>
                                </div>
                              </div>
                            </div>
                            <div class="card">
                              <div class="card-body p-3">
                                @if(count($order->details)>0)
                                    @foreach($order->details as $key=> $item)
                                        <div class="row">
                                            @if($item->status == $status->initial_status)
                                            <div class="col-md-12 text-right" id="cancel-holder-{{$item->id}}">
                                                @if($item->is_cancelled !=1)
                                                    @if($item->status != $status->cancel_request_status)
                                                        <a href="{{route('account.orders.cancel-order', [$item->id])}}" data-target="#common-modal" class="show-modal"><small>Cancel Order</small></a>
                                                    @else
                                                        <small class="text-danger">Requested for cancel</small>
                                                    @endif
                                                @else
                                                    <small class="text-danger">Cancelled</small>
                                                @endif
                                            </div>
                                            @endif
                                            <div class="col-md-5">
                                                <div class="media  p-3">
                                                  <img src="@if($item->product_variants->image_id && $item->product_variants->media) {{ asset('public/'.$item->product_variants->media->thumb_file_path) }} @else {{$no_image}} @endif" onerror="this.onerror=null;this.src='{{$no_image}}'" style="max-width:75px;" class="mr-3">
                                                  <div class="media-body">
                                                    <h6><a href="{{url($item->product_variants->slug)}}" class="text-dark">{{str_limit($item->product_variants->name, $limit = 50, $end = '...')}}</a></h6>
                                                    @if($item->product_variants->attribute_level1)
                                                        <p class="m-0">
                                                                    <small>
                                                                        {{$item->product_variants->attribute_level1->attribute->attribute_name}}: 
                                                                        {{$item->product_variants->attribute_level1->value}}
                                                                    </small>
                                                        </p>
                                                    @endif
                                                    @if($item->product_variants->attribute_level2)
                                                        <p class="m-0">
                                                                    <small>
                                                                        {{$item->product_variants->attribute_level2->attribute->attribute_name}}: 
                                                                        {{$item->product_variants->attribute_level2->value}}
                                                                    </small>
                                                        </p>
                                                    @endif
                                                    @if($item->product_variants->attribute_level3)
                                                        <p class="m-0">
                                                                    <small>
                                                                        {{$item->product_variants->attribute_level3->attribute->attribute_name}}: 
                                                                        {{$item->product_variants->attribute_level3->value}}
                                                                    </small>
                                                        </p>
                                                    @endif
                                                    <p class="m-0"><small>Quantity: {{$item->quantity}}</small></p>

                                                  </div>
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                @if(count($order_status)>0)
                                                @php
                                                    $completed_status = $item->tracking_history->pluck('order_status_labels_master_id')->toArray();
                                                    $completed_status_notes = $item->tracking_history->pluck('notes', 'order_status_labels_master_id')->toArray();
                                                @endphp
                                                <ul class="progress-tracker mt-3">
                                                    @foreach($order_status as $o_status)
                                                        @php
                                                            $status_class = '';
                                                            if($o_status->id == $item->status)
                                                                $status_class = 'is-active';
                                                            elseif(in_array($o_status->id, $completed_status))
                                                                $status_class = 'is-complete';
                                                        @endphp
                                                      <li class="progress-step {{$status_class}}">
                                                        <div class="progress-marker" data-title="{{ isset($completed_status_notes[$o_status->id])?$completed_status_notes[$o_status->id]:''}}"></div>
                                                        <div class="progress-text">
                                                          <p class="progress-title">{{$o_status->name}}</p>
                                                        </div>
                                                      </li>
                                                    @endforeach
                                                </ul>
                                                
                                                @endif
                                            </div>
                                        </div>
                                        @if(count($order->details) != $key+1)
                                        <div class="card-divider"></div>
                                        @endif
                                    @endforeach
                                @endif
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


@endsection
@section('bottom')

    @parent
    <script src="{{asset('assets/js/progress-tracker.js')}}"></script>
    <script>
    $( ".progress-marker" ).mouseenter(function() {
        var obj = $(this);
        if(obj.data('title') != '')
        {
            var html ='<div class="display_notes p-2">'+obj.data('title')+'</div>';
            obj.parents('ul').after(html);
        }
    });
    $( ".progress-marker" ).mouseleave(function() {
        var obj = $(this);
        obj.parents('ul').next('.display_notes').remove();
    });

    $(document).on('change', '#cancelReasonInput', function(){
        if($(this).val() == 'Other')
        {
            $('#specify-reason-section').show();
        }
        else
        {
            $('#specify-reason-section').hide();
        }
    });

    function validateCancel()
    {
        $('#CancelFrm').validate({
          rules:
          {
            cancel_reason: "required",
            other_reason: {
                required: function(element){
                    return $("#cancelReasonInput").val() =="Other";
                }
            }
          },
          messages:
          {
            cancel_reason: "Please section a cancel reason",
            other_reason: "Specify cancel reason",
          },
          errorPlacement: function (error, element) {
            error.insertAfter($(element).parent('div').find('span'));
          }
      });
    }

    $(document).on('click', '#cancel-reason-save-btn', function(){
        var obj = $(this);
        validateAddress();
        var id = $(this).data('id');
        var frmValid = $('#CancelFrm').valid();
        if(frmValid)
        {
            obj.prop('disabled', true);
            obj.html('Saving..');
            var formurl = $('#CancelFrm').attr('action');
            $.ajax({
              url: formurl, 
              type: "POST", 
              data: new FormData($('#CancelFrm')[0]),
              cache: false, 
              processData: false,
              contentType: false, 
              success: function(data) {
                obj.prop('disabled', false);
                obj.html('Save');

                if (typeof data.errors != "undefined") {
                  var errors = JSON.parse(JSON.stringify(data.errors))
                        $.each(errors, function (key, val) {
                            $("#" + key + "_error").text(val[0]);
                        });
                }
                else
                {
                  $('#common-modal').modal('hide');
                  $('body').removeClass('modal-open');
                  $('.modal-backdrop').remove();
                  $('#cancel-holder-'+id).html('<small class="text-danger">Requested for Cancel</small>');
                  Toast.fire({
                    title: 'Success!',
                    text: data.success,
                    icon: 'success',
                  });
                }
              },
              error:function(reject){
                  obj.prop('disabled', false);
                  obj.html('Save');
                  if( reject.status === 422 ) {
                        var errors = $.parseJSON(reject.responseText);
                        $.each(errors, function (key, val) {
                            $("#" + key + "_error").text(val[0]);
                        });
                  }
              }
            });
        }
      })
     
</script>
@endsection