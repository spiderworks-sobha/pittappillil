@extends('client.base')

@section('head')
    <style>
        .block {
            margin-bottom: 0px;
        }
        .site-footer{
            margin-top: 0px !important;
        }
    </style>
@endsection
@section('content')


    <div class="page-header">
      <div class="page-header__container container">
         <div class="page-header__breadcrumb">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                     <a href="{{url('/')}}">Home</a> 
                     <svg class="breadcrumb-arrow" width="6px" height="9px">
                        <use xlink:href="{{asset('client')}}/images/sprite.svg#arrow-rounded-right-6x9"></use>
                     </svg>
                  </li>
                  <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
               </ol>
            </nav>
         </div>
         <div class="page-header__title">
            <h1>Contact Us</h1>
         </div>
      </div>
   </div>
   <div class="block">
      <div class="container">
         <div class="card mb-0">
            <div class="card-body contact-us">
               <div class="contact-us__container">
                  <div class="row">
                     <div class="col-12 col-lg-6 pb-4 pb-lg-0">
                        <h4 class="contact-us__header card-title">{{$contact_data->title}}</h4>
                        <div class="contact-us__address">
                            {!! $contact_data->contact_details !!}
                            <ul class="footer-contacts__contacts">
                                <li>
                                    <a href="{{url('store-locator')}}" class="text-danger">Locate all Branches</a>
                                </li>
                            </ul>
                        </div>
                     </div>
                     <div class="col-12 col-lg-6">
                        <h4 class="contact-us__header card-title">Leave us a Message</h4>
                        <form action="{{url('contact-us/save')}}" method="POST" id="contactFrm">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="form-name">Your Name</label> 
                                    <input type="text" name="name" id="form-name" class="form-control" placeholder="Your Name">
                                    <span class="text-danger ajax-error" id="name_contact_error"></span>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="form-email">Email</label> 
                                    <input type="email" name="email" id="form-email" class="form-control" placeholder="Email Address">
                                    <span class="text-danger ajax-error" id="email_contact_error"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="form-subject">Subject</label> 
                                <input type="text" name="subject" id="form-subject" class="form-control" placeholder="Subject">
                            </div>
                            <div class="form-group">
                                <label for="form-message">Message</label> 
                                <textarea id="form-message" class="form-control" rows="4" name="message"></textarea>
                                <span class="text-danger ajax-error" id="message_contact_error"></span>
                            </div>
                            <button type="submit" class="btn btn-primary" id="contact-btn">Send Message</button>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

       <div class="block-map block" style="margin-top: 50px">
           <div class="block-map__body">
               {!! $contact_data->googlemap_iframe !!}
           </div>
       </div>
@endsection
@section('bottom')
@parent
    <script type="text/javascript">
        $(function(){
            $('#contactFrm').validate({
                rules:
                {
                    name: "required",
                    email:
                    {
                        required: true,
                        email: true,
                    },
                    message: "required",
                },
                messages:
                {
                    name: "Please enter your name",
                    email:
                    {
                        required:"Please enter an email address.",
                        email:"Please enter a valid email address."
                    },
                    message: "Please enter your message",
                },
                errorPlacement: function (error, element) {
                    $(".ajax-error").html('');
                    $(element).parent('form').next('.error').remove();
                    error.addClass('text-danger').insertAfter($(element));
                },
                submitHandler:function(form)
                {
                    $('#contact-btn').text('Processing...');
                    $('#contact-btn').prop('disabled', true);
                        var formurl = $('#contactFrm').attr('action');
                        $.ajax({
                            url: formurl ,
                            type: "POST", 
                            data: new FormData($('#contactFrm')[0]),
                            cache: false, 
                            processData: false,
                            contentType: false, 
                            success: function(data) {
                                $('#contact-btn').text('Send Message');
                                $('#contact-btn').prop('disabled', false);
                                if (typeof data.errors != "undefined") {
                                  var errors = JSON.parse(JSON.stringify(data.errors))
                                        $.each(errors, function (key, val) {
                                            $("#"+key+"_contact_error").html(val);
                                        });
                                }
                                else
                                {
                                    $(".error").remove();
                                    $("#contactFrm")[0].reset();
                                    Toast.fire({
                                        title: 'Success!',
                                        text: 'Thank you for contacting us, we will reach back to you in a short time!',
                                        icon: 'success',
                                    });
                                }
                            },
                            error:function(xhr){
                                $('#contact-btn').text('Send Message');
                                $('#contact-btn').prop('disabled', false);
                                var errors = $.parseJSON(xhr.responseText);
                                $.each(errors, function (key, val) {
                                    $("#" + key + "_error").text(val);
                                });
                            }
                        });
                }
            });
        })
    </script>
@endsection
