@extends('client.base')

@section('content')
    <div class="site__body">
        <div class="page-header">
            <div class="page-header__container container">
                <div class="page-header__breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{URL::to('/')}}">Home</a>
                                <svg class="breadcrumb-arrow" width="6px" height="9px">
                                    <use xlink:href="images/sprite.svg#arrow-rounded-right-6x9"></use>
                                </svg>
                            </li>
                            @if($product->category)
                            <li class="breadcrumb-item">
                                <a href="{{URL::to('stores/'.$product->category_slug)}}">{{$product->category}}</a>
                                <svg class="breadcrumb-arrow" width="6px" height="9px">
                                    <use xlink:href="images/sprite.svg#arrow-rounded-right-6x9"></use>
                                </svg>
                            </li>
                            @endif
                            <li class="breadcrumb-item active" aria-current="page">{{$product->product_name}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="block">
            <div class="container">

                <div class="product product--layout--columnar" data-layout="columnar">
                    <productsingle product_id="{{$product->pid}}" variant_id="{{$product->id}}" @if($product->rating) rating="{{$product->rating}}" @endif  @if($product->rating_count) rating_count="{{$product->rating_count}}" @endif  @if(session('pincode')) pincode_="{{session('pincode')}}" @endif></productsingle>
                </div>
                <div class="product-tabs ">
                    <div class="product-tabs__list">
                        @if($product->top_description || $product->bottom_description)<a href="#tab-description" class="product-tabs__item product-tabs__item--active">Description</a> @endif
                        <a href="#tab-specification" class="product-tabs__item">Specification</a>
                        @if($reviews)<a href="#tab-reviews" class="product-tabs__item">Reviews</a>@endif
                    </div>
                    <div class="product-tabs__content">
                        <div class="product-tabs__pane product-tabs__pane--active" id="tab-description">
                            <div class="typography">
                                @if($product->top_description || $product->bottom_description)
                                <h3>Product Description</h3>
                                    {!! $product->top_description !!}
                                    {!! $product->bottom_description !!}
                                @endif
                            </div>
                        </div>
                        <div class="product-tabs__pane" id="tab-specification">
                            <div class="spec">
                                <h3 class="spec__header">Specification</h3>
                                <specifications id="{{$product->id}}"></specifications>
                            </div>
                        </div>
                        <div class="product-tabs__pane" id="tab-reviews">
                            <div class="reviews-view">
                                <div class="reviews-view__list">
                                    @if($reviews)
                                    <h3 class="reviews-view__header">Customer Reviews</h3>
                                    <div class="reviews-list">
                                        <reviews id="{{$reviews->products_id}}"></reviews>
                                    </div>
                                    @endif
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- .block-products-carousel -->
        <related-products id="{{$product->id}}"></related-products>
        <!-- .block-products-carousel / end -->




    </div>

    <div class="fixed_compare" style="display: none">
        <div class="fc--inner">

            <div class="fci_products" id="dialog" title="Basic dialog">
                <div class="remove_all close-btn-all">
                    <span class="remove-all" style="cursor: pointer" data-compare="{{$product->id}}">REMOVE ALL</span>
                </div>
                <div class="compare-btn-content"></div>
            </div>

            <div class="compare_opt">
                <a class="pro_compare" href="{{url('compare/'.$product->category_slug)}}">
                <span class="pc_btn_left pc_text">
                    <span>COMPARE</span>
                </span>
                    <span class="pc_btn_right pc_text">
                    <span class="pc_btn_right--digit comparecount"></span>
                </span>
                </a>
            </div>
        </div>
    </div>

@endsection

@section('bottom')
    <script>

        function comapre_html(title,image,url,vid) {
            var compare_btn_content = $('.compare-btn-content');
            var html = [
                ' <div class="fc_inner--single popup_'+vid+'">\n' +
                '                    <div class="fis_pro">\n' +
                '                        <div class="fis_close close-btn-single " ><span class="remove-from-compare" data-popup="true" data-compare="'+vid+'">✕</span></div>\n' +
                '                        <div class="fis_pro--thumb">\n' +
                '                            <img alt=""\n' +
                '                                 src="'+image+'" style="display: block">\n' +
                '                        <a href="'+url+'">'+title+'</a></div>\n' +
                '                    </div>\n' +
                '                </div>'
            ].join("\n");
            compare_btn_content.append(html)
            return true;

        }
        function get_compare_list(variant_id) {
            $('.compare-btn-content').html('');
            var data = {
                _token : csrf,
                variant_id : variant_id
            }


            $.post(baseUrl+'/compare/list',data).done(function (data) {
                var data = JSON.parse(data);
                data.forEach(function(entry) {


                    comapre_html(entry.title,entry.image,baseUrl+'/'+entry.slug,entry.vid);
                    $('.fixed_compare').fadeIn();
                });

                    $('.comparecount').html(data.length)

                    if(data.length == 0){
                        $('.fixed_compare').fadeOut();
                    }

            })

        }

        $(document).ready(function() {
            $(document).on("click",".add-to-compare",function() {

                var dom = $(this);
                var variant_id = dom.data('compare');

                var data = {
                    _token : csrf,
                    variant_id : variant_id,
                    type: 'add'
                }
                $.post(baseUrl+'/compare',data).done(function (data) {
                    var data = JSON.parse(data);
                    if(data.status == true) {
                        dom.parent().html('<div class="remove-from-compare" data-compare="'+variant_id+'" style="text-decoration: underline; cursor: pointer;"><span style="color: red;"><i class="fa fa-clone"></i></span> Remove from compare\n' +
                            '            </div>');
                        get_compare_list(variant_id);
                    }else{
                        alert('Something went wrong')
                    }
                });

            });



            $(document).on("click",".remove-from-compare",function() {


                var dom = $(this);
                var popup = dom.data('popup');
                var variant_id = dom.data('compare');

                var data = {
                    _token : csrf,
                    variant_id : variant_id,
                    type: 'remove'
                }

                $.post(baseUrl+'/compare',data).done(function (data) {
                    var data = JSON.parse(data);
                    if(data.status == true){
                        get_compare_list(variant_id);
                        $('.compare-spot').html('<div class="add-to-compare" data-compare="'+variant_id+'" style="text-decoration: underline; cursor: pointer;"><i class="fa fa-clone"></i> Add to compare\n' +
                            '                </div>')
                    }else{
                        alert(data.status)
                    }
                });
            });

            $(document).on("click",".remove-all",function() {


                var dom = $(this);
                var variant_id = dom.data('compare');

                var data = {
                    _token : csrf,
                    variant_id : variant_id,
                    type: 'remove-all'
                }

                $.post(baseUrl+'/compare',data).done(function (data) {
                    var data = JSON.parse(data);
                    if(data.status == true){
                        $('.fixed_compare').fadeOut();
                        $('.compare-spot').html('<div class="add-to-compare" data-compare="'+variant_id+'" style="text-decoration: underline; cursor: pointer;"><i class="fa fa-clone"></i> Add to compare\n' +
                            '                </div>')
                    }else{
                        alert(data.status)
                    }
                });
            });


        });


        get_compare_list({{$product->id}});





    </script>
@endsection
