@extends('client.base')

@section('content')
    <div class="site__body">
        <div class="page-header">
            <div class="page-header__container container">
                <div class="page-header__breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{URL::to('/')}}">Home</a>
                                <svg class="breadcrumb-arrow" width="6px" height="9px">
                                    <use xlink:href="{{asset('client/images')}}/sprite.svg#arrow-rounded-right-6x9"></use>
                                </svg>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Wishlist</li>
                        </ol>
                    </nav>
                </div>
                <div class="page-header__title">
                    <h1>Wishlist</h1>
                </div>
            </div>
        </div>


        <div class="block">
            <div class="container">
                @if(count($wishlists)>0)
                    @foreach($wishlists as $obj)
                        <div class="row" style="border:1px dotted black;margin: 5px 0px;">
                            <div class="col-md-2"><img src="{{asset($obj->product_details->media->file_path)}}" alt="" style="width: auto;height:100px;"></div>
                            <div class="col-md-4" style="padding: 40px;">{{$obj->product_details->name}}</div>
                            <div class="col-md-2" style="padding: 40px;"><strike style="font-size: 10px;display: block;">₹{{number_format($obj->product_details->inventory->retail_price)}}</strike> ₹{{number_format($obj->product_details->inventory->sale_price)}}</div>
                            <div class="col-md-2" style="padding: 40px;"><a href="{{url($obj->product_details->slug)}}">View product</a></div>

                        </div>
                    @endforeach
                @else
                    <div class="row">
                        <div class="col-md-12">
                            Currently your wish list is empty
                        </div>
                    </div>
                @endif
            </div>
        </div>

    </div>
@endsection


@section('bottom')
    <script>

        function cart_update(qty,cart){
            if(qty){
                $.post(baseUrl + '/cart/update/', { cart: cart, _token: csrf,quantity:qty,type:'update' }).done(function (data) {
                    console.log(data)
                    window.location.reload();
                })
            }
        }

        function remove(cart){
            if(cart){
                $.post(baseUrl + '/cart/update/', { cart: cart, _token: csrf,type:'remove' }).done(function (data) {
                    console.log(data)
                    window.location.reload();
                })
            }
        }

        function checkout() {
            $.get(baseUrl+'/get/user').done(function (data) {
                data = JSON.parse(data);
                if(data.status){
                    window.location.replace(baseUrl+'/checkout/address')
                }else{
                    $('#login-register').modal('show');
                }
            })
        }



        $(document).on('click', '.remove_coupon', function(e){
            let dom =  $(this);
            $.post(baseUrl+'/remove_coupon',{_token:csrf,id:dom.data('id')}).done(function (data) {
                let obj = JSON.parse(data);
                if(obj.status == true){
                    window.location.reload()
                }else{
                    swal('Oops!!','Please try again later')
                }
            })
        });

        $(document).on('click', '.apply-coupon', function(e){
            let dom =  $(this);
            const coupon = $('#input-coupon-code').val();
            $.post(baseUrl+'/coupon',{_token:csrf,coupon:coupon}).done(function (data) {
                let obj = JSON.parse(data);
                if(obj.status == true){
                    window.location.reload()
                }else{
                    swal('Oops!!','Please try again later')
                }
            })
        });


        $(document).on('click', '.ex-btn', function(e){

            let dom =  $(this);
            $.post(baseUrl+'/add-extended-warranty',{_token:csrf,id:dom.data('id'),cart:dom.data('cart')}).done(function (data) {
                let obj = JSON.parse(data);
                if(obj.status == true){
                    window.location.reload()
                }else{
                    swal('Oops!!','Please try again later')
                }
            })
        });


        $(document).on('click', '.remove-cart', function(e){

            let dom =  $(this);
            $.post(baseUrl + '/cart/update/', { cart: dom.data('remove'), _token: csrf,type:'remove' }).done(function (data) {
                console.log(data)
                window.location.reload();
            })
        });




    </script>
@endsection

@section('head')
    <style>
        .modal-content{
            width: 100% !important;
        }
        .input-number__input {
            -moz-appearance: textfield;
            display: block;
            width: 100%;
            min-width: 130px;
            padding: 5px;
            text-align: center !important;
        }
    </style>
@endsection