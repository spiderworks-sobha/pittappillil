@extends('client.base')

@section('content')

    <div class="page-header" style="margin-bottom: 20px;">
        <div class="page-header__container container-fluid" style="padding: 0px;">
            <div class="page-header__title" align="center" style="    background: linear-gradient(45deg, rgb(3, 169, 244), rgb(224, 9, 24)); padding: 30px 0px; color: white;}">
                <h1>Shop Big, Save Big</h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-12">
                <div class="block">
                    @if(count($offers)>0)
                        <div class="posts-view">
                            <div class="posts-view__list posts-list posts-list--layout--classic">
                                <div class="posts-list__body">


                                    @foreach($offers as $offer)
                                        <div class="col-md-4">
                                            <div class="posts-list__item"  style="width: 100%">
                                                <div class="post-card post-card--layout--list post-card--size--nl">
                                                    <a href="{{url('offers/'.$offer->slug)}}" style="color: unset">
                                                        <div class="post-card__info" align="center" >
                                                            <div class="post-card__image" align="center" style="width: 100%"><img src="@if($offer->offer_image()) {{asset($offer->offer_image())}} @else {{asset('images/default.png')}} @endif" alt=""></div>
                                                            <div class="post-card__name">{{$offer->offer_name}} </div>

                                                            <p style="text-align: center;width: 100%;border-bottom: 1px solid #e2e2e2;padding-bottom: 10px;">
                                                                <span style="color: green">Offer available from {{\Carbon\Carbon::parse($offer->validity_start_date)->format('d F Y')}} to {{\Carbon\Carbon::parse($offer->validity_end_datess)->format('d F Y')}}</span>
                                                            </p>


                                                            <p style="text-align: left;padding: 10px;" >
                                                                <span>
                                                                <b>Terms and Conditions</b> <br>
                                                                @if($offer->min_purchase_amount) Minimum purchase amount : {{$offer->min_purchase_amount}}<br> @endif
                                                                @if($offer->max_discount_amount) Maximum discount amount : {{$offer->max_discount_amount}} @endif
                                                                </span><br>
                                                                <div  class="timer" data-from="{{\Carbon\Carbon::parse($offer->validity_start_date)->format('Ymdhis')}}" data-timer="{{\Carbon\Carbon::parse($offer->validity_end_date)->format('Ymdhis')}}"></div>
                                                            </p>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="nothing-to-display"> Currently there are no offer available, Please visit us later for offers</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
@section('head')
    <style>
        .post-card__name{
            font-size: 20px !important;
            width: 100%
        }
        .offer-expired{
            margin-top: 10px;
            background: #ff2e31;
            color: white;
            padding: 10px;
            text-align: center;

        }

        .offer-expire{
            margin-top: 10px;
            background: green;
            color: white;
            padding: 10px;
            text-align: center;
            width: fit-content;
            border-radius: 5px;
        }

        .post-card__image img {
            max-width: 180px;
            max-height: 180px;
            min-height: 180px;
            object-fit: contain;
        }
        .timer{
            width: 100%;
        }

    </style>
@endsection
@section('bottom')
    <script src="{{asset('client/js/moment.js')}}"></script>
    <script>
        $('.timer').each(function () {
            let to = $(this).data('timer');
            let from = $(this).data('from');

            if(moment().isAfter(moment(to, "YYYYMMDDhhmmss"))){
                $(this).html('<div class="offer-expired" >Offer Expired : '+moment(to, "YYYYMMDDhhmmss").fromNow()+'</div>');

            } else{
                $(this).html('<div class="offer-expire">Expiry time : '+moment(to, "YYYYMMDDhhmmss").fromNow()+'</div>');
            }

        })


    </script>
@endsection