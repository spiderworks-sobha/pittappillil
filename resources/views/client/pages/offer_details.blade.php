@extends('client.base')

@section('content')

    <div class="page-header" style="margin-bottom: 20px;">
        <div class="page-header__container container-fluid" style="padding: 0px;">
            <div class="page-header__title" align="center" style="background: linear-gradient(45deg, #03A9F4, #FFC107);
    padding: 30px 0px;
    color: black;
}">
                <h1>{{$offer->offer_name}}</h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-12">
                <div class="block">
                    <div class="products-view__list products-list" data-layout="grid-4-full" data-with-features="false">
                    @if(count($products)>0)
                        <div class="products-list__body">
                            @foreach($products as $obj)
                                <div class="products-list__item">
                                <div class="product-card m-0">
                                    <div class="product-card__image">
                                        @isset($obj->product_details->media)
                                        <a href="{{url($obj->product_details->slug)}}"><img src="{{asset($obj->product_details->media->file_path)}}" alt=""></a>
                                            @else
                                        <a href="#"><img src="{{asset('images/default.png')}}" alt=""></a>
                                        @endif
                                    </div>
                                    <div class="product-card__info">
                                        <div class="product-card__name"><a href="{{url($obj->product_details->slug)}}">{{$obj->product_details->name}}</a></div>
                                    </div>
                                    <div class="product-card__actions">
                                        <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                        <div class="product-card__prices">{{'₹ '.number_format($obj->product_details->inventory->sale_price)}}</div>

                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="col-md-12 d-flex justify-content-center pt-4" align="center">
                            {{$products->links()}}
                        </div>
                    @else
                        <div class="nothing-to-display"> Currently there are no offer available, Please visit us later for offers</div>
                    @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('head')
    <style>
        .post-card__name{
            font-size: 20px !important;
            width: 100%
        }
        .offer-expired{
            margin-top: 10px;
            background: #ff2e31;
            color: white;
            padding: 10px;
            text-align: center;

        }

        .offer-expire{
            margin-top: 10px;
            background: green;
            color: white;
            padding: 10px;
            text-align: center;
        }

        .post-card__image img {
            max-width: 180px;
            max-height: 180px;
            min-height: 180px;
            object-fit: contain;
        }
        .timer{
            width: 100%;
        }

    </style>
@endsection
@section('bottom')
    <script src="{{asset('client/js/moment.js')}}"></script>
    <script>
        $('.timer').each(function () {
            let to = $(this).data('timer');
            let from = $(this).data('from');

            if(moment().isAfter(moment(to, "YYYYMMDDhhmmss"))){
                $(this).html('<div class="offer-expired">Offer Expired : '+moment(to, "YYYYMMDDhhmmss").fromNow()+'</div>');

            } else{
                $(this).html('<div class="offer-expire">Expiry time : '+moment(to, "YYYYMMDDhhmmss").fromNow()+'</div>');
            }

        })
    </script>
@endsection