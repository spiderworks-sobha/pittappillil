@extends('client.base')

@section('content')
    <div class="site__body">
        <div class="page-header">
            <div class="page-header__container container">
                <div class="page-header__breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{URL::to('/')}}">Home</a>
                                <svg class="breadcrumb-arrow" width="6px" height="9px">
                                    <use xlink:href="{{asset('images')}}/sprite.svg#arrow-rounded-right-6x9"></use>
                                </svg>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Cart</li>
                        </ol>
                    </nav>
                </div>
                <div class="page-header__title">
                    <h1>Shopping Cart</h1>
                </div>
            </div>
        </div>

        <div class="cart block">
            @if(count($obj)>0)
                <div class="container">
                <!-- table starts here -->
                <div class="cart__section">
                    <div class="cart-table__head mob_view">
                        <div class="row justify-content-center align-self-center card_section__row">
                            <div class="col-md-2 justify-content-center align-self-center">
                                <span class="cart__section__column cart__section--image">Image</span>
                            </div>
                            <div class="col-md-3 justify-content-center align-self-center">
                                <span class="cart__section__column cart__section__column--product">Product</span>
                            </div>
                            <div class="col-md-2 justify-content-center align-self-center">
                                <span class="cart__section__column cart__section__column--price">Price</span>
                            </div>
                            <div class="col-md-2 justify-content-center align-self-center" align="center">
                                <span class="cart__section__column cart__section__column--quantity">Quantity</span>
                            </div>
                            <div class="col-md-2 justify-content-center align-self-center">
                                <span class="cart__section__column cart__section__column--total">Total</span>
                            </div>
                            <div class="col-md-1 justify-content-center top_view">
                            <span class="cart__section__column cart__section__column--remove">
                              <svg width="12px" height="12px">
                                 <use xlink:href="{{asset('images')}}/sprite.svg#cross-12"></use>
                             </svg>
                            </span>
                            </div>
                        </div>
                    </div>
                    <div class="cart-section__body">
                        @php $total = 0; @endphp
                        @foreach($obj as $o)
                        <div class="row align-self-center @if($o->offer_parent == null) card_section__row @else sub_pro_row @endif">
                            <div class="col-md-2 col-sm-4 col-4 align-self-center" align="center">
                                <div class="@if($o->offer_parent) sub_image_cart @endif cart__section__column cart__section__column--image">
                                    @if($o->offer_parent)<i class="fas fa-angle-double-right"></i>@endif
                                    @isset($o->product->media)
                                        <a href="{{url($o->product->slug)}}"><img src="{{asset($o->product->media->file_path)}}" alt="" style="@if(!$o->offer_parent)max-height: 80px;max-width: 80px;@else max-height: 50px;max-width: 50px; @endif width: unset;    "></a>
                                    @endisset
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-8 col-8 justify-content-center align-self-center">
                                <div class="cart__section__column cart__section__column--product">
                                    <a href="{{url($o->product->slug)}}" class="cart__section__product-name">{{$o->product->name}}</a>
                                    <ul class="cart__section__options">
                                        <li>MRP : ₹{{number_format($o->product->inventory->retail_price)}}</li>
                                        @if($o->offer)<li style="color:green;">Offer applied : {{$o->offer->offer_name}}</li>@endif
                                    </ul>

                                    @if(null !== $o->product->extended_warranty() && null == $o->warranty_parent)
                                        <div class="extended-warranty-box">
                                            @if(count($o->product->extended_warranty()) > 0)
                                                <b><u>Extended warranty options</u></b><br>
                                                @foreach($o->product->extended_warranty() as $ew)
                                                    <button type="button" class="btn btn-secondary btn-sm ex-btn" data-id="{{$ew->id}}" data-cart="{{$o->id}}" >
                                                        {{$ew->title}} @ {{$ew->warranty_price}}
                                                    </button>
                                                @endforeach
                                            @endif
                                        </div>
                                    @endif

                                </div>

                            </div>
                            <div class="col-md-2  col-sm-4 col-4 justify-content-center align-self-center">
                                <div class="cart__section__column cart__section__column--price" data-title="Price">@if($o->price == 0) FREE @else ₹{{number_format($o->price)}} @endif</div>
                            </div>
                            <div class="col-md-2  col-sm-4 col-4 justify-content-center align-self-center" align="center">
                                <div class="cart__section__column--quantity" data-title="Quantity">
                                    <div class="input-number">
                                        @if($o->offer_parent)
                                            {{$o->quantity}}
                                        @else
                                            <input class="form-control input-number__input" type="number" min="1" value="{{$o->quantity}}" readonly>
                                            <div class="input-number__add" data-cart="{{encrypt($o->id)}}"></div>
                                            <div class="input-number__sub" data-cart="{{encrypt($o->id)}}"></div>
                                        @endif


                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2  col-sm-4 col-4 justify-content-center align-self-center">@php $total += ($o->price*$o->quantity); @endphp
                                <div class="cart__section__column cart__section__column--total" data-title="Total">@if($o->price == 0) FREE @else ₹{{number_format($o->price*$o->quantity)}} @endif </div>
                            </div>
                            <div class="col-md-1 justify-content-center top_view align-self-center">
                               @if(!$o->offer_parent)
                                    <span class="cart__section__column cart__section__column--remove remove-cart"  style="cursor: pointer"  data-remove="{{encrypt($o->id)}}">
                                         <svg width="12px" height="12px">
                                            <use xlink:href="{{asset('client/images')}}/sprite.svg#cross-12"></use>
                                         </svg>
                                    </span>
                               @endif
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
                <div class="cart__actions">
                    <form class="cart__coupon-form">
                        <label for="input-coupon-code" class="sr-only">Password</label>
                        <input type="text" class="form-control" id="input-coupon-code" placeholder="Coupon Code" value="{{$coupon}}">
                        <button type="button" class="btn btn-primary apply-coupon">Apply Coupon</button>

                    </form>

                </div> @if($total_from_model['coupon_message']) {!! $total_from_model['coupon_message'] !!} @endif
                <div class="row justify-content-end pt-5">
                    <div class="col-12 col-md-7 col-lg-6 col-xl-5">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="card-title">Cart Totals</h3>
                                <table class="cart__totals">
                                    <thead class="cart__totals-header">
                                    <tr>
                                        <th>Subtotal</th>
                                        <td>₹{{$total_from_model['total']}}</td>
                                    </tr>
                                    </thead>
                                    <tbody class="cart__totals-body">
                                    <tr>
                                        <th>Shipping</th>
                                        <td>
                                            FREE
                                        </td>
                                    </tr>
                                    @if($total_from_model['coupon_discount'])
                                        <tr>
                                            <th>Discount</th>
                                            <td>
                                                - {{$total_from_model['coupon_discount']}}
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                    <tfoot class="cart__totals-footer">
                                    <tr>
                                        <th>Total</th>
                                        <td>₹{{$total_from_model['final_total']}}</td>
                                    </tr>
                                    </tfoot>
                                </table>
                                @auth
                                    <button type="button" class="btn btn-primary btn-xl btn-block cart__checkout-button" onclick="checkout()">Proceed to checkout</button>
                                    @else
                                    <button type="button" class="btn btn-primary btn-xl btn-block cart__checkout-button" onclick="checkout()">Please login to checkout</button>
                                @endauth
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @else
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            Currently your shopping cart is empty
                        </div>
                    </div>
                </div>
            @endif
        </div>

    </div>
    @endsection


@section('bottom')
    <script>

        function cart_update(qty,cart){
            if(qty){
                $.post(baseUrl + '/cart/update/', { cart: cart, _token: csrf,quantity:qty,type:'update' }).done(function (data) {
                    console.log(data)
                    window.location.reload();
                })
            }
        }

        function remove(cart){
            if(cart){
                $.post(baseUrl + '/cart/update/', { cart: cart, _token: csrf,type:'remove' }).done(function (data) {
                    console.log(data)
                    window.location.reload();
                })
            }
        }

        function checkout() {
            $.get(baseUrl+'/get/user').done(function (data) {
                data = JSON.parse(data);
                if(data.status){
                    window.location.replace(baseUrl+'/checkout/address')
                }else{
                    $('#login-register').modal('show');
                }
            })
        }



        $(document).on('click', '.remove_coupon', function(e){
            let dom =  $(this);
            $.post(baseUrl+'/remove_coupon',{_token:csrf,id:dom.data('id')}).done(function (data) {
                let obj = JSON.parse(data);
                if(obj.status == true){
                    window.location.reload()
                }else{
                    swal('Oops!!','Please try again later')
                }
            })
        });

        $(document).on('click', '.apply-coupon', function(e){
            let dom =  $(this);
            const coupon = $('#input-coupon-code').val();
            $.post(baseUrl+'/coupon',{_token:csrf,coupon:coupon}).done(function (data) {
                let obj = JSON.parse(data);
                if(obj.status == true){
                    window.location.reload()
                }else{
                    swal('Oops!!','Please try again later')
                }
            })
        });


        $(document).on('click', '.ex-btn', function(e){

                let dom =  $(this);
                $.post(baseUrl+'/add-extended-warranty',{_token:csrf,id:dom.data('id'),cart:dom.data('cart')}).done(function (data) {
                    let obj = JSON.parse(data);
                    if(obj.status == true){
                        window.location.reload()
                    }else{
                        swal('Oops!!','Please try again later')
                    }
                })
        });


        $(document).on('click', '.remove-cart', function(e){

                let dom =  $(this);
                $.post(baseUrl + '/cart/update/', { cart: dom.data('remove'), _token: csrf,type:'remove' }).done(function (data) {
                    console.log(data)
                    window.location.reload();
                })
        });


        $(document).on('click', '.input-number__add', function(e){
            let dom =  $(this);
            cart_update(1,dom.data('cart'))
        });

        $(document).on('click', '.input-number__sub', function(e){
            let dom =  $(this);
            cart_update(-1,dom.data('cart'))
        });



    </script>
@endsection

@section('head')
    <style>
        .modal-content{
            width: 100% !important;
        }
        .input-number__input {
            -moz-appearance: textfield;
            display: block;
            width: 100%;
            min-width: 130px;
            padding: 5px;
            text-align: center !important;
        }
    </style>
@endsection