@extends('client.base')

@section('head')

@endsection
@section('content')

    <div class="page-header">
        <div class="page-header__container container">
            <div class="page-header__breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{url('/')}}">Home</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="images/sprite.svg#arrow-rounded-right-6x9"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Compare</li>
                    </ol>
                </nav>
            </div>
            <div class="page-header__title">
                <h1>Compare</h1>
            </div>
        </div>
    </div>
    <div class="block">
        <div class="container">
            <div class="table-responsive">
                <table class="compare-table">
                    <tbody>

                    <tr>
                        <th>Product</th>
                            @for($i=0;$i<$category_attributes_group[0]->count;$i++)
                                <td>
                                    <product-block variant_id="{{$category_attributes_group[0]->{'p_'.$i} }}"></product-block>
                                </td>
                            @endfor
                    </tr>



                    @foreach($category_attributes_group as $obj)
                        <tr>
                            <td colspan="{{$obj->count+1}}">{{$obj->group_name}}</td>
                        </tr>
                        @foreach($obj->attributes() as $o)
                            <tr>
                                <th>{{$o->attribute_name}}</th>
                                @for($i=0;$i<$obj->count;$i++)
                                    <td>
                                        <div class="compare-table__product-name">{{$o->get($obj->{'p_'.$i}) }}</div>
                                    </td>
                                @endfor
                            </tr>
                        @endforeach
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection
@section('bottom')
    @parent

@endsection
