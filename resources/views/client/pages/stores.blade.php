@extends('client.base')

@section('content')

      {{--  <div class="page-header">
            <div class="page-header__container container">
                <div class="page-header__title">
                    @if($category)
                    <h1>{{$category->category_name}}</h1>
                    @endif
                </div>
            </div>
        </div>
--}}
        <div class="container" style="margin-top: 20px;">
                <store @if($page) page="{{$page}}"  @endif @if($brands) brands_="{{$brands}}" @endif  @if($attributes) attributes_="{{$attributes}}" @endif @if($keyword) keyword_="{{$keyword}}" @endif  @if($from) from_="{{$from}}" @endif  @if($to) to_="{{$to}}" @endif @if($category) slug="{{$category->slug}}" @endif category="@if($category){{$category->id}}@else 0 @endif" ref="store"></store>
        </div>



@endsection