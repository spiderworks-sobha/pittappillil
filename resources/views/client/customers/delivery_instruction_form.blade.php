<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Add Delivery Instructions</h5>
    <a  class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </a>
</div>
<div class="modal-body">
	<form action="{{url('account/address/save-delivery-instructions')}}" id="DeliveryInstructionFrm" method="post" class="form-horizontal style-form" enctype="multipart/form-data">
	    @csrf
	    @if($obj->id)
	    	<input type="hidden" name="id" value="{{encrypt($obj->id)}}">
	    @endif
	    <div class="form-group">
	      	<p class="address-card__row-title">Provide details such as building description, a nearby landmark, or other navigation instructions.</p>
	      	<textarea class="form-control" name="delivery_instructions">{{$obj->delivery_instructions}}</textarea>
	    </div>
	    <div class="form-group">
            <button class="btn btn-primary" type="button" id="delivery-instruction-save-btn">Save</button>
       	</div>
	</form>
</div>