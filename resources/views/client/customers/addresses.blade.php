@extends('client.base')

@section('content')

    <div class="page-header">
                <div class="page-header__container container">
                    <div class="page-header__breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="{{url('/')}}">Home</a>
                                    <svg class="breadcrumb-arrow" width="6px" height="9px">
                                        <use xlink:href="{{asset('client')}}/images/sprite.svg#arrow-rounded-right-6x9"></use>
                                    </svg>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="{{url('account/dashboard')}}">My Account</a>
                                    <svg class="breadcrumb-arrow" width="6px" height="9px">
                                        <use xlink:href="{{asset('client')}}/images/sprite.svg#arrow-rounded-right-6x9"></use>
                                    </svg>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Manage Addresses</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="page-header__title">
                        <h1>Manage Addresses</h1>
                    </div>
                </div>
            </div>
            <div class="block">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-lg-3 d-flex">
                            <div class="account-nav flex-grow-1">
                                <h4 class="account-nav__title">Navigation</h4>
                                @include('client.includes.account_menu')
                            </div>
                        </div>
                        <div class="col-12 col-lg-9 mt-4 mt-lg-0">
                            <div class="addresses-list">
                                <a href="{{url('account/address/address')}}" class="addresses-list__item addresses-list__item--new show-modal" id="add-new-address-btn" data-target="#common-modal">
                                    <div class="addresses-list__plus"></div>
                                    <div class="btn btn-secondary btn-sm">Add New</div>
                                </a>
                                @if(count($addresses)>0)
                                    @foreach($addresses as $key=>$address)
                                        <div class="addresses-list__divider" id="address-list-divider-{{$address->id}}"></div>
                                        <div class="addresses-list__item card address-card" id="address-list-item-{{$address->id}}">
                                            @include('client.includes.address', ['address'=>$address, 'from'=>'address'])
                                        </div>
                                        @if($key+1 == count($addresses))
                                            <div class="addresses-list__divider"></div>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection

@section('bottom')
<script>
    $(document).on('click', '.address-list-remove', function(){
        var obj = $(this);
        var id = obj.data('id');
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, remove it!'
        }).then((result) => {
          if (result.value) {
            obj.replaceWith('<i class="fas fa-spinner fa-spin"></i>');
            $.get("{{url('account/address-remove')}}"+"/"+id, function(data){
                if(data.success == true)
                {
                    $('#address-list-divider-'+id).remove();
                    $('#address-list-item-'+id).remove();
                    Toast.fire({
                        title: 'Success!',
                        text: 'Address successfully removed!',
                        icon: 'success',
                      });
                }
                else{
                    Toast.fire({
                        title: 'Error!',
                        text: 'Oops, something went wrong.Please try again!',
                        icon: 'error',
                      });
                }
            });
          }
        });
    });

    $(document).on('click', '.address-list-default', function(){
            var obj = $(this);
            var id = obj.data('id');
            obj.html('&nbsp;&nbsp;<i class="fas fa-spinner fa-spin"></i>');
            $.get("{{url('account/address-make-default')}}"+"/"+id, function(data){
                if(data.success == true)
                {
                    $('.address-list-remove').show();
                    $('.address-list-default').show();
                    $('#address-list-remove-'+id).hide();
                    $('#address-list-default-'+id).hide();
                    obj.html('&nbsp;|&nbsp;Make Default');
                    $('.address-card__badge').remove();
                    $( '<div class="address-card__badge">Default</div>' ).insertBefore( "#card-body-"+id );
                    Toast.fire({
                        title: 'Success!',
                        text: 'Address successfully made default!',
                        icon: 'success',
                      });
                }
                else{
                    Toast.fire({
                        title: 'Error!',
                        text: 'Oops, something went wrong.Please try again!',
                        icon: 'error',
                      });
                }
            });
        });
</script>
@endsection