<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Add Address</h5>
    <a  class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </a>
</div>
<div class="modal-body">
	<form action="{{url('account/address/save')}}" id="AddressFrm" method="post" class="form-horizontal style-form" enctype="multipart/form-data">
	    @csrf
	    @if($obj->id)
	    	<input type="hidden" name="id" value="{{encrypt($obj->id)}}">
	    @endif
	    <input type="hidden" name="location" value="{{$location}}">
	    <div class="form-group">
	      	<label>Full Name</label>
	        <input type="text" class="form-control" name="full_name" id="full_name" value="{{$obj->full_name}}">
	        <span id="full_name_error"></span>
	    </div>
	    <div class="form-group">
	      	<label>Mobile Number</label>
	        <input type="text" class="form-control" name="mobile_number" id="mobile_number" placeholder="10 digit mobile number without prefix" maxlength="10" value="{{$obj->mobile_number}}">
	        <span id="mobile_number_error"></span>
	    </div>
	    <div class="form-group">
	      	<label>Pincode</label>
	        <input type="text" class="form-control" name="pincode" placeholder="6 digit pincode" maxlength="6" value="{{$obj->pincode}}">
	        <span id="pincode_error"></span>
	    </div>
	    <div class="form-group">
	      	<label>Address</label>
	        <input type="text" class="form-control" name="address1" placeholder="Flat / House No. / Floor / Building" value="{{$obj->address1}}">
	        <span id="address1_error"></span>
	    </div>
	    <div class="form-group">
	        <input type="text" class="form-control" name="address2" placeholder="Colony / Street / Locality" value="{{$obj->address2}}">
	    </div>
	    <div class="form-group">
	      	<label>Landmark</label>
	        <input type="text" class="form-control" name="landmark" placeholder="Eg: Near SBI Palarivattom" value="{{$obj->landmark}}">
	    </div>
	    <div class="form-group">
	      	<label>City</label>
	        <input type="text" class="form-control" name="city" value="{{$obj->city}}">
	        <span id="city_error"></span>
	    </div>
	    <div class="form-group">
	      	<label>State</label>
	      	<select class="form-control select2_input" data-placeholder="Select State" data-select2-url="{{url('select2/state', [101])}}" name="state" id="state" data-parent="#common-modal .modal-body">
	      		@if($obj->state_details)
	      			<option value="{{$obj->state_details->id}}" selected="selected">{{$obj->state_details->name}}</option>
	      		@endif
	      	</select>
	      	<span id="state_error"></span>
	    </div>
	    <div class="form-group">
	      	<label>Address Type</label>
	        <select class="form-control select2_input w-100" data-placeholder="Select Address Type" name="type" id="type" data-parent="#common-modal .modal-body">
	        	<option value="1" @if($obj->type == '1') selected="selected" @endif>Home</option>
	        	<option value="0" @if($obj->type == '0') selected="selected" @endif>Office</option>
	        </select>
	        <span id="type_error"></span>
	    </div>
	    <div class="form-group">
            <button class="btn btn-primary" type="button" id="address-save-btn">Save</button>
       	</div>
	</form>
</div>