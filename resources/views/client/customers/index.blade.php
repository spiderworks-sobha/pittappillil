@extends('client.base')

@section('content')

    <div class="page-header">
                <div class="page-header__container container">
                    <div class="page-header__breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="{{url('/')}}">Home</a>
                                    <svg class="breadcrumb-arrow" width="6px" height="9px">
                                        <use xlink:href="{{asset('client')}}/images/sprite.svg#arrow-rounded-right-6x9"></use>
                                    </svg>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">My Account</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="page-header__title">
                        <h1>My Account</h1>
                    </div>
                </div>
            </div>
            <div class="block">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-lg-3 d-flex">
                            <div class="account-nav flex-grow-1">
                                <h4 class="account-nav__title">Navigation</h4>
                                @include('client.includes.account_menu')
                            </div>
                        </div>
                        <div class="col-12 col-lg-9 mt-4 mt-lg-0">
                            <div class="dashboard">
                                <div class="dashboard__profile card profile-card">
                                    <div class="card-body profile-card__body">

                                        <div class="profile-card__name">{{Auth::user()->full_name}}</div>
                                        <div class="profile-card__email">@if(Auth::user()->email) {{Auth::user()->email}} @endif</div>
                                        <div class="profile-card__email">@if(Auth::user()->username) {{Auth::user()->username}} @endif</div>
                                        <div class="profile-card__edit">
                                            <a href="{{route('account.edit-profile')}}" class="btn btn-secondary btn-sm">Manage Profile</a>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="dashboard__address card address-card address-card--featured" @if(!$address) style="display: none;" @endif id="address-home-display">
                                    @include('client.includes.address', ['address'=>$address, 'from'=>'home'])
                                </div>
                                @if(!$address)
                                    <div class="dashboard__address" id="address-home-new-add">
                                        <div class="card-body profile-card__body p-0">
                                            <a href="{{url('account/address/home')}}" class="w-100 addresses-list__item--new show-modal" data-target="#common-modal">
                                                <div class="addresses-list__plus"></div>
                                                <div class="btn btn-secondary btn-sm">Add New Address</div>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection