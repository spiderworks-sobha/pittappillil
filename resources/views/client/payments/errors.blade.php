@extends('client.base')

@section('head')
<style>
    #googleMap{
            width:100%;
            height:600px;
    }
    .store-item {
        border: 1px solid #ddd;
        margin-bottom: 10px;
    }
</style>
@endsection
@section('content')

    <div class="page-header">
        <div class="page-header__container container">
            <div class="page-header__breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.html">Home</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="{{asset('client')}}/images/sprite.svg#arrow-rounded-right-6x9"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="">Breadcrumb</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="{{asset('client')}}/images/sprite.svg#arrow-rounded-right-6x9"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Store Locator</li>
                    </ol>
                </nav>
            </div>
            <div class="page-header__title">
                <h1>Store Locator</h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div>Opps.. Something wrong happend! please try again...</div>
        </div>
    </div>
@endsection
@section('bottom')
@parent

@endsection
