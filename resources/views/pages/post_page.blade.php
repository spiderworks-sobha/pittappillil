@extends('client.base')

@section('head')
   <style>
      {!! $page->extra_css !!}
   </style>
@endsection
@section('content')
<div class="block about-us">
   <div class="about-us__image"></div>
   <div class="container">
      <div class="row justify-content-center">
         <div class="col-12 col-xl-10">
            <div class="about-us__body">
               <h1 class="about-us__title">{{$page->primary_heading}}</h1>
               <div class="about-us__text typography">
                  {!! $page->content !!}
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('bottom')
@parent

@endsection
