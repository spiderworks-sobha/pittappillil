@extends('client.base')

@section('head')

@endsection
@section('content')

    <div class="page-header">
        <div class="page-header__container container">
            <div class="page-header__breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{url('/')}}">Home</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="{{asset('client')}}/images/sprite.svg#arrow-rounded-right-6x9"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item">
                            @if($page->type == 'News')
                                <a href="{{url('news')}}">News</a>
                            @elseif($page->type == 'Blog')
                                <a href="{{url('blogs')}}">Blogs</a>
                            @endif
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="{{asset('client')}}/images/sprite.svg#arrow-rounded-right-6x9"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{$page->name}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 col-lg-9 col-xl-8">
                <h1 class="post-header__title text-center">{{$page->primary_heading}}</h1>
                <div class="post-header__meta m-2">
                    <div class="post-header__meta-item" style="margin: 0 auto;">{{date('F, d Y', strtotime($page->updated_at))}}</div>
                </div>
                <div class="post__featured"><img src="@if($page->banner_image) {{asset($page->banner_image->file_path)}} @else {{asset('images/default.png')}} @endif" alt=""></a></div>
                <div class="post__content typography typography--expanded">{!! $page->content !!}</div>
            </div>
        </div>
    </div>
@endsection
@section('bottom')
@parent

@endsection
