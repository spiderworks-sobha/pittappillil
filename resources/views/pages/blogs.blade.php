@extends('client.base')

@section('head')

@endsection
@section('content')

    <div class="page-header">
        <div class="page-header__container container">
            <div class="page-header__breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.html">Home</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="{{asset('client')}}/images/sprite.svg#arrow-rounded-right-6x9"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="">Breadcrumb</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="{{asset('client')}}/images/sprite.svg#arrow-rounded-right-6x9"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Store Locator</li>
                    </ol>
                </nav>
            </div>
            <div class="page-header__title">
                <h1>Latest Blogs</h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-12">
             <div class="block">
                @if(count($pages)>0)
                <div class="posts-view">
                   <div class="posts-view__list posts-list posts-list--layout--classic">
                      <div class="posts-list__body">
                        @foreach($pages as $page)
                         <div class="posts-list__item">
                           <div class="post-card post-card--layout--list post-card--size--nl">
                              <div class="post-card__image"><a href="{{url('blog', [$page->slug])}}"><img src="@if($page->featured_image) {{asset($page->featured_image->file_path)}} @else {{asset('images/default.png')}} @endif" alt=""></a></div>
                              <div class="post-card__info">
                                 <div class="post-card__name"><a href="{{url('blog', [$page->slug])}}">{{$page->primary_heading}}</a></div>
                                 <div class="post-card__category w-100">{{date('F, d Y', strtotime($page->updated_at))}}</div>
                                 <div class="post-card__content">{{$page->short_description}}</div>
                                 <div class="post-card__read-more"><a href="{{url('blog', [$page->slug])}}" class="btn btn-secondary btn-sm">Read More</a></div>
                              </div>
                           </div>
                        </div>
                        @endforeach
                      </div>
                   </div>
                   <div class="posts-view__pagination">
                      {{ $pages->links('client.includes.pagination') }}
                   </div>
                </div>
                @else
                    <div class="nothing-to-display"> Oops...Nothing to display...</div>
                @endif
             </div>
            </div>
        </div>
    </div>
@endsection
@section('bottom')
@parent

@endsection
