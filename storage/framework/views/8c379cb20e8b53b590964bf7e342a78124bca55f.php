<?php $__env->startSection('head'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="container-fluid">

        <div class="col-md-12" style="margin-bottom: 20px;" align="right">
            <?php if($obj->id): ?>
                <span class="page-heading">EDIT PAGE</span>
            <?php else: ?>
                <span class="page-heading">CREATE NEW PAGE</span>
            <?php endif; ?>
            <div >
                <div class="btn-group">
                    <a href="<?php echo e(route($route.'.index')); ?>"  class="btn btn-success"><i class="fa fa-list"></i> List
                    </a>
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="card card-borderless">
                <?php if($obj->id): ?>
                    <form method="POST" action="<?php echo e(route($route.'.update')); ?>" class="p-t-15" id="PageFrm" data-validate=true>
                <?php else: ?>
                    <form method="POST" action="<?php echo e(route($route.'.store')); ?>" class="p-t-15" id="PageFrm" data-validate=true>
                <?php endif; ?>
                <?php echo csrf_field(); ?>
                <input type="hidden" name="id" <?php if($obj->id): ?> value="<?php echo e(encrypt($obj->id)); ?>" <?php endif; ?> id="inputId">

                <ul class="nav nav-tabs nav-tabs-simple d-none d-md-flex d-lg-flex d-xl-flex" role="tablist" data-init-reponsive-tabs="dropdownfx">
                    <li class="nav-item">
                        <a class="active show" data-toggle="tab" role="tab"
                           data-target="#tab1Basic"
                        href="#" aria-selected="true">Extra Details</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" data-toggle="tab" role="tab"
                           data-target="#tab3SEO"
                        class="" aria-selected="false">SEO</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" data-toggle="tab" role="tab"
                           data-target="#tab4Media"
                           class="" aria-selected="false">Media</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active show" id="tab1Basic">
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="row column-seperation padding-5">
                                        <div class="form-group form-group-default required">
                                            <label>Title</label>
                                            <input type="text" name="name" class="form-control" value="<?php echo e($obj->name); ?>" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row column-seperation padding-5">
                                        <div class="form-group form-group-default required">
                                            <label>Top content</label>
                                            <textarea name="top_description" class="form-control richtext" id="top_description" data-image-url="<?php echo e(route('admin.summernote.image')); ?>"><?php echo e($obj->top_description); ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="row column-seperation padding-5">
                                        <div class="form-group form-group-default required">
                                            <label>Bottom content</label>
                                            <textarea name="bottom_description" class="form-control richtext" id="bottom_description" data-image-url="<?php echo e(route('admin.summernote.image')); ?>"><?php echo e($obj->bottom_description); ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row column-seperation padding-5">
                                        <div class="form-group form-group-default">
                                            <label class="">Extra Css</label>
                                            <textarea name="extra_css" class="form-control" rows="3" id="extra_css"><?php echo e($obj->extra_css); ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row column-seperation padding-5">
                                        <div class="form-group form-group-default">
                                            <label class="">Extra Js</label>
                                            <textarea name="extra_js" class="form-control" rows="3" id="extra_js"><?php echo e($obj->extra_js); ?></textarea>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab3SEO">
                        <div class="row">

                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default">
                                        <label>Browser title</label>
                                        <input type="text" class="form-control" name="browser_title" id="browser_title" value="<?php echo e($obj->browser_title); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default">
                                        <label class="">Meta Keywords</label>
                                        <textarea name="meta_keywords" class="form-control" rows="3" id="meta_keywords"><?php echo e($obj->meta_keywords); ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default">
                                        <label class="">Meta description</label>
                                        <textarea name="meta_description" class="form-control" rows="3" id="meta_description"><?php echo e($obj->meta_description); ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab4Media">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <p class="col-md-12">Featured Image</p>
                                    <div class="default-image-holder padding-5">
                                        <a href="javascript:void(0);" class="image-remove"><i class="fa  fa-times-circle"></i></a>
                                        <a href="<?php echo e(route('admin.media.popup', ['popup_type'=>'single_image', 'type'=>'Image', 'holder_attr'=>'0', 'related_id'=>$obj->id])); ?>" class="open-ajax-popup" title="Media Images" data-popup-size="large" id="image-holder-0">
                                          <?php if($obj->media_id && $obj->featured_image): ?>
                                            <img class="card-img-top padding-20" src="<?php echo e(asset('public/'.$obj->featured_image->thumb_file_path)); ?>">
                                          <?php else: ?>
                                            <img class="card-img-top padding-20" src="<?php echo e(asset('assets/img/add_image.png')); ?>">
                                          <?php endif; ?>
                                        </a>
                                        <input type="hidden" name="media_id" id="mediaId0" value="<?php echo e($obj->media_id); ?>">
                                    </div>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" align="right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom'); ?>
    <script type="text/javascript">
        var validator = $('#PageFrm').validate({
            ignore: [],
            rules: {
                "name": "required",
              },
              messages: {
                "name": "Page title cannot be blank",
              },
            });
    </script>
##parent-placeholder-c03e9099aad17cb58e4fff1d93d751105735c9c2##
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.common.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp\www\pittappillil\resources\views/admin/frontend_pages/form.blade.php ENDPATH**/ ?>