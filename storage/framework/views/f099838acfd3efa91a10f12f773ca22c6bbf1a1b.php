<?php $__env->startSection('head'); ?>
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/keditor/css/keditor.css')); ?>" data-type="keditor-style" />
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/keditor/css/keditor-components.css')); ?>" data-type="keditor-style" />
        <!-- End of KEditor styles -->
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/keditor/css/prettify.css')); ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/keditor/css/keditor-custom.css')); ?>" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="container-fluid">

        <div class="col-md-12" style="margin-bottom: 20px;" align="right">
            <?php if($obj->id): ?>
                <span class="page-heading">EDIT PAGE</span>
            <?php else: ?>
                <span class="page-heading">CREATE NEW PAGE</span>
            <?php endif; ?>
            <div >
                <div class="btn-group">
                    <a href="<?php echo e(route($route.'.index')); ?>"  class="btn btn-success"><i class="fa fa-list"></i> List
                    </a>
                    <?php if($obj->id): ?>
                    <a href="<?php echo e(route($route.'.create')); ?>" class="btn btn-success"><i class="fa fa-pencil"></i> Create new
                    </a>
                    <a href="<?php echo e(route($route.'.destroy', [encrypt($obj->id)])); ?>" class="btn btn-success btn-warning-popup" data-message="Are you sure to delete?  Associated data will be removed if it is deleted." data-redirect-url="<?php echo e(route($route.'.index')); ?>"><i class="fa fa-trash"></i> Delete</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="card card-borderless">
                <?php if($obj->id): ?>
                    <form method="POST" action="<?php echo e(route($route.'.update')); ?>" class="p-t-15" id="PageFrm" data-validate=true>
                <?php else: ?>
                    <form method="POST" action="<?php echo e(route($route.'.store')); ?>" class="p-t-15" id="PageFrm" data-validate=true>
                <?php endif; ?>
                <?php echo csrf_field(); ?>
                <input type="hidden" name="id" <?php if($obj->id): ?> value="<?php echo e(encrypt($obj->id)); ?>" <?php endif; ?> id="inputId">

                <ul class="nav nav-tabs nav-tabs-simple d-none d-md-flex d-lg-flex d-xl-flex" role="tablist" data-init-reponsive-tabs="dropdownfx">
                    <li class="nav-item">
                        <a class="active show" data-toggle="tab" role="tab"
                           data-target="#tab1Basic"
                        href="#" aria-selected="true">Basic Details</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" data-toggle="tab" role="tab"
                           data-target="#tab2Content"
                        class="" aria-selected="false">Content</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" data-toggle="tab" role="tab"
                           data-target="#tab3SEO"
                        class="" aria-selected="false">SEO</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" data-toggle="tab" role="tab"
                           data-target="#tab4Media"
                           class="" aria-selected="false">Extra</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active show" id="tab1Basic">
                        <div class="row">
                            <div class="col-md-12">
                                <div data-keditor="html">
                                    <div id="content-area"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default form-group-default-select2 required">
                                        <label>Type</label>
                                        <?php echo Form::select('type', array('Page'=>'Page', 'Blog'=>'Blog', 'News'=>'News'), $obj->type, array('class'=>'full-width select2_input', 'id'=>'inputStatus', 'data-placeholder'=>'Choose a type','data-init-plugin'=>'select2',));; ?>


                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default required">
                                        <label>Title</label>
                                        <input type="text" name="name" class="form-control" value="<?php echo e($obj->name); ?>" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default required">
                                        <label class="">Slug (for url)</label>
                                        <input type="text" name="slug" class="form-control" value="<?php echo e($obj->slug); ?>" id="slug">
                                    </div>
                                    <p class="hint-text small">The “slug” is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens.</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default form-group-default-select2">
                                        <label class="">Parent</label>
                                        <select name="parent_id" class="full-width select2_input" data-select2-url="<?php echo e(route('select2.pages')); ?>" data-placeholder="Select Parent Page" data-init-plugin="select2">
                                            
                                         </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default required">
                                        <label>Primary Heading</label>
                                        <input type="text" name="primary_heading" class="form-control" value="<?php echo e($obj->primary_heading); ?>" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default">
                                        <label>Short Description</label>
                                        <textarea name="short_description" class="form-control" rows="3" id="short_description"><?php echo e($obj->short_description); ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default required">
                                        <label>Content</label>
                                        <textarea name="content" class="form-control richtext" id="content" data-image-url="<?php echo e(route('admin.summernote.image')); ?>"><?php echo e($obj->content); ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab2Content">
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="row column-seperation padding-5">
                                        <div class="form-group form-group-default required">
                                            <label>Top content</label>
                                            <textarea name="top_description" class="form-control richtext" id="top_description" data-image-url="<?php echo e(route('admin.summernote.image')); ?>"><?php echo e($obj->top_description); ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="row column-seperation padding-5">
                                        <div class="form-group form-group-default required">
                                            <label>Bottom content</label>
                                            <textarea name="bottom_description" class="form-control richtext" id="bottom_description" data-image-url="<?php echo e(route('admin.summernote.image')); ?>"><?php echo e($obj->bottom_description); ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row column-seperation padding-5">
                                        <div class="form-group form-group-default">
                                            <label class="">Extra Css</label>
                                            <textarea name="extra_css" class="form-control" rows="3" id="extra_css"><?php echo e($obj->extra_css); ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row column-seperation padding-5">
                                        <div class="form-group form-group-default">
                                            <label class="">Extra Js</label>
                                            <textarea name="extra_js" class="form-control" rows="3" id="extra_js"><?php echo e($obj->extra_js); ?></textarea>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab3SEO">
                        <div class="row">

                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default">
                                        <label>Browser title</label>
                                        <input type="text" class="form-control" name="browser_title" id="browser_title" value="<?php echo e($obj->browser_title); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default">
                                        <label class="">Meta Keywords</label>
                                        <textarea name="meta_keywords" class="form-control" rows="3" id="meta_keywords"><?php echo e($obj->meta_keywords); ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default">
                                        <label class="">Meta description</label>
                                        <textarea name="meta_description" class="form-control" rows="3" id="meta_description"><?php echo e($obj->meta_description); ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab4Media">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <p class="col-md-12">Featured Image</p>
                                    <div class="default-image-holder padding-5">
                                        <a href="javascript:void(0);" class="image-remove" data-remove-id="mediaId0"><i class="fa  fa-times-circle"></i></a>
                                        <a href="<?php echo e(route('admin.media.popup', ['popup_type'=>'single_image', 'type'=>'Image', 'holder_attr'=>'0', 'related_id'=>$obj->id])); ?>" class="open-ajax-popup" title="Media Images" data-popup-size="large" id="image-holder-0">
                                          <?php if($obj->media_id && $obj->featured_image): ?>
                                            <img class="card-img-top padding-20" src="<?php echo e(asset('public/'.$obj->featured_image->thumb_file_path)); ?>">
                                          <?php else: ?>
                                            <img class="card-img-top padding-20" src="<?php echo e(asset('assets/img/add_image.png')); ?>">
                                          <?php endif; ?>
                                        </a>
                                        <input type="hidden" name="media_id" id="mediaId0" value="<?php echo e($obj->media_id); ?>">
                                    </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <p class="col-md-12">Banner Image</p>
                                    <div class="default-image-holder padding-5">
                                        <a href="javascript:void(0);" class="image-remove" data-remove-id="mediaId1"><i class="fa  fa-times-circle"></i></a>
                                        <a href="<?php echo e(route('admin.media.popup', ['popup_type'=>'single_image', 'type'=>'Image', 'holder_attr'=>'1', 'related_id'=>$obj->id])); ?>" class="open-ajax-popup" title="Media Images" data-popup-size="large" id="image-holder-1">
                                          <?php if($obj->banner_id && $obj->banner_image): ?>
                                            <img class="card-img-top padding-20" src="<?php echo e(asset('public/'.$obj->banner_image->thumb_file_path)); ?>">
                                          <?php else: ?>
                                            <img class="card-img-top padding-20" src="<?php echo e(asset('assets/img/add_image.png')); ?>">
                                          <?php endif; ?>
                                        </a>
                                        <input type="hidden" name="banner_id" id="mediaId1" value="<?php echo e($obj->banner_id); ?>">
                                    </div>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" align="right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom'); ?>
        <script type="text/javascript" src="<?php echo e(asset('assets/keditor/js/ckeditor.js')); ?>"></script>
        <script type="text/javascript" src="<?php echo e(asset('assets/keditor/js/form-builder.min.js')); ?>"></script>
        <script type="text/javascript" src="<?php echo e(asset('assets/keditor/js/form-render.min.js')); ?>"></script>
        <!-- Start of KEditor scripts -->
        <script type="text/javascript" src="<?php echo e(asset('assets/keditor/js/keditor.js')); ?>"></script>
        <script type="text/javascript" src="<?php echo e(asset('assets/keditor/js/keditor-components.js')); ?>"></script>
        <!-- End of KEditor scripts -->
        <script type="text/javascript" src="<?php echo e(asset('assets/keditor/js/prettify.js')); ?>"></script>
        <script type="text/javascript" src="<?php echo e(asset('assets/keditor/js/beautify.js')); ?>"></script>
        <script type="text/javascript" src="<?php echo e(asset('assets/keditor/js/beautify-html.js')); ?>"></script>
        <script type="text/javascript" src="<?php echo e(asset('assets/keditor/js/keditor-custom.js')); ?>"></script>
        <script type="text/javascript" data-keditor="script">
            $(function () {
                $('#content-area').keditor();
            });
        </script>
    <script type="text/javascript">
        var validator = $('#PageFrm').validate({
            ignore: [],
            rules: {
                "name": "required",
                slug: {
                  required: true,
                  remote: {
                      url: "<?php echo e(route('validate.unique.page-slug')); ?>",
                      data: {
                        id: function() {
                          return $( "#inputId" ).val();
                      }
                    }
                  }
                },
                content: {
                  required: function(textarea) {
                     return $('#'+textarea.id).summernote('isEmpty');
                  }
                },
              },
              messages: {
                "name": "Page title cannot be blank",
                slug: {
                  required: "Slug cannot be blank",
                  remote: "Slug is already in use",
                },
                "description": "Page content cannot be blank",
              },
            });
    </script>
##parent-placeholder-c03e9099aad17cb58e4fff1d93d751105735c9c2##
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.common.datatable', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/pittappillil/resources/views/admin/pages/form.blade.php ENDPATH**/ ?>