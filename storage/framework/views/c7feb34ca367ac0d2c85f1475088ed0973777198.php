<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <meta name="google-signin-client_id" content="<?php echo e(config('services.google.client_id')); ?>">
    <title>SpiderWorks</title>
    <script>
        const baseUrl = '<?php echo e(URL::to('/')); ?>';
        const csrf = '<?php echo e(csrf_token()); ?>';
    </script>
    <!-- fonts -->
    
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <!-- css -->
    <link rel="stylesheet" href="<?php echo e(asset('client/vendor/bootstrap-4.2.1/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('client/vendor/owl-carousel-2.3.4/assets/owl.carousel.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('client/vendor/photoswipe-4.1.3/photoswipe.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('client/vendor/photoswipe-4.1.3/default-skin/default-skin.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('client/vendor/select2-4.0.10/css/select2.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('client/css/style.css')); ?>">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css" integrity="sha256-VxlXnpkS8UAw3dJnlJj8IjIflIWmDUVQbXD9grYXr98=" crossorigin="anonymous" />
    <link rel="stylesheet" href="<?php echo e(asset('client/css/style-new.css')); ?>">


    <!-- font - fontawesome -->
    <link rel="stylesheet" href="<?php echo e(asset('client/vendor/fontawesome-5.6.1/css/all.min.css')); ?>">
    <!-- font - stroyka -->

    <link rel="stylesheet" href="<?php echo e(asset('assets/plugins/jquery-ui/jquery-ui.min.css')); ?>">

    <style type="text/css">
        .modal-content .select2-container{
            width: 100% !important;
        }

        .jq-stars {
            display: inline-block;
        }

        .jq-rating-label {
            font-size: 22px;
            display: inline-block;
            position: relative;
            vertical-align: top;
            font-family: helvetica, arial, verdana;
        }

        .jq-star {
            width: 100px;
            height: 100px;
            display: inline-block;
            cursor: pointer;
        }

        .jq-star-svg {
            padding-left: 3px;
            width: 100%;
            height: 100% ;
        }

        .jq-star:hover .fs-star-svg path {
        }

        .jq-star-svg path {
            /* stroke: #000; */
            stroke-linejoin: round;
        }

        /* un-used */
        .jq-shadow {
            -webkit-filter: drop-shadow( -2px -2px 2px #888 );
            filter: drop-shadow( -2px -2px 2px #888 );
        }

        #forgot-password .modal-header, #update-password .modal-header{
            padding: 20px;
        }

        .error {
            color: red;
            font-size: 13px;
        }

        body{background:#fff}.container{max-width:1400px}.product-cat{padding:25px;background:#f5f5f5;width:calc(100% - 0px);position:relative}.product-cat::after{content:'';position:absolute;top:-2px;right:-2;bottom:-2;left:-2px;z-index:-1;border-radius:inherit;background:linear-gradient(0deg,rgba(224,7,21,1) 0,rgba(255,124,0,1) 100%);width:calc(100% + 4px);height:calc(100% + 4px)}.product-cat::before{content:'';position:absolute;top:0;right:0;bottom:0;left:0;z-index:0;border-radius:inherit;background:#fff;width:100%;height:50%}.product-data{position:relative;z-index:1;text-align:center}.product-data i{background:#333;color:#fff;width:25px;height:25px;text-align:center;border-radius:50%;line-height:25px;font-size:12px;margin-bottom:3px}.product-data p{margin:0;padding:0;text-align:center;font-size:10px!important;font-weight:600;line-height:12px}.product-list{border:5px solid #fff;box-shadow:0 4px 15px rgba(0,0,0,.2);margin-top:20px;float:left;width:100%}.product-cat a.product-btn{float:left;width:50%;background:#fff;font-size:14px;text-align:center;color:#000;font-weight:600;padding-bottom:10px}.product-cat a.product-btn img{width:100px;margin-bottom:5px;margin-top:20px}.product-cat a.product-btn span{width:100%;float:left}.product-cat .block-header{margin-bottom:0}.product-cat .block-header h3.block-header__title{text-align:center;width:100%;font-size:24px;font-weight:400;padding-bottom:15px;margin:20px 20px 20px;position:relative;color:#000}.product-cat .block-header h3.block-header__title::after{content:"";position:absolute;bottom:0;width:40%;border-bottom:1px dashed #666;left:30%}.product-cat a.more-btn{color:#e00715;text-align:center}.offer{border:1px solid #ccc;padding:35px 25px;background:#fff;margin:0 8px}.offer h3{font-size:24px;color:#000}.offer p{font-size:15px;color:#000}.offer img{float:right;margin-left:15px}.top-cat{padding:10px;background:#fff;width:calc(100% - 16px);margin:0 8px}.top-cat .block-header h3.block-header__title{text-align:center;width:100%;margin:20px 0;font-size:24px}.top-cat .product-card__image img{max-width:100%;display:block;position:absolute;max-height:290px;left:0;top:0;right:0;bottom:0;margin:auto}.top-cat a.more-btn{color:#e00715}.top-cat p{border-top:1px solid #ccc;margin:0;padding:5px 0 0;text-align:right}.owl-stage .container{position:relative;height:100%}.login-bg{background:#e00715;text-align:center}.login-popup h3{text-align:center;font-size:20px;color:#fff;font-weight:400;margin:10px 0 0}.login-popup .new-user-btn{color:#e00715!important;cursor:pointer}.login-popup .new-user-btn:hover{text-decoration:underline!important}.login-popup .close{position:absolute;z-index:10;font-size:30px;color:#fff;opacity:1;right:-25px;top:0}.or{position:relative;text-align:center;color:#333;font-weight:600}.or span{color:#333;background:#fff;box-shadow:0 0 5px rgba(0,0,0,.2);padding:10px;text-align:center;line-height:50px;position:relative;z-index:2}.or::after{content:"";position:absolute;top:50%;left:0;width:100%;height:1px;background:#ccc;z-index:1}.social-log{text-align:center}.social-log a{margin:0 15px;font-size:24px}.social-log a i{padding-bottom:10px;border-bottom:3px solid #ccc;width:30px}.social-log a .fa-facebook-f{color:#3b5998}.social-log a .fa-twitter{color:#00acee}.social-log a .fa-google-plus-g{color:#dd4b39}.social-log a:hover .fa-facebook-f{color:#3b5998;border-bottom:3px solid #3b5998}.social-log a:hover .fa-twitter{color:#00acee;border-bottom:3px solid #00acee}.social-log a:hover .fa-google-plus-g{color:#dd4b39;border-bottom:3px solid #dd4b39}.field-icon{float:right;margin-top:-25px;position:relative;z-index:2;right:5px}.sticky{position:sticky;top:64px;bottom:0;z-index:2;-webkit-align-self:flex-start;-ms-flex-item-align:start;align-self:flex-start}.contact-us__container .table{font-size:14px}.block-slideshow--layout--with-departments .block-slideshow__body{height:auto}.block-slideshow{margin-bottom:50px;position:relative;margin:0 auto 50px;max-width:2000px}.block{margin-bottom:25px}.block-features--layout--classic .block-features__divider{margin-top:45px;margin-bottom:45px}.block-features__list{border:none}.popover{max-width:800px}.item-list ul,.item-list ul li{list-style:none;list-style-type:none;padding:0;margin:0}.hide{display:none}.head-list{font-weight:600;margin-bottom:10px}.pop-ov-list .form-check-input{margin-top:6px!important}.active-button-addcart .product-card__buttons{border-top:1px solid #e2e2e2;padding-top:10px;display:flex!important}.block-features__icon{width:60px;height:60px;border-radius:50%;background:#f5f5f5;padding:10px;webkit-transition:all .5s ease;-webkit-transition-delay:0s;-moz-transition:all .5s ease 0s;-o-transition:all .5s ease 0s;transition:all .5s ease 0s}.block-features__item:hover .block-features__icon{fill:#f5f5f5;background:#e00715}.block-features__item:hover .block-features__title{color:#e00715}.block-features__title{webkit-transition:all .5s ease;-webkit-transition-delay:0s;-moz-transition:all .5s ease 0s;-o-transition:all .5s ease 0s;transition:all .5s ease 0s}a.banner-anchor{display:inline-block;position:relative}a.banner-anchor:after,a.banner-anchor:before{content:"";bottom:0;box-sizing:border-box;content:"";left:0;position:absolute;right:0;top:0;transition:all .6s ease-in-out 0s;-webkit-transition:all .6s ease-in-out 0s;-moz-transition:all .6s ease-in-out 0s;-ms-transition:all .6s ease-in-out 0s;-o-transition:all .6s ease-in-out 0s}.ads-all:hover a.banner-anchor:before{background:rgba(255,255,255,.15);left:50%;right:50%}.ads-all:hover a.banner-anchor:after{background:rgba(255,255,255,.15);bottom:50%;top:50%}@media (max-width:1450px){.container{max-width:1140px}.block-slideshow--layout--with-departments .block-slideshow__slide{height:268px}.product-cat{padding:10px}.product-cat .block-header h3.block-header__title{font-size:20px}}@media (max-width:992px){.block-slideshow .owl-carousel .owl-dots{display:none}.top-cat{margin:8px 8px}.product-cat{margin:10px 0}.block-features--layout--classic .block-features__divider{margin-top:0;margin-bottom:0}}.modal-header{padding:0}.modal-header .close{padding:10px 15px}.modal-header ul{border:none}.modal-header ul li{margin:0}.modal-header ul li a{border:none;border-radius:0}.modal-header ul li.active a{color:#e12f27}.modal-header ul li a:hover{border:none}.modal-header ul li a span{margin-left:10px}.modal-body .form-group{margin-bottom:10px}
        .block-slideshow .owl-carousel .owl-dots {
            display: none !important;
        }
        .ads-all{
            text-align: center;
        }

        .product-card__name a {
            font-weight: 600;
            text-transform: capitalize;
        }

        .widget-filters__item{
            max-height: 350px;
            overflow: scroll;
            overflow-x: hidden;
        }

        body{
            font-family: Roboto,"sans-serif" !important;
        }
        .topbar-link:hover {
            color: #d4d4d4;
        }


    </style>
    <?php $__env->startSection('head'); ?>
    <?php echo $__env->yieldSection(); ?>
</head>

<body>
<?php if(auth()->guard()->check()): ?>
<?php if(!Auth::user()->email_verified_at  && Auth::user()->email): ?>
<div class="container-fluid verify-enable" align="center" style="background: linear-gradient(45deg, #03A9F4, #E91E63);padding: 5px;color: white;font-weight: bold;" data-encrypt-type="<?php echo e(encrypt('email')); ?>" data-type="email" data-note="verify_email">
Please verify your email address - <?php echo e(Auth::user()->email); ?> - <span style="text-decoration: underline;cursor: pointer">Verify now</span>
</div>
<?php endif; ?>
<?php if(!Auth::user()->phone_verified_at && Auth::user()->username): ?>
    <div class="container-fluid verify-enable" align="center" style="background: linear-gradient(45deg, #b9da25, #11c9bb);padding: 5px;color: white;font-weight: bold;" data-encrypt-type="<?php echo e(encrypt('mobile')); ?>" data-type="mobile" data-note="verify_mobile">
        Please verify your mobile number - <?php echo e(Auth::user()->username); ?> - <span style="text-decoration: underline;cursor: pointer">Verify now</span>
    </div>
<?php endif; ?>
<?php endif; ?>

<!-- quickview-modal -->
<div id="quickview-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content"></div>
    </div>
</div>
<!-- quickview-modal / end -->
<!-- mobilemenu -->
<div class="mobilemenu">
    <div class="mobilemenu__backdrop"></div>
    <div class="mobilemenu__body">
        <div class="mobilemenu__header">
            <div class="mobilemenu__title">Menu</div>
            <button type="button" class="mobilemenu__close">
                <svg width="20px" height="20px">
                    <use xlink:href="<?php echo e(URL::asset('client')); ?>/images/sprite.svg#cross-20"></use>
                </svg>
            </button>
        </div>
        <?php echo $__env->make('client.includes.mobile_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
</div>
<!-- mobilemenu / end -->
<!-- site -->
<div class="site">
    <!-- mobile site__header -->
    <?php echo $__env->make('client.includes.mobile_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <!-- mobile site__header / end -->
    <!-- desktop site__header -->
    <?php echo $__env->make('client.includes.site_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <!-- desktop site__header / end -->
    <!-- site__body -->

    <div class="site__body" id="app">
            <?php $__env->startSection('content'); ?>
            <?php echo $__env->yieldSection(); ?>
    </div>
    <!-- site__body / end -->
    <!-- site__footer -->
    <?php echo $__env->make('client.includes.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <!-- site__footer / end -->
</div>
<!-- site / end -->
<!-- photoswipe -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>
                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                <!--<button class="pswp__button pswp__button&#45;&#45;share" title="Share"></button>-->
                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>
            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>
        </div>
    </div>
</div>

<!-- login-modal -->
          
            <div id="login-register"  class="modal fade"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content login-popup">
                
                  <div class="modal-body p-0">




                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    
                    <div class="row m-0 login-cntr">
                        <div class="col-md-5 login-bg d-flex flex-column justify-content-center ">
                           <a href="index.html">
                             <img src="<?php echo e(URL::asset('client')); ?>/images/PITTAPPILLIL-LOGO.png" class="" width="220">
                        </a>
                        <h3> Shop for your favourite home appliance at best prices</h3>
                        </div>
                        <div class="col-md-7 p-0">

                             <div class="">
                                <div class="card-body">
                                    <h3 class="card-title">Login</h3>
                                    <div class="login-errors mt-2 mb-2 text-danger"></div>
                                    <form method="POST" action="<?php echo e(route('login')); ?>" id="loginForm">
                                      <?php echo csrf_field(); ?>
                                        <input type="hidden" name="referrer_page" value="<?php echo e(Request::path()); ?>">
                                        <div class="form-group">
                                            <label>Email address / Phone Number</label>
                                            <input id="login" type="text" class="form-control <?php if ($errors->has('login')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('login'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="login" value="<?php echo e(old('login')); ?>" required autocomplete="email" placeholder="Enter email or Phone">
                                        </div>
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input id="password-field" type="password" class="form-control" placeholder="Password" name="password">
                                             <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                            <small class="form-text text-muted">
                                                <a href="<?php echo e(route('password.request')); ?>" class="open-forgot">Forgotten Password</a>
                                            </small>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-check">
                                                <span class="form-check-input input-check">
                                                    <span class="input-check__body">
                                                        <input class="input-check__input" type="checkbox" name="remember" id="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>>
                                                        <span class="input-check__box"></span>
                                                        <svg class="input-check__icon" width="9px" height="7px">
                                                            <use xlink:href="<?php echo e(URL::asset('client')); ?>/images/sprite.svg#check-9x7"></use>
                                                        </svg>
                                                    </span>
                                                </span>
                                                <label class="form-check-label" for="login-remember">Remember Me</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                        <button type="submit" class="btn btn-primary float-left" id="login-btn">Login</button> 
                                        <a class=" mt-1 ml-2 float-left new-user-btn" id="reg-btn">Create New account?</a>
                                        <div class="clearfix"></div>
                                    </div>


                                    <div class="form-group">                                       
                                      <div class="or"><span>or</span></div>
                                    </div>


                                     <div class="form-group text-center m-0">  
                                      <a href="javascript:void(0);"  onclick="fbLogin()" id="fbLink"><i class="fab fa-facebook-f"></i> Facebook</a> | 
                                      <a href="javascript:void(0);"  id="googleSignIn"><i class="fab fa-google"></i> Google</a>                                   
                                    </div>

                                    </form>
                                </div>
                            </div>



                          
                        </div>
                    </div>




                     <div class="row m-0  reg-cntr" style="display: none;">
                        <div class="col-md-5 login-bg d-flex flex-column justify-content-center ">
                           <a href="index.html">
                             <img src="<?php echo e(URL::asset('client')); ?>/images/PITTAPPILLIL-LOGO.png" class="" width="220">
                        </a>
                        <h3> Shop for your favourite home appliance at best prices</h3>
                        </div>
                        <div class="col-md-7 p-0">

                            



                            <div >
                                <div class="card-body">
                                    <h3 class="card-title">Register</h3>
                                    <div class="register-errors mt-2 mb-2 text-danger"></div>
                                    <form method="POST" action="<?php echo e(route('register')); ?>" id="registerForm">
                                      <?php echo csrf_field(); ?>
                                            <input type="hidden" name="referrer_page" value="<?php echo e(Request::path()); ?>">
                                        <div class="form-group">
                                            <label>Email address / Phone Number</label>
                                            <div class="input-group mb-2">
                                              <div class="input-group-prepend" style="display: none;" id="phone-holder">
                                                <div class="input-group-text">+91</div>
                                              </div>
                                              <input id="phone_email" type="input" class="form-control <?php if ($errors->has('phone_email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('phone_email'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="phone_email" value="<?php echo e(old('phone_email')); ?>" required autocomplete="phone_email" placeholder="Enter email or phone">
                                                  <?php if ($errors->has('phone_email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('phone_email'); ?>
                                                      <span class="invalid-feedback" role="alert">
                                                          <strong><?php echo e($message); ?></strong>
                                                      </span>
                                                  <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                              <input type="hidden" name="email" readonly="" id="hidden_email">
                                              <input type="hidden" name="username" readonly="" id="hidden_phone">
                                            </div>
                                        </div>
                                         <div class="form-group row">
                                            <div class="col-md-6">
                                             <label>First Name</label>
                                             <input id="first_name" type="text" class="form-control <?php if ($errors->has('first_name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('first_name'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="first_name" value="<?php echo e(old('first_name')); ?>" required autocomplete="first_name" placeholder="First Name ">
                                            </div>
                                            <div class="col-md-6">
                                                <label>Last Name</label>
                                                <input id="last_name" type="text" class="form-control <?php if ($errors->has('last_name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('last_name'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="last_name" value="<?php echo e(old('last_name')); ?>" autocomplete="last_name" placeholder="Last Name ">
                                            </div>
                                            
                                        </div>

                                       
                                        <div class="form-group">
                                             <input id="password-field2" type="password" class="form-control" placeholder="Password" name="password">
                                             <span toggle="#password-field2" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                        </div>
                                      
                                        <div class="form-group">
                                        <button type="submit" class="btn btn-primary float-left" id="register-btn">Register</button> 
                                        <a class=" mt-1 ml-2 float-left new-user-btn" id="log-btn">Login</a>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="form-group">                                       
                                      <div class="or"><span>or</span></div>
                                    </div>


                                     <div class="form-group text-center m-0">  
                                      <a href="javascript:void(0);"  onclick="fbLogin()" id="fbLink"><i class="fab fa-facebook-f"></i> Facebook</a> | 
                                      <a href="javascript:void(0);"  id="googleSignUp"><i class="fab fa-google"></i> Google</a>                                   
                                    </div>
                              
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>


                  </div>
                 
                </div>
              </div>
            </div>
    <!-- login-modal / end -->



<div class="modal fade" id="forgot-password" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Forgot Password</h5>
            <a  class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </a>
         </div>
         <form action="<?php echo e(route('password.email')); ?>" id="forgotPasswordForm" method="post" class="form-horizontal style-form" enctype="multipart/form-data">
              <?php echo csrf_field(); ?>
         <div class="modal-body login-main">
            <div class="form-group">
               <input type="email"  class="form-control" name="email" id="email_forgot" value="" placeholder="Email Id">
            </div>

            <div class="form-group form-check">
               <button type="submit" class="reg-btn btn btn-danger reset-btn"><?php echo e(__('Send Password Reset Link')); ?></button>
            </div>
         </div>
       </form>
      </div>
   </div>
</div>

<div class="modal fade" id="common-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content p-3">
         
      </div>
   </div>
</div>

<?php if(auth()->guard()->check()): ?>
<div class="modal fade" id="update-password" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Update Password</h5>
            <a  class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </a>
         </div>
         <div class="modal-body ">
            <form class="form-horizontal" method="POST" action="<?php echo e(route('account.update-password')); ?>" id="updatePasswordFrm">
                        <?php echo e(csrf_field()); ?>

                        <div class="form-group">
                          <label>Current Password</label>
                          <input id="current-password" type="password" class="form-control" name="current-password" required>
                          <span id="current-password_error"></span>
                        </div>
                        <div class="form-group">
                          <label>New Password</label>
                          <input id="new-password" type="password" class="form-control" name="new-password" required>
                          <span id="new-password_error"></span>
                        </div>
                        <div class="form-group">
                          <label>Confirm New Password</label>
                          <input id="new-password-confirm" type="password" class="form-control" name="new-password_confirmation" required>
                        </div>
                        <div class="form-group">
                           <button type="submit" class="reg-btn btn btn-primary" id="change-password-btn">Change Password</button>
                        </div>
                    </form>
         </div>
      </div>
   </div>
</div>
<?php endif; ?>

<!-- photoswipe / end -->
<!-- js -->
<script>
    const image_upload_url = baseUrl+'image/upload';
</script>
<script src="<?php echo e(asset('client/vendor/jquery-3.3.1/jquery.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/app.js')); ?>"></script>

<script src="<?php echo e(asset('client/vendor/bootstrap-4.2.1/js/bootstrap.bundle.min.js')); ?>"></script>
<script src="<?php echo e(asset('client/vendor/owl-carousel-2.3.4/owl.carousel.min.js')); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js" integrity="sha256-Ka8obxsHNCz6H9hRpl8X4QV3XmhxWyqBpk/EpHYyj9k=" crossorigin="anonymous"></script>
<script src="<?php echo e(asset('client/vendor/photoswipe-4.1.3/photoswipe.min.js')); ?>"></script>
<script src="<?php echo e(asset('client/vendor/photoswipe-4.1.3/photoswipe-ui-default.min.js')); ?>"></script>

<script src="<?php echo e(asset('client/vendor/select2-4.0.10/js/select2.min.js')); ?>" ></script>
<script src="<?php echo e(asset('client/js/number.js')); ?>"></script>
<script src="<?php echo e(asset('client/js/main.js')); ?>"></script>
<script src="<?php echo e(asset('client/vendor/svg4everybody-2.1.9/svg4everybody.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/plugins/jquery-validation/js/jquery.validate.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/plugins/jquery-ui/jquery-ui.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/jquery.star-rating-svg.min.js')); ?>"></script>

<script src="<?php echo e(asset('js/spiderworks.min.js')); ?>"></script>

<script>
    var current_page = "<?php echo e(Request::path()); ?>";
    var url_autocomplete = "<?php echo e(url('autocomplete')); ?>";
</script>
<script src="<?php echo e(asset('client/js/pittappillil.js')); ?>"></script>
<?php $__env->startSection('bottom'); ?>
<?php echo $__env->yieldSection(); ?>

<script src="https://apis.google.com/js/platform.js?onload=onLoadGoogleCallback" async defer></script>
<script !src="">
    const appid = "<?php echo e(config('services.facebook.client_id')); ?>";
    const login_callback = "<?php echo e(url('login/save-callback')); ?>";
    const client_id = "<?php echo e(config('services.google.client_id')); ?>";
</script>
<script src="<?php echo e(asset('client/js/socialauth.min.js')); ?>"></script>
<script>



    $(document).ready(function () {
        $.get(baseUrl+'/carttotal').done(function (data){
            $('.cartcount').html(data);
        });

        $.get(baseUrl+'/wishlisttotal').done(function (data){
            $('.wishlistcount').html(data);
        });
    })



    $(document).on('click', '.verify-enable', function() {
        var type = $(this).data('type');
        var note = $(this).data('note');
        var e_type = $(this).data('encrypt-type');
        var dom = $(this);
        dom.html('Sending.....');
        $.post('<?php echo e(url('/send/otp')); ?>',{_token:csrf,type:type}).done(function (data) {
            var obj = JSON.parse(data);
            console.log(obj.status)
            dom.html('OTP sent, Please check your '+type+' for OTP');
            $.confirm({
                title: 'Please enter the otp!',
                content: '' +
                    '<form action="" class="formName">' +
                    '<div class="form-group">' +
                    '<label>Please enter the otp we send to your '+type+'</label>' +
                    '<input type="email" placeholder="eg, 523 890" class="otp form-control" required />' +
                    '</div>' +
                    '</form>',
                buttons: {
                    formSubmit: {
                        text: 'Submit',
                        btnClass: 'btn-blue',
                        action: function () {
                            var otp = this.$content.find('.otp').val();


                            if (!otp) {
                                $.alert('Please enter the otp');
                                return false;
                            }

                            $.post('<?php echo e(url('otp/verify')); ?>', {_token: csrf, otp: otp, type:e_type, note:note}).done(function (data) {
                                var obj = JSON.parse(data);
                                console.log(obj.status)
                                if (!obj.status) {
                                    $.alert('The given OTP is Invalid.');
                                    return false;
                                }
                                if (obj.type == 'email') {
                                    $.alert('Thanks!, Your email address is verified!');
                                    setTimeout(function() {
                                        location.reload()
                                    },750)
                                }

                                if (obj.type == 'mobile') {
                                    $.alert('Thanks!, Your mobile number is verified!');
                                    setTimeout(function() {
                                        location.reload()
                                    },750)
                                }
                            })


                        }
                    },
                    cancel: function () {
                        //close
                    },
                },
            });
        })
    });

</script>
</body>
</html><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/pittappillil/resources/views/client/base.blade.php ENDPATH**/ ?>