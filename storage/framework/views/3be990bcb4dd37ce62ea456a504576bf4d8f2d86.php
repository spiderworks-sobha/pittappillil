<?php $__env->startSection('head'); ?>
    <style>
        .post-card__image img {
            max-width: 100%;
            max-height: 200px;
        }
        .post-card__content{
            margin-top: 0px !important;
        }
        .posts-list__item{
            margin: 5px 16px;
            border: 1px solid #f3f3f3;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="page-header">
        <div class="page-header__container container">
            <div class="page-header__breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo e(URL::to('/')); ?>">Home</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="<?php echo e(asset('client')); ?>/images/sprite.svg#arrow-rounded-right-6x9"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">News</li>
                    </ol>
                </nav>
            </div>
            <div class="page-header__title">
                <h1>Latest News</h1>
            </div>
        </div>
    </div>

    <div class="container" style="margin-top: 25px">
        <div class="row">
            <div class="col-12 col-lg-12">
             <div class="block">
                <?php if(count($pages)>0): ?>
                <div class="posts-view">
                   <div class="posts-view__list posts-list posts-list--layout--classic">
                      <div class="posts-list__body">
                        <?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                         <div class="posts-list__item" >
                           <div class="post-card post-card--layout--list post-card--size--nl">
                              <div class="post-card__image" align="center"><a href="<?php echo e(url('blog', [$page->slug])); ?>"><img src="<?php if($page->featured_image): ?> <?php echo e(asset($page->featured_image->file_path)); ?> <?php else: ?> <?php echo e(asset('images/default.png')); ?> <?php endif; ?>" alt=""></a></div>
                              <div class="post-card__info">
                                 <div class="post-card__name"><a href="<?php echo e(url('blog', [$page->slug])); ?>"><h2 style="font-size: 25px"><?php echo e($page->primary_heading); ?></h2></a></div>
                                 <div class="post-card__category w-100"><?php echo e(date('F, d Y', strtotime($page->updated_at))); ?></div>
                                 <div class="post-card__content"><?php echo e($page->short_description); ?></div>
                                 <div class="post-card__read-more"><a href="<?php echo e(url('blog', [$page->slug])); ?>" class="btn btn-secondary btn-sm">Read More</a></div>
                              </div>
                           </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </div>
                   </div>
                   <div class="posts-view__pagination">
                      <?php echo e($pages->links('client.includes.pagination')); ?>

                   </div>
                </div>
                <?php else: ?>
                    <div class="nothing-to-display"> Oops...Nothing to display...</div>
                <?php endif; ?>
             </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom'); ?>
##parent-placeholder-c03e9099aad17cb58e4fff1d93d751105735c9c2##

<?php $__env->stopSection(); ?>

<?php echo $__env->make('client.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/pittappillil/resources/views/pages/news.blade.php ENDPATH**/ ?>