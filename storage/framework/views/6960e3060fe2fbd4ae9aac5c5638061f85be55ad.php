<header class="site__header d-lg-none">
    <!-- data-sticky-mode - one of [pullToShow, alwaysOnTop] -->
    <div class="mobile-header mobile-header--sticky" data-sticky-mode="pullToShow">
        <div class="mobile-header__panel">
            <div class="container">
                <div class="mobile-header__body">
                    <button class="mobile-header__menu-button">
                        <svg width="18px" height="14px">
                            <use xlink:href="<?php echo e(URL::asset('client')); ?>/images/sprite.svg#menu-18x14"></use>
                        </svg>
                    </button>
                    <a class="mobile-header__logo" href="<?php echo e(URL::to('/')); ?>">
                        Pittappillil
                    </a>
                    <div class="mobile-header__search">
                        <form class="mobile-header__search-form" action="">
                            <input class="mobile-header__search-input" name="search" placeholder="Search over 10,000 products" aria-label="Site search" type="text" autocomplete="off">
                            <button class="mobile-header__search-button mobile-header__search-button--submit" type="submit">
                                <svg width="20px" height="20px">
                                    <use xlink:href="<?php echo e(URL::asset('client')); ?>/images/sprite.svg#search-20"></use>
                                </svg>
                            </button>
                            <button class="mobile-header__search-button mobile-header__search-button--close" type="button">
                                <svg width="20px" height="20px">
                                    <use xlink:href="<?php echo e(URL::asset('client')); ?>/images/sprite.svg#cross-20"></use>
                                </svg>
                            </button>
                            <div class="mobile-header__search-body"></div>
                        </form>
                    </div>
                    <div class="mobile-header__indicators">
                        <div class="indicator indicator--mobile-search indicator--mobile d-sm-none">
                            <button class="indicator__button">
                                        <span class="indicator__area">
                                            <svg width="20px" height="20px">
                                                <use xlink:href="<?php echo e(URL::asset('client')); ?>/images/sprite.svg#search-20"></use>
                                            </svg>
                                        </span>
                            </button>
                        </div>

                        <div class="indicator indicator--mobile d-sm-flex d-none">
                            <a href="<?php echo e(URL::to('wishlist')); ?>" class="indicator__button">
                                        <span class="indicator__area">
                                            <svg width="20px" height="20px">
                                                <use xlink:href="<?php echo e(URL::asset('client')); ?>/images/sprite.svg#heart-20"></use>
                                            </svg>
                                            <span class="indicator__value">0</span>
                                        </span>
                            </a>
                        </div>
                        <div class="indicator indicator--mobile">
                            <a href="<?php echo e(URL::to('cart')); ?>" class="indicator__button">
                                        <span class="indicator__area">
                                            <svg width="20px" height="20px">
                                                <use xlink:href="<?php echo e(URL::asset('client')); ?>/images/sprite.svg#cart-20"></use>
                                            </svg>
                                            <span class="indicator__value cartcount">-</span>
                                        </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/pittappillil/resources/views/client/includes/mobile_header.blade.php ENDPATH**/ ?>