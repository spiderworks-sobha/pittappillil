<?php if(isset($config['name']) && $config['name'] == 'My Account Top Menu'): ?>
    <?php $__currentLoopData = $menu_items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if($item->slug != 'logout'): ?>
        <li class="menu__item">
            <div class="menu__item-submenu-offset"></div>
            <a class="menu__item-link" href="<?php echo e($item->slug); ?>"><?php echo e($item->title); ?></a>
        </li>
        <?php endif; ?>
        <?php if($item->slug == 'logout'): ?>
            <li class="menu__item">
                <div class="menu__item-submenu-offset"></div>
                <a class="menu__item-link" href="javascript::void(0);" onclick="event.preventDefault(); document.getElementById('logout-form-menu').submit();"><?php echo e($item->title); ?></a>
                <form id="logout-form-menu" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                           <?php echo csrf_field(); ?>
                                        </form>
            </li>
        <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php else: ?>
    <?php $__currentLoopData = $menu_items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="topbar__item topbar__item--link">
            <a class="topbar-link" href="<?php echo e($item->slug); ?>"><?php echo e($item->title); ?></a>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?><?php /**PATH C:\wamp\www\pittappillil\resources\views/widgets/top_menu.blade.php ENDPATH**/ ?>