<?php $__env->startSection('head'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="page-header">
        <div class="page-header__container container">
            <div class="page-header__breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo e(url('/')); ?>">Home</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="<?php echo e(asset('client')); ?>/images/sprite.svg#arrow-rounded-right-6x9"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item">
                            <?php if($page->type == 'News'): ?>
                                <a href="<?php echo e(url('news')); ?>">News</a>
                            <?php elseif($page->type == 'Blog'): ?>
                                <a href="<?php echo e(url('blogs')); ?>">Blogs</a>
                            <?php endif; ?>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="<?php echo e(asset('client')); ?>/images/sprite.svg#arrow-rounded-right-6x9"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page"><?php echo e($page->name); ?></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 col-lg-9 col-xl-8">
                <h1 class="post-header__title text-center"><?php echo e($page->primary_heading); ?></h1>
                <div class="post-header__meta m-2">
                    <div class="post-header__meta-item" style="margin: 0 auto;"><?php echo e(date('F, d Y', strtotime($page->updated_at))); ?></div>
                </div>
                <div class="post__featured"><img src="<?php if($page->banner_image): ?> <?php echo e(asset($page->banner_image->file_path)); ?> <?php else: ?> <?php echo e(asset('images/default.png')); ?> <?php endif; ?>" alt=""></a></div>
                <div class="post__content typography typography--expanded"><?php echo $page->content; ?></div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom'); ?>
##parent-placeholder-c03e9099aad17cb58e4fff1d93d751105735c9c2##

<?php $__env->stopSection(); ?>

<?php echo $__env->make('client.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/pittappillil/resources/views/pages/index.blade.php ENDPATH**/ ?>