<?php $__env->startSection('head'); ?>
<style>
    #googleMap{
            width:100%;
            height:100%;
    }
    .site-footer {
        margin-top: 0px;
    }
    .block {
        margin-bottom: 0px;
    }
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="page-header">
        <div class="page-header__container container">
            <div class="page-header__breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo e(url('/')); ?>">Home</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="<?php echo e(asset('client')); ?>/images/sprite.svg#arrow-rounded-right-6x9"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Store Locator</li>
                    </ol>
                </nav>
            </div>
            <div class="page-header__title">
                <h1>Store Locator</h1>
            </div>
        </div>
    </div>
    <div class="block-map block " style="position: relative;">
   <div class="block-map__body locator-img">
      <div id="googleMap"></div>
   </div>
   <div class="block locator-list" >
      <div class="container" >
         <div class="card mb-0" >
            <div class="card-body contact-us">
               <div class="contact-us__container">
                  <div class="row">
                     <div class="col-12 col-lg-12 mb-4">
                        <form action="<?php echo e(url('store-locator')); ?>" class="m-3" method="POST">
                            <?php echo csrf_field(); ?>
                            <div class="form-row">
                              <div class="form-group col-md-12">
                                 <select class="form-control" placeholder="Select Districts" id="searchDistrict" name="district">
                                    <option value="">Select District</option>
                                    <?php if(count($districts)>0): ?>
                                        <?php $__currentLoopData = $districts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $district): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($district->id); ?>" <?php if($district->id == $selected_district): ?> selected="selected" <?php endif; ?>><?php echo e($district->district_name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </select>
                              </div>
                            </div>
                            <div class="form-row">
                              <div class="form-group col-md-12">
                                    <select class="form-control" placeholder="Select City" id="searchLocation" name="location">
                                        <option value="">Select Location</option>
                                        <?php if($locations): ?>
                                            <?php $__currentLoopData = $locations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $location): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($location->id); ?>" <?php if($location->id == $selected_location): ?> selected="selected" <?php endif; ?>><?php echo e($location->landmark); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php else: ?>
                                            <option value="">[Select a district first]</option>
                                        <?php endif; ?>
                                    </select>
                              </div>
                           </div>
                           <button type="submit" class="btn btn-primary float-right">Search</button>
                        </form>
                     </div>
                     <div class="col-12 col-lg-12 pb-4 pb-lg-0">
                        <div class="contact-us__address address-list">
                            <?php if(count($branches)>0): ?>
                                <?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=> $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div>
                                        <a href="<?php echo e(url('store-locator', [$branch->slug])); ?>">
                                            <span> <?php echo e($key+1); ?> </span>
                                            <p>
                                                <strong class="title"><?php echo e($branch->page_heading); ?></strong><br/>
                                                <?php echo nl2br($branch->address); ?>

                                             </p>
                                        </a>
                                    </div>
                                    <?php if(count($branches) != $key+1): ?>
                                        <hr/>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom'); ?>
##parent-placeholder-c03e9099aad17cb58e4fff1d93d751105735c9c2##
<script>
    var markers = <?php echo json_encode($branches_json, 15, 512) ?>;
    markers = jQuery.parseJSON(markers);

    function initMap()
    {
        var mapOptions = {
            zoom: 9,
            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);
        var infoWindow = new google.maps.InfoWindow();
        var lat_lng = new Array();
        var latlngbounds = new google.maps.LatLngBounds();
          
        for (i = 0; i < markers.length; i++) {
            var data = markers[i];
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            lat_lng.push(myLatlng);
            var marker = new google.maps.Marker({
                            position: myLatlng,
                            map: map,
                            title: data.title,

            });

            latlngbounds.extend(marker.position);
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent(data.description);
                    infoWindow.open(map, marker);
                });
            })(marker, data);
        }

        map.setCenter(latlngbounds.getCenter());
        map.fitBounds(latlngbounds);
            
    }

    $(function(){
        $(document).on('change', '#searchDistrict', function(){
            var type_id = $(this).val();
            var target = $('#searchLocation');
            var options = '<option value="">[ Select a district first ]</option>';
            if(parseInt(type_id)>0){
                target.html('<option value="">Loading...</option>');
                $.getJSON(baseUrl  + '/ajax/locations/' + type_id, function(jsonData){
                    if ( jsonData.length != 0 ) {
                        options = '<option value="">--- Select a location ---</option>';
                        $.each(jsonData, function(i,data) {
                            options +='<option value="'+data.id+'">'+data.text+'</option>';
                        });
                    } else {
                        options = '<option value="">[No locations listed in this district]</option>';
                    }
                    target.html(options).change();
                });
            } else {
                target.html(options).change();
            }
        })
    })

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKNmMGKJYRzfsMF5Jom1Rj5-VPIoyLbak&libraries=places&callback=initMap" type="text/javascript"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('client.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp\www\pittappillil\resources\views/client/branches/index.blade.php ENDPATH**/ ?>