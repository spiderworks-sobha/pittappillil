<header class="site__header d-lg-block d-none"><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <div class="site-header">
        <!-- .topbar -->
        <div class="site-header__topbar topbar">
            <div class="topbar__container container">
                <div class="topbar__row">
                    <?php echo app('arrilot.widget')->run('TopMenu', ['name'=>'Top Menu']); ?>
                    <div class="topbar__spring"></div>
                    <?php if(auth()->guard()->check()): ?>
                        <div class="topbar__item">
                            <div class="topbar-dropdown">
                                <button class="topbar-dropdown__btn" type="button">
                                    <?php if(auth()->guard()->check()): ?> <?php echo e(Auth::user()->first_name); ?> <?php else: ?> My Account <?php endif; ?>
                                    <svg width="7px" height="5px">
                                        <use xlink:href="<?php echo e(URL::asset('client')); ?>/images/sprite.svg#arrow-rounded-down-7x5"></use>
                                    </svg>
                                </button>
                                <div class="topbar-dropdown__body">
                                    <!-- .menu -->
                                    <div class="menu menu--layout--topbar ">
                                        <div class="menu__submenus-container"></div>
                                        <ul class="menu__list">
                                            <?php echo app('arrilot.widget')->run('TopMenu', ['name'=>'My Account Top Menu']); ?>
                                        </ul>
                                    </div>
                                    <!-- .menu / end -->
                                </div>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="topbar__item topbar__item--link">
                            <a class="topbar-link" href="javascript:void(0)" id="open-register">Register</a>
                        </div>
                        <div class="topbar__item topbar__item--link">
                            <a class="topbar-link" href="javascript:void(0)" id="open-login">Login</a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <!-- .topbar / end -->
        <div class="site-header__middle container">
            <div class="site-header__logo">
                <a href="<?php echo e(URL::to('/')); ?>">
                    <img src="<?php echo e(Key::get('logo_img')); ?>" alt=""   style="width: 170px;">
                </a>
            </div>
            
            <div class="site-header__search">
                <div class="search">
                    <form class="search__form" action="" id="GlobalSearchForm">
                        <input class="search__input" name="search" id="search" placeholder="<?php echo e(Key::get('search_placeholder')); ?>" aria-label="Site search" type="text" <?php if(isset($keyword)): ?> value="<?php echo e($keyword); ?>" <?php endif; ?>>
                        <button class="search__button" type="button" id="GlobalSearchBtn">
                            <svg width="20px" height="20px">
                                <use xlink:href="<?php echo e(URL::asset('client')); ?>/images/sprite.svg#search-20"></use>
                            </svg>
                        </button>
                        <div class="search__border"></div>
                    </form>
                </div>
            </div>
            <div class="site-header__phone">
                <div class="site-header__phone-title">Customer Service</div>
                <div class="site-header__phone-number"><?php echo e(Key::get('cs_number')); ?></div>
            </div>
        </div>
        <div class="site-header__nav-panel">
            <!-- data-sticky-mode - one of [pullToShow, alwaysOnTop] -->
            <div class="nav-panel nav-panel--sticky" data-sticky-mode="pullToShow">
                <div class="nav-panel__container container">
                    <div class="nav-panel__row">

                        <div class="nav-panel__nav-links nav-links">
                            <ul class="nav-links__list">
                                <?php echo app('arrilot.widget')->run('MainMenu', ['menu_position' => 'Main Menu']); ?>
                            </ul>
                        </div>
                        <!-- .nav-links / end -->
                        <div class="nav-panel__indicators">
                            <div class="indicator">
                                <a href="<?php echo e(URL::to('wishlist')); ?>" class="indicator__button">
                                            <span class="indicator__area">
                                                <svg width="20px" height="20px">
                                                    <use xlink:href="<?php echo e(URL::asset('client')); ?>/images/sprite.svg#heart-20"></use>
                                                </svg>
                                                <span class="indicator__value wishlistcount">-</span>
                                            </span>
                                </a>
                            </div>
                            <div class="indicator">
                                <a href="<?php echo e(URL::to('cart')); ?>" class="indicator__button">
                                            <span class="indicator__area">
                                                <svg width="20px" height="20px">
                                                    <use xlink:href="<?php echo e(URL::asset('client')); ?>/images/sprite.svg#cart-20"></use>
                                                </svg>
                                                <span class="indicator__value cartcount">-</span>
                                            </span>
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/pittappillil/resources/views/client/includes/site_header.blade.php ENDPATH**/ ?>