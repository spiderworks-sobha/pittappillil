<?php $__env->startSection('content'); ?>
    <div class="container-fluid">

        <div class="col-md-12" style="margin-bottom: 20px;" align="right">
            <?php if($obj->id): ?>
                <span class="page-heading">EDIT Brand</span>
            <?php else: ?>
                <span class="page-heading">Create Brand</span>
            <?php endif; ?>
            <div >
                <div class="btn-group">
                    <a href="<?php echo e(route('admin.brand.home')); ?>"  class="btn btn-success"><i class="fa fa-list"></i> List
                    </a>
                    <?php if($obj->id): ?>
                        <a href="<?php echo e(route('admin.brand.create')); ?>" class="btn btn-success"><i class="fa fa-pencil"></i> Create new
                        </a>
                        <a href="<?php echo e(route($route.'.destroy', [encrypt($obj->id)])); ?>" class="btn btn-success btn-warning-popup" data-message="Are you sure to delete?  Associated data will be removed if it is deleted." data-redirect-url="<?php echo e(route($route.'.home')); ?>"><i class="fa fa-trash"></i> Delete</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="card card-borderless">
                <?php if($obj->id): ?>
                    <?php echo e(Form::open(['url' => route('admin.brand.update'), 'method' => 'post','enctype' => 'multipart/form-data'])); ?>

                    <input type="hidden" name="id" value="<?php echo e(encrypt($obj->id)); ?>">
                <?php else: ?>
                    <?php echo e(Form::open(['url' => route('admin.brand.save'), 'method' => 'post','enctype' => 'multipart/form-data'])); ?>

                <?php endif; ?>
                <?php echo csrf_field(); ?>
                <ul class="nav nav-tabs nav-tabs-simple d-none d-md-flex d-lg-flex d-xl-flex" role="tablist" data-init-reponsive-tabs="dropdownfx">
                    <li class="nav-item">
                        <a class="active show" data-toggle="tab" role="tab"
                           data-target="#tab1Basic"
                           href="#" aria-selected="true">Mandatory Details</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" data-toggle="tab" role="tab"
                           data-target="#tab3SEO"
                           class="" aria-selected="false">SEO</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" data-toggle="tab" role="tab"
                           data-target="#tab4Media"
                           class="" aria-selected="false">Media</a>
                    </li>

                </ul>
                <div class="nav-tab-dropdown cs-wrapper full-width d-lg-none d-xl-none d-md-none"><div class="cs-select cs-skin-slide full-width" tabindex="0"><span class="cs-placeholder">Hello World</span><div class="cs-options"><ul><li data-option="" data-value="#tab2hellowWorld"><span>Hello World</span></li><li data-option="" data-value="#tab2FollowUs"><span>Hello Two</span></li><li data-option="" data-value="#tab2Inspire"><span>Hello Three</span></li></ul></div><select class="cs-select cs-skin-slide full-width" data-init-plugin="cs-select"><option value="#tab2hellowWorld" selected="">Hello World</option><option value="#tab2FollowUs">Hello Two</option><option value="#tab2Inspire">Hello Three</option></select><div class="cs-backdrop"></div></div></div>
                <div class="tab-content">
                    <div class="tab-pane active show" id="tab1Basic">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default required">
                                        <label>Brand name</label>
                                        <?php echo e(Form::text("brand_name", $obj->brand_name, array('class'=>'form-control', 'id' => 'brand_name','required' => true))); ?>


                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default required">
                                        <label>Page heading</label>
                                        <?php echo e(Form::text("page_heading", $obj->page_heading, array('class'=>'form-control', 'id' => 'page_heading','required' => true))); ?>


                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default required">
                                        <label class="">Brand slug (for url)</label>
                                        <?php echo e(Form::text("slug", $obj->slug, array('class'=>'form-control', 'id' => 'slug'))); ?>


                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default required">
                                        <label class="">Website</label>
                                        <?php echo e(Form::text("website", $obj->website, array('class'=>'form-control', 'id' => 'website'))); ?>


                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="tab-pane" id="tab3SEO">
                        <div class="row">

                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default">
                                        <label>Meta title</label>
                                        <?php echo e(Form::text("browser_title", $obj->browser_title, array('class'=>'form-control', 'id' => 'browser_title'))); ?>


                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default">
                                        <label class="">Meta Keywords</label>
                                        <?php echo e(Form::text("meta_keywords", $obj->meta_keywords, array('class'=>'form-control', 'id' => 'meta_keywords'))); ?>


                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default">
                                        <label class="">Meta description</label>
                                        <?php echo e(Form::textarea("meta_description", $obj->meta_description, array('class'=>'form-control', 'id' => 'meta_description'))); ?>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab4Media">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <p class="col-md-12">Logo</p>
                                    <div class="default-image-holder padding-5">
                                        <a href="javascript:void(0);" class="image-remove" data-remove-id="mediaId0"><i class="fa  fa-times-circle"></i></a>
                                        <a href="<?php echo e(route('admin.media.popup', ['popup_type'=>'single_image', 'type'=>'Image', 'holder_attr'=>'0', 'related_id'=>$obj->id])); ?>" class="open-ajax-popup" title="Media Images" data-popup-size="large" id="image-holder-0">
                                          <?php if($obj->media_id && $obj->logo): ?>
                                            <img class="card-img-top padding-20" src="<?php echo e(asset('public/'.$obj->logo->thumb_file_path)); ?>">
                                          <?php else: ?>
                                            <img class="card-img-top padding-20" src="<?php echo e(asset('assets/img/add_image.png')); ?>">
                                          <?php endif; ?>
                                        </a>
                                        <input type="hidden" name="media_id" id="mediaId0" value="<?php echo e($obj->media_id); ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12" align="right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
                <?php echo e(Form::close()); ?>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom'); ?>
    <script>

        custom_summernote('top-description',"<?php echo e(route('admin.summernote.image')); ?>");
    </script>
    ##parent-placeholder-c03e9099aad17cb58e4fff1d93d751105735c9c2##
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.common.datatable', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/pittappillil/resources/views/admin/brand/form.blade.php ENDPATH**/ ?>