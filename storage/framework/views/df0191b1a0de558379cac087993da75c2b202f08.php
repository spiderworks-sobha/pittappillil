<?php $__env->startSection('content'); ?>

    <div class="page-header" style="margin-bottom: 20px;">
        <div class="page-header__container container-fluid" style="padding: 0px;">
            <div class="page-header__title" align="center" style="    background: linear-gradient(45deg, rgb(3, 169, 244), rgb(224, 9, 24)); padding: 30px 0px; color: white;}">
                <h1>Shop Big, Save Big</h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-12">
                <div class="block">
                    <?php if(count($offers)>0): ?>
                        <div class="posts-view">
                            <div class="posts-view__list posts-list posts-list--layout--classic">
                                <div class="posts-list__body">


                                    <?php $__currentLoopData = $offers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $offer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-md-4">
                                            <div class="posts-list__item"  style="width: 100%">
                                                <div class="post-card post-card--layout--list post-card--size--nl">
                                                    <a href="<?php echo e(url('offers/'.$offer->slug)); ?>" style="color: unset">
                                                        <div class="post-card__info" align="center" >
                                                            <div class="post-card__image" align="center" style="width: 100%"><img src="<?php if($offer->offer_image()): ?> <?php echo e(asset($offer->offer_image())); ?> <?php else: ?> <?php echo e(asset('images/default.png')); ?> <?php endif; ?>" alt=""></div>
                                                            <div class="post-card__name"><?php echo e($offer->offer_name); ?> </div>

                                                            <p style="text-align: center;width: 100%;border-bottom: 1px solid #e2e2e2;padding-bottom: 10px;">
                                                                <span style="color: green">Offer available from <?php echo e(\Carbon\Carbon::parse($offer->validity_start_date)->format('d F Y')); ?> to <?php echo e(\Carbon\Carbon::parse($offer->validity_end_datess)->format('d F Y')); ?></span>
                                                            </p>


                                                            <p style="text-align: left;padding: 10px;" >
                                                                <span>
                                                                <b>Terms and Conditions</b> <br>
                                                                <?php if($offer->min_purchase_amount): ?> Minimum purchase amount : <?php echo e($offer->min_purchase_amount); ?><br> <?php endif; ?>
                                                                <?php if($offer->max_discount_amount): ?> Maximum discount amount : <?php echo e($offer->max_discount_amount); ?> <?php endif; ?>
                                                                </span><br>
                                                                <div  class="timer" data-from="<?php echo e(\Carbon\Carbon::parse($offer->validity_start_date)->format('Ymdhis')); ?>" data-timer="<?php echo e(\Carbon\Carbon::parse($offer->validity_end_date)->format('Ymdhis')); ?>"></div>
                                                            </p>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="nothing-to-display"> Currently there are no offer available, Please visit us later for offers</div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('head'); ?>
    <style>
        .post-card__name{
            font-size: 20px !important;
            width: 100%
        }
        .offer-expired{
            margin-top: 10px;
            background: #ff2e31;
            color: white;
            padding: 10px;
            text-align: center;

        }

        .offer-expire{
            margin-top: 10px;
            background: green;
            color: white;
            padding: 10px;
            text-align: center;
            width: fit-content;
            border-radius: 5px;
        }

        .post-card__image img {
            max-width: 180px;
            max-height: 180px;
            min-height: 180px;
            object-fit: contain;
        }
        .timer{
            width: 100%;
        }

    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom'); ?>
    <script src="<?php echo e(asset('client/js/moment.js')); ?>"></script>
    <script>
        $('.timer').each(function () {
            let to = $(this).data('timer');
            let from = $(this).data('from');

            if(moment().isAfter(moment(to, "YYYYMMDDhhmmss"))){
                $(this).html('<div class="offer-expired" >Offer Expired : '+moment(to, "YYYYMMDDhhmmss").fromNow()+'</div>');

            } else{
                $(this).html('<div class="offer-expire">Expiry time : '+moment(to, "YYYYMMDDhhmmss").fromNow()+'</div>');
            }

        })


    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('client.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp\www\pittappillil\resources\views/client/pages/offer.blade.php ENDPATH**/ ?>