<?php $__env->startSection('head'); ?>
   <style>
      <?php echo $page->extra_css; ?>

   </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="block about-us">
   <div class="about-us__image"></div>
   <div class="container">
      <div class="row justify-content-center">
         <div class="col-12 col-xl-10">
            <div class="about-us__body">
               <h1 class="about-us__title"><?php echo e($page->primary_heading); ?></h1>
               <div class="about-us__text typography">
                  <?php echo $page->content; ?>

               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom'); ?>
##parent-placeholder-c03e9099aad17cb58e4fff1d93d751105735c9c2##

<?php $__env->stopSection(); ?>

<?php echo $__env->make('client.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/pittappillil/resources/views/pages/post_page.blade.php ENDPATH**/ ?>