<?php
	$selected_items = isset($selected_items)?$selected_items:[];
?>
<?php if(count($products)>0): ?>
                                    <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <li class="list-group-item <?php if(in_array($product->id, $selected_items)): ?> disabled <?php else: ?> <?php if(isset($all_selected) && $all_selected): ?> selected <?php endif; ?> selectable <?php endif; ?>" id="<?php echo e($product->id); ?>">
                                        <label><?php echo e($product->product_name); ?></label>
                                        <input type="hidden" name="product[]" value="<?php echo e($product->id); ?>">
                                      </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($products->hasMorePages()): ?>
                                    	<li id="load-more"><a href="javascript:void(0)">Load More</a></li>
                                    <?php endif; ?>
                                    <li id="pagination-ajax" style="display: none;">
                                    	<?php echo e($products->links()); ?>

                                    </li>
                                  <?php endif; ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/pittappillil/resources/views/admin/offers/ajax_list.blade.php ENDPATH**/ ?>