<?php $__env->startSection('head'); ?>
<style>
    #googleMap{
            width:100%;
            height:600px;
    }
    .store-item {
        border: 1px solid #ddd;
        margin-bottom: 10px;
    }
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="page-header">
        <div class="page-header__container container">
            <div class="page-header__breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.html">Home</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="<?php echo e(asset('client')); ?>/images/sprite.svg#arrow-rounded-right-6x9"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="">Breadcrumb</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="<?php echo e(asset('client')); ?>/images/sprite.svg#arrow-rounded-right-6x9"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Store Locator</li>
                    </ol>
                </nav>
            </div>
            <div class="page-header__title">
                <h1>Store Locator</h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div>Opps.. Something wrong happend! please try again...</div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom'); ?>
##parent-placeholder-c03e9099aad17cb58e4fff1d93d751105735c9c2##

<?php $__env->stopSection(); ?>

<?php echo $__env->make('client.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/pittappillil/resources/views/client/payments/errors.blade.php ENDPATH**/ ?>