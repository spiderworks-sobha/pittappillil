<?php $__env->startSection('content'); ?>
        <!-- .block-slideshow -->

        <div class="block-slideshow block-slideshow--layout--with-departments block">
            <div class="row m-0">
                <div class="col-12 col-lg-12 p-0">
                    <home-slider></home-slider>
                </div>
            </div>
        </div>

        <grid-4></grid-4>
        <!-- .block-slideshow / end -->
        <!-- .block-features -->
       <div class="mob-dis-none"> <website-features></website-features></div>
        <!-- .block-features / end -->

        <div class="block block-categories block-categories--layout--classic">
                <div class="container">
                   
                    <div class="block-categories__list">

                        <div class="row">

                           <div class="col-md-4">
                                <home-cubed-banner type="home-banner-cubed-1"></home-cubed-banner>                     
                           </div>

                           <div class="col-md-4">
                                <home-cubed-banner type="home-banner-cubed-2"></home-cubed-banner>
                           </div>


                           <div class="col-md-4">
                                <home-cubed-banner type="home-banner-cubed-3"></home-cubed-banner>
                           </div>

                        </div>
                    </div>
                </div>
            </div>

        <home-discount-banner></home-discount-banner>

        <div class="container">
        <!-- .block-products-carousel -->
        <productfloor api="is_featured_in_home_page" title="Featured products"></productfloor>
        <!-- .block-products-carousel / end -->
        </div>

        <home-deals-banner></home-deals-banner>
        
        <!-- .block-banner -->
        <home-banner-full-width></home-banner-full-width>
        <!-- .block-banner / end -->

        <!-- .block-products -->
        <div class="container"><productfloor api="is_top_seller" title="Today's Deals"></productfloor></div>
        <!-- .block-products / end -->

        <grid-3></grid-3>


        <div class="block block--highlighted block-categories block-categories--layout--classic">
                <div class="container">
                    <div class="block-header">
                        <h3 class="block-header__title">Popular Categories</h3>
                        <div class="block-header__divider"></div>
                    </div>
                    <home-popular-categories></home-popular-categories>
                </div>
        </div>



        <div class="block block-posts block-posts--layout--list-sm" data-layout="list-sm">
            <div class="container">
                <div class="block-header">
                    <h3 class="block-header__title">Latest News</h3>
                    <div class="block-header__divider"></div>
                    <div class="block-header__arrows-list">
                        <button class="block-header__arrow block-header__arrow--left" type="button">
                            <svg width="7px" height="11px">
                                <use xlink:href="<?php echo e(URL::asset('client')); ?>/images/sprite.svg#arrow-rounded-left-7x11"></use>
                            </svg>
                        </button>
                        <button class="block-header__arrow block-header__arrow--right" type="button">
                            <svg width="7px" height="11px">
                                <use xlink:href="<?php echo e(URL::asset('client')); ?>/images/sprite.svg#arrow-rounded-right-7x11"></use>
                            </svg>
                        </button>
                    </div>
                </div>
                <news-slider></news-slider>
            </div>
        </div>

        <!-- .block-product-columns / end -->
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('client.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/pittappillil/resources/views/client/pages/home.blade.php ENDPATH**/ ?>