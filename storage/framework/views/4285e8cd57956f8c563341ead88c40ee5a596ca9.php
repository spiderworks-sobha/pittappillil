<?php
  $trackings = $obj->tracking_history->pluck('order_status_labels_master_id')->toArray();
?>
<div class="settings-item w-100 confirm-wrap">
  <div class="row">
    <div class="col-md-6">
      <h6><?php echo e($obj->product_variants->name); ?></h6>
      <p>Quantity: <?php echo e($obj->quantity); ?></p>
      <p>MRP: <?php echo e($obj->mrp); ?></p>
      <p>Sale Price: <?php echo e($obj->sale_price); ?></p>
      <p>Discount: <?php echo e($obj->discount); ?></p>
    </div>
    <div class="col-md-6">
      <h6><?php echo e($obj->order->delivery_address->full_name); ?></h6>
      <p>
          <?php echo e($obj->order->delivery_address->address1); ?>, 
          <?php if($obj->order->delivery_address->address2): ?>
              <?php echo e($obj->order->delivery_address->address2); ?>, 
          <?php endif; ?>
          <?php if($obj->order->delivery_address->landmark): ?>
              <?php echo e($obj->order->delivery_address->landmark); ?>, 
          <?php endif; ?>
          <?php echo e($obj->order->delivery_address->city); ?>, 
          <?php echo e($obj->order->delivery_address->state_details->name); ?>,
          <?php echo e($obj->order->delivery_address->pincode); ?>

      </p>
      <p>Phone Number <?php echo e($obj->order->delivery_address->phone); ?></p>
      <?php if($obj->order->delivery_instructions): ?>
          <p>Delivery Instructions: <?php echo e($obj->order->delivery_instructions); ?></p>
      <?php endif; ?>
      <p>Address Type: <?php if($obj->order->delivery_address->type == 1): ?> Home <?php else: ?> Work <?php endif; ?></p>
    </div>
  </div>
  <div class="row w-100">
    <ol class="progtrckr w-100" data-progtrckr-steps="<?php echo e(count($tracking_statuses)); ?>">
      <?php $__currentLoopData = $tracking_statuses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php
          $status = 'progtrckr-todo';
          if(in_array($item->id, $trackings))
            $status = 'progtrckr-done';
        ?>
        <li class="<?php echo e($status); ?>"><?php echo e($item->name); ?></li>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ol>
  </div>
  <div class="row">
    <div class="col-md-12">
     <table class="table table-hover demo-table-search table-responsive-block">
        <thead>
          <th>Status</th>
          <th>Note</th>
          <th>Updated By</th>
          <th>Updated On</th>
        </thead>
        <tbody>
          <?php $__currentLoopData = $obj->tracking_history; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <tr>
            <td><?php echo e($item->status->name); ?></td>
            <td><?php echo e($item->notes); ?></td>
            <td><?php echo e($item->user->username); ?></td>
            <td><?php echo e(date('d M, Y h:i A', strtotime($item->updated_at))); ?></td>
          </tr>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
     </table>
    </div>
  </div>
  <?php if($obj->order->delivery_instructions): ?>
  <div class="row">
    <div class="col-md-12">
      <p class="mt-2"><b>Delivery Instructions: </b> <span class="text-danger"><?php echo e($obj->order->delivery_instructions); ?></span></p>
    </div>
  </div>
  <?php endif; ?>
  <div class="row">
    <div class="card card-borderless">
      <div class="card-body" id="update-order-status" <?php if($order_processing_type != 'N'): ?> style="display:none" <?php endif; ?>>
          <h5 class="card-title">Update Status</h5>
          <?php echo e(Form::open(['url' => route('admin.orders.change-status'), 'method' => 'post','enctype' => 'multipart/form-data', 'id'=>'orderStatudFrm', 'class'=>'col-md-12'])); ?>

          <input type="hidden" name="id" value="<?php echo e($obj->id); ?>">
          <div class="col-md-12 first-form-row">
                            <div class="row column-seperation padding-5">
                                <div class="form-group w-100" id="status-parent">
                                    <label>Status</label>
                                    <?php echo Form::select('status', $tracking_statuses->pluck('name', 'id')->toArray(), $obj->status, array('class'=>'full-width select2_input', 'id'=>'inputStatus', 'data-parent'=>'#status-parent'));; ?>

                                </div>
                            </div>
                        </div>
          <div class="col-md-12 first-form-row">
                            <div class="row column-seperation padding-5">
                                <div class="form-group w-100">
                                    <label>Note</label>
                                    <?php echo e(Form::textarea("note", $obj->group_name, array('class'=>'form-control', 'id' => 'name', 'rows'=>'3'))); ?>

                                </div>
                            </div>
                        </div>
          <div class="col-md-12 padding-10" align="right">
                        <a href="javascript:void(0)" id="cancel-link">Cancel this order</a>
                          <button type="button" class="btn btn-primary" id="order-status-update-btn">Update</button>
                      </div>
        <?php echo e(Form::close()); ?>

        </div>
        <div class="card-body" id="cancel-order" <?php if($order_processing_type != 'C'): ?> style="display:none" <?php endif; ?>>
          <h5 class="card-title">Cancel Order</h5>
          <?php echo e(Form::open(['url' => route('admin.orders.change-status'), 'method' => 'post','enctype' => 'multipart/form-data', 'id'=>'cancelOrderFrm', 'class'=>'col-md-12'])); ?>

          <input type="hidden" name="id" value="<?php echo e($obj->id); ?>">
          <input type="hidden" name="is_cancel" value="1">
              <div class="col-md-12 first-form-row">
                            <div class="row column-seperation padding-5">
                                <div class="form-group w-100">
                                    <label>Note</label>
                                    <?php echo e(Form::textarea("note", null, array('class'=>'form-control', 'id' => 'name', 'rows'=>'3'))); ?>

                                </div>
                            </div>
                        </div>
              <div class="col-md-12 padding-10" align="right">
                        <?php if($order_processing_type == 'N'): ?>
                        <a href="javascript:void(0)" id="change-status-link">Change order status</a>
                        <?php endif; ?>
                          <button type="button" class="btn btn-primary" id="cancel-order-btn">Cancel Order</button>
                      </div>
        <?php echo e(Form::close()); ?>

        </div>
    </div>
  </div>
</div><?php /**PATH C:\wamp\www\pittappillil\resources\views/admin/orders/form.blade.php ENDPATH**/ ?>