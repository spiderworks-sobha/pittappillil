<?php $__env->startSection('content'); ?>

    <div class="page-header">
        <div class="page-header__container container">
            <div class="page-header__title"><br>

            </div>
        </div>
    </div>
    <div class="block">
        <div class="container">
            <div class="row">
                <div class="col-12 offset-lg-3 col-lg-6 col-xl-6 mt-4 mt-lg-0">
                    <div class="card mb-0">
                        <div class="card-body">
                            <h3 class="card-title">Your Order</h3>
                            <table class="checkout__totals">
                                <thead class="checkout__totals-header">
                                <tr>
                                    <th>Product</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody class="checkout__totals-products"><?php $total_amount = 0; ?>
                                <?php $__currentLoopData = $cart; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $o): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($o->product->name); ?> × <?php echo e($o->quantity); ?></td>
                                    <td><?php if($o->price == 0): ?> FREE <?php else: ?> <?php echo e('₹ '.number_format($o->price * $o->quantity)); ?> <?php endif; ?></td>
                                </tr>  <?php $total_amount = $total_amount + $o->price * $o->quantity; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                                <tbody class="checkout__totals-subtotals">
                                <tr>
                                    <th>Subtotal</th>
                                    <td>₹<?php echo e(number_format($total_amount)); ?></td>
                                </tr>
                                <tr>
                                    <th>Shipping</th>
                                    <td>₹0.00</td>
                                </tr>
                                <?php if($total['coupon_discount']): ?>
                                    <tr>
                                        <th>Coupon discount</th>
                                        <td>- ₹<?php echo e($total['coupon_discount']); ?></td>
                                    </tr>
                                <?php endif; ?>
                                </tbody>
                                <tfoot class="checkout__totals-footer">
                                <tr>
                                    <th>Total</th>
                                    <td>₹<?php echo e(number_format($total['final_total'])); ?></td>
                                </tr>
                                </tfoot>
                            </table>
                            <form action="<?php echo e(url('complete')); ?>" method="post">
                                <?php echo e(csrf_field()); ?>

                                <div class="payment-methods">
                                    <ul class="payment-methods__list">

                                        <li class="payment-methods__item active">
                                            <div class="payment-methods__item-header row">
                                                <div class="col-md-6">
                                                    <span class="payment-methods__item-radio input-radio"><span class="input-radio__body"><input class="input-radio__input" name="checkout_payment_method" type="radio" value="cash"> <span class="input-radio__circle"></span> </span></span><span class="payment-methods__item-title">Cash on delivery</span>
                                                </div>
                                                <div class="col-md-6">
                                                    <span class="payment-methods__item-radio input-radio"><span class="input-radio__body"><input class="input-radio__input" name="checkout_payment_method" type="radio" checked value="online"> <span class="input-radio__circle"></span> </span></span><span class="payment-methods__item-title">Pay online</span>
                                                </div>
                                            </div>
                                            <div class="payment-methods__item-container">
                                                <div class="payment-methods__item-description text-muted">Pay with cash upon delivery.</div>
                                            </div>
                                        </li>

                                    </ul>
                                </div>
                                <input type="hidden"name="amount" value="<?php echo e(encrypt($total)); ?>">
                                <button type="submit" class="btn btn-primary btn-xl btn-block">Place Order</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('bottom'); ?>
    <script>


    </script>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('head'); ?>
    <style>
        .modal-content{
            width: 100% !important;
        }
        @media (min-width: 1200px) {
            .addresses-list__item {
                max-width: unset;
                flex-basis: unset;
            }
        }
    </style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('client.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/pittappillil/resources/views/client/checkout/summary.blade.php ENDPATH**/ ?>