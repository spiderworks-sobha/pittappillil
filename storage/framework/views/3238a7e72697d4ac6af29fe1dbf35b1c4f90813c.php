<!DOCTYPE html>
<html>
<head>
    <title>PDF Create</title>
    <style type="text/css">
        th, td {
            border-right: solid 1px #777;
            text-align: center;
            font-size: 12px;
            padding: 5px;
        }
        thead,tbody{

            border-top: solid 1px #777;
        }
        tbody{
            border-bottom: solid 1px #777;
        }
        td:nth-child(9), th:nth-child(9)  {
            border-right:none;
        }
        td:nth-child(0), th:nth-child(0)  {
            border-left:none;
        }

        p{
            font-size: 12px;
        }
    </style>
</head>
<body style="border: 1px solid black">

<div style="width: 100%;display: inline-block;">

    <div style="float: left">
    <img src="<?php echo e(base_path()); ?>/public/logo.jpg" style="width: 120px;margin: 5px;"><br>
    <p style="margin: 5px;"><b>Branch : Thripunithiura</b> <br>
    Opp: Hill Palace Police Station <br>
    Vaikom Road <br>
    Phone : 0484 2785199 | Service -2782199 | Delvery - 8606966821 <br><br>
        <b>GSTIN : 32AADFP6144R1ZV</b><br>
    </p>
    </div>

    <div style="float: right">
        <h1 style="margin-right: 10px">TAX INVOICE</h1>
        <p style="margin: 5px;">
            Invoice No : <?php echo e($order->order_reference_number); ?><br>
            Invoice Date : <?php echo e(\Carbon\Carbon::parse($order->created_at)->format('d-F-Y')); ?>

        </p>
    </div>

</div>
<br><br><br><br><br><br><br><br><br><br><br><br>
<div style="width: 100%;display: inline-block;">

    <div style="float: left;width: 50%" >
        <p style="margin: 5px;padding:5px 0px;background: grey;color:#FFF;width:100%;text-align: center">
            <b>Billing Address</b>
        </p>
        <p style="margin: 5px 20px;">
            <b>Customer Code : <?php echo e($order->delivery_address->id); ?><br><?php echo e($order->delivery_address->full_name); ?></b>
            <br><?php echo e($order->delivery_address->address1); ?>

            <br><?php echo e($order->delivery_address->address2); ?>

            <br>Pin : <?php echo e($order->delivery_address->pincode); ?>

            <br>City : <?php echo e($order->delivery_address->city); ?>

        </p>
    </div>

    <div style="float: right;width: 50%">
        <p style="margin: 5px;padding:5px 0px;background: grey;color:#FFF;width:100%;text-align: center">
            <b>Shipping Address</b>
        <p style="margin: 5px 20px;">
            <b>Customer Code : <?php echo e($order->delivery_address->id); ?><br><?php echo e($order->delivery_address->full_name); ?></b>
            <br><?php echo e($order->delivery_address->address1); ?>

            <br><?php echo e($order->delivery_address->address2); ?>

            <br>Pin : <?php echo e($order->delivery_address->pincode); ?>

            <br>City : <?php echo e($order->delivery_address->city); ?>

        </p>
    </div>

</div>
<br><br><br><br><br><br><br>
<div style="width: 100%;display: inline-block;">
    <table width="100%" style=" border-spacing: 0px;">
        <thead style="background: #e4e4e4;">
            <td>S No</td>
            <td>Description of Goods</td>
            <td>Qty</td>
            <td>Unit Rate</td>
            <td>Taxable value</td>
            <td>CGST</td>
            <td>SGST</td>
            <td>IGST</td>
            <td>Total</td>
        </thead>
        <tbody> <?php $i=1;  ?>
        <?php $__currentLoopData = $order->details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $obj): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php $count = count($order->details); if($count == $i){ $hgt = 400 - (50*$count); if($hgt<0){$hgt=0;}    $pad=$hgt.'px';}else{$pad='10px';} ?>
            <tr>
                <td style="padding-bottom:<?php echo e($pad); ?>;"><?php echo e($i++); ?></td>
                <td style="padding-bottom:<?php echo e($pad); ?>;"><?php echo e($obj->product_variants->name); ?></td>
                <td style="padding-bottom:<?php echo e($pad); ?>;"><?php echo e($obj->quantity); ?></td>
                <td style="padding-bottom:<?php echo e($pad); ?>;"><?php echo e(number_format($obj->sale_price,2)); ?></td>
                <td style="padding-bottom:<?php echo e($pad); ?>;"><?php echo e(number_format(($obj->sale_price*100)/(100+18),2)); ?></td>
                <td style="padding-bottom:<?php echo e($pad); ?>;"><?php echo e(number_format((($obj->sale_price)-(($obj->sale_price*100)/(100+18)))/2,2)); ?></td>
                <td style="padding-bottom:<?php echo e($pad); ?>;"><?php echo e(number_format((($obj->sale_price)-(($obj->sale_price*100)/(100+18)))/2,2)); ?></td>
                <td style="padding-bottom:<?php echo e($pad); ?>;"><?php echo e(number_format(0,2)); ?></td>
                <td style="padding-bottom:<?php echo e($pad); ?>;"><?php echo e(number_format($obj->quantity*$obj->sale_price,2)); ?></td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
</div>
<br>
<div style="width: 100%;display: inline-block;">
<p style="margin: 10px;text-transform: capitalize;float: left;font-weight: bold;"><?php echo e(strtolower('In Words : '.$ordertotalinwords)); ?></p>
<p style="margin: 10px;float: right;font-size: 14px;font-weight: bold;"><?php echo e('Total Invoice Value : '.number_format($order->total_sale_price,2)); ?></p>
</div>
<br>
<br>
<div style="width: 100%;display: inline-block;">
    <p style="margin: 10px;"><b>Payment Method : </b><?php echo e($order->payment_method); ?></p>

</div>
<br>
<br>
<div style="width: 100%;display: inline-block;border-bottom: 1px solid black;border-top: 1px solid black">
    <p style="margin: 10px;"><b>DECLARATION : </b>Certified that all the particulars shown in the above Tax Invoice are true and that my/our registration under GST is valid
        as on the data of the bill</p>

</div>
<br>
<div style="width: 100%;display: inline-block;">

    <div style="width: 33.33%;float: left;">
        <p style="margin: 10px;"><b>E. & O. E. : </b>
            <br><br><br><br>
            Subject to perumbavur Jurisdiction</p>
    </div>

    <div style="width: 33.33%;float: left">
        <p style="margin: 10px;"><b>Received Goods in good condition : </b>
            <br><br><br><br>Signature & Seal</p>
    </div>

    <div style="width: 33.33%;;float: left">
        <p style="margin: 10px;text-align: right"><b>FOR PITTAPPILLIL AGENCIES : </b>
            <br><br><br><br>Authorised Signatory</p>
    </div>


</div>

</body>
<?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/pittappillil/resources/views/client/pdf/invoice.blade.php ENDPATH**/ ?>