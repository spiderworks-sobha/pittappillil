<?php $__env->startSection('content'); ?>
    <div class="site__body">
        <div class="page-header">
            <div class="page-header__container container">
                <div class="page-header__breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="<?php echo e(URL::to('/')); ?>">Home</a>
                                <svg class="breadcrumb-arrow" width="6px" height="9px">
                                    <use xlink:href="<?php echo e(asset('images')); ?>/sprite.svg#arrow-rounded-right-6x9"></use>
                                </svg>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Cart</li>
                        </ol>
                    </nav>
                </div>
                <div class="page-header__title">
                    <h1>Shopping Cart</h1>
                </div>
            </div>
        </div>

        <div class="cart block">
            <?php if(count($obj)>0): ?>
                <div class="container">
                <!-- table starts here -->
                <div class="cart__section">
                    <div class="cart-table__head mob_view">
                        <div class="row justify-content-center align-self-center card_section__row">
                            <div class="col-md-2 justify-content-center align-self-center">
                                <span class="cart__section__column cart__section--image">Image</span>
                            </div>
                            <div class="col-md-3 justify-content-center align-self-center">
                                <span class="cart__section__column cart__section__column--product">Product</span>
                            </div>
                            <div class="col-md-2 justify-content-center align-self-center">
                                <span class="cart__section__column cart__section__column--price">Price</span>
                            </div>
                            <div class="col-md-2 justify-content-center align-self-center" align="center">
                                <span class="cart__section__column cart__section__column--quantity">Quantity</span>
                            </div>
                            <div class="col-md-2 justify-content-center align-self-center">
                                <span class="cart__section__column cart__section__column--total">Total</span>
                            </div>
                            <div class="col-md-1 justify-content-center top_view">
                            <span class="cart__section__column cart__section__column--remove">
                              <svg width="12px" height="12px">
                                 <use xlink:href="<?php echo e(asset('images')); ?>/sprite.svg#cross-12"></use>
                             </svg>
                            </span>
                            </div>
                        </div>
                    </div>
                    <div class="cart-section__body">
                        <?php $total = 0; ?>
                        <?php $__currentLoopData = $obj; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $o): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="row align-self-center <?php if($o->offer_parent == null): ?> card_section__row <?php else: ?> sub_pro_row <?php endif; ?>">
                            <div class="col-md-2 col-sm-4 col-4 align-self-center" align="center">
                                <div class="<?php if($o->offer_parent): ?> sub_image_cart <?php endif; ?> cart__section__column cart__section__column--image">
                                    <?php if($o->offer_parent): ?><i class="fas fa-angle-double-right"></i><?php endif; ?>
                                    <?php if(isset($o->product->media)): ?>
                                        <a href="<?php echo e(url($o->product->slug)); ?>"><img src="<?php echo e(asset($o->product->media->file_path)); ?>" alt="" style="<?php if(!$o->offer_parent): ?>max-height: 80px;max-width: 80px;<?php else: ?> max-height: 50px;max-width: 50px; <?php endif; ?> width: unset;    "></a>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-8 col-8 justify-content-center align-self-center">
                                <div class="cart__section__column cart__section__column--product">
                                    <a href="<?php echo e(url($o->product->slug)); ?>" class="cart__section__product-name"><?php echo e($o->product->name); ?></a>
                                    <ul class="cart__section__options">
                                        <li>MRP : ₹<?php echo e(number_format($o->product->inventory->retail_price)); ?></li>
                                        <?php if($o->offer): ?><li style="color:green;">Offer applied : <?php echo e($o->offer->offer_name); ?></li><?php endif; ?>
                                    </ul>

                                    <?php if(null !== $o->product->extended_warranty() && null == $o->warranty_parent): ?>
                                        <div class="extended-warranty-box">
                                            <?php if(count($o->product->extended_warranty()) > 0): ?>
                                                <b><u>Extended warranty options</u></b><br>
                                                <?php $__currentLoopData = $o->product->extended_warranty(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ew): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <button type="button" class="btn btn-secondary btn-sm ex-btn" data-id="<?php echo e($ew->id); ?>" data-cart="<?php echo e($o->id); ?>" >
                                                        <?php echo e($ew->title); ?> @ <?php echo e($ew->warranty_price); ?>

                                                    </button>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </div>
                                    <?php endif; ?>

                                </div>

                            </div>
                            <div class="col-md-2  col-sm-4 col-4 justify-content-center align-self-center">
                                <div class="cart__section__column cart__section__column--price" data-title="Price"><?php if($o->price == 0): ?> FREE <?php else: ?> ₹<?php echo e(number_format($o->price)); ?> <?php endif; ?></div>
                            </div>
                            <div class="col-md-2  col-sm-4 col-4 justify-content-center align-self-center" align="center">
                                <div class="cart__section__column--quantity" data-title="Quantity">
                                    <div class="input-number">
                                        <?php if($o->offer_parent): ?>
                                            <?php echo e($o->quantity); ?>

                                        <?php else: ?>
                                            <input class="form-control input-number__input" type="number" min="1" value="<?php echo e($o->quantity); ?>" readonly>
                                            <div class="input-number__add" data-cart="<?php echo e(encrypt($o->id)); ?>"></div>
                                            <div class="input-number__sub" data-cart="<?php echo e(encrypt($o->id)); ?>"></div>
                                        <?php endif; ?>


                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2  col-sm-4 col-4 justify-content-center align-self-center"><?php $total += ($o->price*$o->quantity); ?>
                                <div class="cart__section__column cart__section__column--total" data-title="Total"><?php if($o->price == 0): ?> FREE <?php else: ?> ₹<?php echo e(number_format($o->price*$o->quantity)); ?> <?php endif; ?> </div>
                            </div>
                            <div class="col-md-1 justify-content-center top_view align-self-center">
                               <?php if(!$o->offer_parent): ?>
                                    <span class="cart__section__column cart__section__column--remove remove-cart"  style="cursor: pointer"  data-remove="<?php echo e(encrypt($o->id)); ?>">
                                         <svg width="12px" height="12px">
                                            <use xlink:href="<?php echo e(asset('client/images')); ?>/sprite.svg#cross-12"></use>
                                         </svg>
                                    </span>
                               <?php endif; ?>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </div>
                </div>
                <div class="cart__actions">
                    <form class="cart__coupon-form">
                        <label for="input-coupon-code" class="sr-only">Password</label>
                        <input type="text" class="form-control" id="input-coupon-code" placeholder="Coupon Code" value="<?php echo e($coupon); ?>">
                        <button type="button" class="btn btn-primary apply-coupon">Apply Coupon</button>

                    </form>

                </div> <?php if($total_from_model['coupon_message']): ?> <?php echo $total_from_model['coupon_message']; ?> <?php endif; ?>
                <div class="row justify-content-end pt-5">
                    <div class="col-12 col-md-7 col-lg-6 col-xl-5">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="card-title">Cart Totals</h3>
                                <table class="cart__totals">
                                    <thead class="cart__totals-header">
                                    <tr>
                                        <th>Subtotal</th>
                                        <td>₹<?php echo e($total_from_model['total']); ?></td>
                                    </tr>
                                    </thead>
                                    <tbody class="cart__totals-body">
                                    <tr>
                                        <th>Shipping</th>
                                        <td>
                                            FREE
                                        </td>
                                    </tr>
                                    <?php if($total_from_model['coupon_discount']): ?>
                                        <tr>
                                            <th>Discount</th>
                                            <td>
                                                - <?php echo e($total_from_model['coupon_discount']); ?>

                                            </td>
                                        </tr>
                                    <?php endif; ?>
                                    </tbody>
                                    <tfoot class="cart__totals-footer">
                                    <tr>
                                        <th>Total</th>
                                        <td>₹<?php echo e($total_from_model['final_total']); ?></td>
                                    </tr>
                                    </tfoot>
                                </table>
                                <?php if(auth()->guard()->check()): ?>
                                    <button type="button" class="btn btn-primary btn-xl btn-block cart__checkout-button" onclick="checkout()">Proceed to checkout</button>
                                    <?php else: ?>
                                    <button type="button" class="btn btn-primary btn-xl btn-block cart__checkout-button" onclick="checkout()">Please login to checkout</button>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php else: ?>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            Currently your shopping cart is empty
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>

    </div>
    <?php $__env->stopSection(); ?>


<?php $__env->startSection('bottom'); ?>
    <script>

        function cart_update(qty,cart){
            if(qty){
                $.post(baseUrl + '/cart/update/', { cart: cart, _token: csrf,quantity:qty,type:'update' }).done(function (data) {
                    console.log(data)
                    window.location.reload();
                })
            }
        }

        function remove(cart){
            if(cart){
                $.post(baseUrl + '/cart/update/', { cart: cart, _token: csrf,type:'remove' }).done(function (data) {
                    console.log(data)
                    window.location.reload();
                })
            }
        }

        function checkout() {
            $.get(baseUrl+'/get/user').done(function (data) {
                data = JSON.parse(data);
                if(data.status){
                    window.location.replace(baseUrl+'/checkout/address')
                }else{
                    $('#login-register').modal('show');
                }
            })
        }



        $(document).on('click', '.remove_coupon', function(e){
            let dom =  $(this);
            $.post(baseUrl+'/remove_coupon',{_token:csrf,id:dom.data('id')}).done(function (data) {
                let obj = JSON.parse(data);
                if(obj.status == true){
                    window.location.reload()
                }else{
                    swal('Oops!!','Please try again later')
                }
            })
        });

        $(document).on('click', '.apply-coupon', function(e){
            let dom =  $(this);
            const coupon = $('#input-coupon-code').val();
            $.post(baseUrl+'/coupon',{_token:csrf,coupon:coupon}).done(function (data) {
                let obj = JSON.parse(data);
                if(obj.status == true){
                    window.location.reload()
                }else{
                    swal('Oops!!','Please try again later')
                }
            })
        });


        $(document).on('click', '.ex-btn', function(e){

                let dom =  $(this);
                $.post(baseUrl+'/add-extended-warranty',{_token:csrf,id:dom.data('id'),cart:dom.data('cart')}).done(function (data) {
                    let obj = JSON.parse(data);
                    if(obj.status == true){
                        window.location.reload()
                    }else{
                        swal('Oops!!','Please try again later')
                    }
                })
        });


        $(document).on('click', '.remove-cart', function(e){

                let dom =  $(this);
                $.post(baseUrl + '/cart/update/', { cart: dom.data('remove'), _token: csrf,type:'remove' }).done(function (data) {
                    console.log(data)
                    window.location.reload();
                })
        });


        $(document).on('click', '.input-number__add', function(e){
            let dom =  $(this);
            cart_update(1,dom.data('cart'))
        });

        $(document).on('click', '.input-number__sub', function(e){
            let dom =  $(this);
            cart_update(-1,dom.data('cart'))
        });



    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('head'); ?>
    <style>
        .modal-content{
            width: 100% !important;
        }
        .input-number__input {
            -moz-appearance: textfield;
            display: block;
            width: 100%;
            min-width: 130px;
            padding: 5px;
            text-align: center !important;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('client.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/pittappillil/resources/views/client/pages/cart.blade.php ENDPATH**/ ?>