<?php $__env->startSection('content'); ?>

    <div class="page-header" style="margin-bottom: 20px;">
        <div class="page-header__container container-fluid" style="padding: 0px;">
            <div class="page-header__title" align="center" style="background: linear-gradient(45deg, #03A9F4, #FFC107);
    padding: 30px 0px;
    color: black;
}">
                <h1><?php echo e($offer->offer_name); ?></h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-12">
                <div class="block">
                    <div class="products-view__list products-list" data-layout="grid-4-full" data-with-features="false">
                    <?php if(count($products)>0): ?>
                        <div class="products-list__body">
                            <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $obj): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="products-list__item">
                                <div class="product-card m-0">
                                    <div class="product-card__image">
                                        <?php if(isset($obj->product_details->media)): ?>
                                        <a href="<?php echo e(url($obj->product_details->slug)); ?>"><img src="<?php echo e(asset($obj->product_details->media->file_path)); ?>" alt=""></a>
                                            <?php else: ?>
                                        <a href="#"><img src="<?php echo e(asset('images/default.png')); ?>" alt=""></a>
                                        <?php endif; ?>
                                    </div>
                                    <div class="product-card__info">
                                        <div class="product-card__name"><a href="<?php echo e(url($obj->product_details->slug)); ?>"><?php echo e($obj->product_details->name); ?></a></div>
                                    </div>
                                    <div class="product-card__actions">
                                        <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                        <div class="product-card__prices"><?php echo e('₹ '.number_format($obj->product_details->inventory->sale_price)); ?></div>

                                    </div>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <div class="col-md-12 d-flex justify-content-center pt-4" align="center">
                            <?php echo e($products->links()); ?>

                        </div>
                    <?php else: ?>
                        <div class="nothing-to-display"> Currently there are no offer available, Please visit us later for offers</div>
                    <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('head'); ?>
    <style>
        .post-card__name{
            font-size: 20px !important;
            width: 100%
        }
        .offer-expired{
            margin-top: 10px;
            background: #ff2e31;
            color: white;
            padding: 10px;
            text-align: center;

        }

        .offer-expire{
            margin-top: 10px;
            background: green;
            color: white;
            padding: 10px;
            text-align: center;
        }

        .post-card__image img {
            max-width: 180px;
            max-height: 180px;
            min-height: 180px;
            object-fit: contain;
        }
        .timer{
            width: 100%;
        }

    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom'); ?>
    <script src="<?php echo e(asset('client/js/moment.js')); ?>"></script>
    <script>
        $('.timer').each(function () {
            let to = $(this).data('timer');
            let from = $(this).data('from');

            if(moment().isAfter(moment(to, "YYYYMMDDhhmmss"))){
                $(this).html('<div class="offer-expired">Offer Expired : '+moment(to, "YYYYMMDDhhmmss").fromNow()+'</div>');

            } else{
                $(this).html('<div class="offer-expire">Expiry time : '+moment(to, "YYYYMMDDhhmmss").fromNow()+'</div>');
            }

        })
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('client.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/pittappillil/resources/views/client/pages/offer_details.blade.php ENDPATH**/ ?>