<?php $__env->startSection('content'); ?>
    <div class="site__body">
        <div class="page-header">
            <div class="page-header__container container">
                <div class="page-header__breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="<?php echo e(URL::to('/')); ?>">Home</a>
                                <svg class="breadcrumb-arrow" width="6px" height="9px">
                                    <use xlink:href="images/sprite.svg#arrow-rounded-right-6x9"></use>
                                </svg>
                            </li>
                            <?php if($product->category): ?>
                            <li class="breadcrumb-item">
                                <a href="<?php echo e(URL::to('stores/'.$product->category_slug)); ?>"><?php echo e($product->category); ?></a>
                                <svg class="breadcrumb-arrow" width="6px" height="9px">
                                    <use xlink:href="images/sprite.svg#arrow-rounded-right-6x9"></use>
                                </svg>
                            </li>
                            <?php endif; ?>
                            <li class="breadcrumb-item active" aria-current="page"><?php echo e($product->product_name); ?></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="block">
            <div class="container">
                <div class="product product--layout--columnar" data-layout="columnar">
                    <productsingle product_id="<?php echo e($product->pid); ?>" variant_id="<?php echo e($product->id); ?>" <?php if($product->rating): ?> rating="<?php echo e($product->rating); ?>" <?php endif; ?>  <?php if($product->rating_count): ?> rating_count="<?php echo e($product->rating_count); ?>" <?php endif; ?>  <?php if(session('pincode')): ?> pincode_="<?php echo e(session('pincode')); ?>" <?php endif; ?>></productsingle>
                </div>
                <div class="product-tabs ">
                    <div class="product-tabs__list">
                        <?php if($product->top_description || $product->bottom_description): ?><a href="#tab-description" class="product-tabs__item product-tabs__item--active">Description</a> <?php endif; ?>
                        <a href="#tab-specification" class="product-tabs__item">Specification</a>
                        <?php if($reviews): ?><a href="#tab-reviews" class="product-tabs__item">Reviews</a><?php endif; ?>
                    </div>
                    <div class="product-tabs__content">
                        <div class="product-tabs__pane product-tabs__pane--active" id="tab-description">
                            <div class="typography">
                                <?php if($product->top_description || $product->bottom_description): ?>
                                <h3>Product Description</h3>
                                    <?php echo $product->top_description; ?>

                                    <?php echo $product->top_description; ?>

                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="product-tabs__pane" id="tab-specification">
                            <div class="spec">
                                <h3 class="spec__header">Specification</h3>
                                <specifications id="<?php echo e($product->id); ?>"></specifications>
                            </div>
                        </div>
                        <div class="product-tabs__pane" id="tab-reviews">
                            <div class="reviews-view">
                                <div class="reviews-view__list">
                                    <?php if($reviews): ?>
                                    <h3 class="reviews-view__header">Customer Reviews</h3>
                                    <div class="reviews-list">
                                        <reviews id="<?php echo e($reviews->products_id); ?>"></reviews>
                                    </div>
                                    <?php endif; ?>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- .block-products-carousel -->
        <related-products id="<?php echo e($product->id); ?>"></related-products>
        <!-- .block-products-carousel / end -->




    </div>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('client.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/pittappillil/resources/views/client/pages/single.blade.php ENDPATH**/ ?>