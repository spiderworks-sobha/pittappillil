<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Cancel Order</h5>
    <a  class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </a>
</div>
<div class="modal-body">
	<form action="<?php echo e(url('account/cancel-order/save')); ?>" id="CancelFrm" method="post" class="form-horizontal style-form" enctype="multipart/form-data">
	    <?php echo csrf_field(); ?>
	    <input type="hidden" name="id" value="<?php echo e(encrypt($order_id)); ?>">
	    <div class="form-group">
	      	<label>Cancel Type</label>
	        <select class="form-control select2_input w-100" data-placeholder="Select Cancel Reason" name="cancel_reason" id="cancelReasonInput" data-parent="#common-modal .modal-body">
	        	<?php if(count($cancel_reasons)>0): ?>
	        		<?php $__currentLoopData = $cancel_reasons; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reason): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	        			<option value="<?php echo e($reason->title); ?>"><?php echo e($reason->title); ?></option>
	        		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	        	<?php endif; ?>
	        	<option value="Other">Other</option>
	        </select>
	        <span id="type_error"></span>
	    </div>
	    <div class="form-group" style="display: none;" id="specify-reason-section">
	      	<label>Specify Reason</label>
	        <textarea class="form-control" name="other_reason" id="other_reason"></textarea>
	        <span id="address1_error"></span>
	    </div>
	    <div class="form-group">
            <button class="btn btn-primary" type="button" id="cancel-reason-save-btn" data-id="<?php echo e($order_id); ?>">Save</button>
       	</div>
	</form>
</div><?php /**PATH C:\wamp\www\pittappillil\resources\views/client/orders/cancel_order.blade.php ENDPATH**/ ?>