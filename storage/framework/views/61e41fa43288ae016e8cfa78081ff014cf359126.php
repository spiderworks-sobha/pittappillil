<?php $__env->startSection('head'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="col-md-12 mb-20"  align="right" style="margin-bottom: 20px; ">
          <span class="page-heading"><?php if($obj->id): ?>Edit Offer <?php else: ?> Add new Offer <?php endif; ?></span>
          <div >
              <div class="btn-group">
                  <a href="<?php echo e(url('admin/offers')); ?>" class="btn btn-success"><i class="fa fa-list"></i> List Offers</a>
                  <?php if($obj->id): ?>
                    <a href="<?php echo e(url('admin/offers/create')); ?>" class="btn btn-success"><i class="fa fa-pencil"></i> Create new</a>
                    <a href="<?php echo e(url('admin/offers/destroy', [encrypt($obj->id)])); ?>" class="btn btn-success btn-warning-popup" data-message="Are you sure to delete?  Associated data will be removed if it is deleted." data-redirect-url="<?php echo e(url('admin/offers')); ?>"><i class="fa fa-trash"></i> Delete</a>
                  <?php endif; ?>
              </div>
          </div>
        </div>
        <div class="col-lg-12">
            <div class="card card-borderless">
                <?php if($obj->id): ?>
                    <?php echo e(Form::open(['url' => route('admin.offers.update'), 'method' => 'post','enctype' => 'multipart/form-data', 'id'=>'OfferFrm'])); ?>

                    <input type="hidden" name="id" value="<?php echo e(encrypt($obj->id)); ?>" id="inputId">
                <?php else: ?>
                    <?php echo e(Form::open(['url' => route('admin.offers.store'), 'method' => 'post','enctype' => 'multipart/form-data', 'id'=>'OfferFrm'])); ?>

                <?php endif; ?>
                <ul class="nav nav-tabs nav-tabs-simple d-none d-md-flex d-lg-flex d-xl-flex" role="tablist" data-init-reponsive-tabs="dropdownfx">
                    <li class="nav-item">
                        <a class="active show" data-toggle="tab" role="tab"
                           data-target="#tab1Basic"
                        href="#" aria-selected="true">Basic Details</a>
                    </li>
                    <li class="nav-item">
                        <a data-toggle="tab" role="tab"
                           data-target="#tab2Basic"
                        href="#" aria-selected="true">SEO</a>
                    </li>
                    <li class="nav-item">
                        <a data-toggle="tab" role="tab"
                           data-target="#tab3Basic"
                        href="#" aria-selected="true">Offer Management</a>
                    </li>
                </ul>
                
                <div class="tab-content">
                    <div class="tab-pane active show" id="tab1Basic">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default form-group-default-select2 required">
                                        <label>Status</label>
                                        <?php echo Form::select('is_active', array('1'=>'Enabled', '0'=>'Disabled'), (!$obj->id || $obj->is_active)?1:0, array('class'=>'full-width select2-dropdown', 'id'=>'inputStatus'));; ?>

                                    </div>
                                    <span class="error"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default required">
                                        <label>Offer Name</label>
                                        <?php echo e(Form::text("offer_name", $obj->offer_name, array('class'=>'form-control', 'id' => 'offer_name','required' => true))); ?>

                                    </div>
                                    <span class="error"></span>
                                </div>
                            </div>
                            <div class="col-md-6 no-padding">
                                    <div class="row column-seperation padding-5">
                                        <div class="form-group form-group-default form-group-default-select2 required">
                                            <label>Offer Type</label>
                                            <?php echo Form::select('type', array('Price'=>'Price', 'Combo'=>'Combo', 'Group'=>'Group', 'Free'=>'Free'), $obj->type, array('class'=>'full-width select2-dropdown', 'id'=>'inputType'));; ?>

                                        </div>
                                    </div>
                            </div>
                            <?php if(config('common.multi_vendor')): ?>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default form-group-default-select2">
                                        <label class="">Vender</label>
                                        <?php
                                          $venders = [];
                                          if($obj->vender_id)
                                            $venders = [$obj->vender_id => $obj->vendor->vendor_name];
                                        ?>
                                        <?php echo Form::select('vender_id',$venders, $obj->vender_id, array('data-placeholder'=>'Choose a vender','data-init-plugin'=>'select2','data-select2-url'=>route('select2.venders'),'class'=>'full-width select2_input', 'id'=>'vender_id'));; ?>

                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default input-group required">
                                      <div class="form-input-group">
                                        <label>Offer Start Date</label>
                                        <?php
                                          $validity_start_date = ($obj->validity_start_date)?date('d-m-Y', strtotime($obj->validity_start_date)):null;
                                        ?>
                                        <?php echo e(Form::text("validity_start_date", $validity_start_date, array('class'=>'form-control datepicker', 'id' => 'validity_start_date'))); ?>

                                      </div>
                                      <div class="input-group-append ">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                      </div>
                                    </div>
                                    <span class="error"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default input-group required">
                                      <div class="form-input-group">
                                        <label class="">Offer End Date</label>
                                        <?php
                                          $validity_end_date = ($obj->validity_end_date)?date('d-m-Y', strtotime($obj->validity_end_date)):null;
                                        ?>
                                        <?php echo Form::text('validity_end_date',$validity_end_date, array('class'=>'form-control datepicker', 'id'=>'validity_end_date'));; ?>

                                      </div>
                                      <div class="input-group-append ">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                      </div>
                                    </div>
                                    <span class="error"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab2Basic">
                        <div class="row">
                            <div class="col-md-6">
                              <div class="row column-seperation padding-5">
                                  <div class="form-group form-group-default">
                                      <label>Browser Title</label>
                                      <?php echo e(Form::text("browser_title", $obj->browser_title, array('class'=>'form-control', 'id' => 'browser_title'))); ?>

                                  </div>
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="row column-seperation padding-5">
                                  <div class="form-group form-group-default">
                                      <label>Meta Tags</label>
                                      <?php echo e(Form::textarea("meta_keywords", $obj->meta_keywords, array('class'=>'form-control', 'id' => 'meta_keywords'))); ?>

                                  </div>
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="row column-seperation padding-5">
                                  <div class="form-group form-group-default">
                                      <label>Meta Description</label>
                                      <?php echo e(Form::textarea("meta_description", $obj->meta_description, array('class'=>'form-control', 'id' => 'meta_description'))); ?>

                                  </div>
                              </div>
                          </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab3Basic">
                      <div class="row offer-manager-sec" id="price-wrapper" <?php if($obj->id && $obj->type != "Price"): ?> style="display: none;" <?php endif; ?>>
                        <?php echo $__env->make('admin/offers/offer_manage/price', ['obj'=>$obj], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                      </div>
                      <div class="row offer-manager-sec" id="combo-wrapper" <?php if($obj->type != "Combo"): ?> style="display: none;" <?php endif; ?>>
                        <?php echo $__env->make('admin/offers/offer_manage/combo', ['obj'=>$obj], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                      </div>
                      <div class="row offer-manager-sec" id="group-wrapper" <?php if($obj->type != "Group"): ?> style="display: none;" <?php endif; ?>>
                        <?php echo $__env->make('admin/offers/offer_manage/group', ['obj'=>$obj], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                      </div>
                      <div class="row offer-manager-sec" id="free-wrapper" <?php if($obj->type != "Free"): ?> style="display: none;" <?php endif; ?>>
                        <?php echo $__env->make('admin/offers/offer_manage/free', ['obj'=>$obj], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                      </div>
                      <div class="row offer-wrapper" <?php if(($obj->type == 'Price' && $obj->applicable_for_full_order == 1) || ($obj->type == 'Price' && count($obj->categories)>0) || $obj->type == 'Group'): ?> style="display:none;" <?php endif; ?>>
                          <div class="col-md-6">
                            <div class="card card-default">
                              <div class="card-header">
                              Products
                              <div class="row float-right">
                                <a href="javascript:void(0)" class="btn btn-success btn-sm" title="Filter Products" data-toggle="modal" data-target="#filterModal"><i class="fa fa-filter"></i></a>
                                <div>
                                  <a href="javascript:void(0)" class="btn btn-sm btn-success" id="source-btn" style="<?php if(($obj->type == 'Free' && $obj->applicable_for_full_order == 1) || ($obj->type == 'Free' && count($obj->categories)>0)): ?> display: none; <?php endif; ?> margin-left:10px;">Move to Offers <i class="fa fa-angle-double-right"></i></a>
                                </div>
                                <div>
                                  <a href="javascript:void(0)" class="btn btn-sm btn-success" id="free-source-btn" style="<?php if($obj->type != 'Free'): ?> display: none; <?php endif; ?> margin-left:10px;">Move to Free <i class="fa fa-angle-double-right"></i></a>
                                </div>
                                <div>
                                  <a href="javascript:void(0)" class="btn btn-sm btn-success" id="combo-free-source-btn" style="<?php if($obj->type != 'Combo' || ($obj->type == 'Combo' && count($obj->free_products)<=0)): ?> display: none; <?php endif; ?> margin-left:10px;">Move to Combo <i class="fa fa-angle-double-right"></i></a>
                                </div>
                              </div>
                              </div>
                              <div class="card-body">
                                <?php
                                  $selected_items = [];
                                  if($obj->price_offers)
                                  {
                                    $offer_products = $obj->price_offers->pluck('products_id')->toArray();
                                    $selected_items = array_merge($selected_items, $offer_products);
                                  }
                                  if($obj->combo_offers)
                                  {
                                    $combo_products = $obj->combo_offers->pluck('products_id')->toArray();
                                    $selected_items = array_merge($selected_items, $combo_products);
                                  }
                                  if($obj->free_products)
                                  {
                                    $free_products = $obj->free_products->pluck('products_id')->toArray();
                                    $selected_items = array_merge($selected_items, $free_products);
                                  }

                                ?>
                                <ul class="list-group" id="source">
                                  <?php echo $__env->make('admin/offers/ajax_list', ['products'=>$products, 'selected_items'=>$selected_items], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                </ul>
                                <div class="checkbox check-success">
                                  <input type="checkbox" value="1" id="all-select">
                                  <label for="all-select">Select All</label>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="card card-default" id="offer-product-wrapper" <?php if(($obj->type == 'Free' && $obj->applicable_for_full_order == 1) || ($obj->type == 'Free' && count($obj->categories)>0)): ?> style="display:none;" <?php endif; ?>>
                              <div class="card-header">
                                Selected Products
                                <div class="pull-right">
                                  <a href="javascript:void(0)" class="btn btn-sm btn-success" id="destination-btn"><i class="fa fa-angle-double-left"></i> Remove from Offer</a>
                                  <input type="hidden" name="offer_check" id="offer_check" <?php if($selected_items): ?> value="1" <?php endif; ?>>
                                  <span class="error"></span>
                                </div>
                              </div>
                              <div class="card-body">
                                <ul class="list-group" id="destination">
                                    <?php if($obj->id && ($obj->type == 'Price' || $obj->type == 'Combo' || $obj->type == 'Free')): ?>
                                      <?php if($obj->type == 'Price' && count($obj->price_offers)>0): ?>
                                        <?php $__currentLoopData = $obj->price_offers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $price_offer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                          <li class="list-group-item"  id="<?php echo e($price_offer->product->id); ?>">
                                            <label><?php echo e($price_offer->product->name); ?></label>
                                            <input type="hidden" name="offer_products[]" value="<?php echo e($price_offer->product->id); ?>">
                                          </li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                      <?php elseif(count($obj->combo_offers)>0): ?>
                                        <?php $__currentLoopData = $obj->combo_offers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $combo_offer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                          <li class="list-group-item"  id="<?php echo e($combo_offer->product->id); ?>">
                                            <label><?php echo e($combo_offer->product->name); ?></label>
                                            <input type="hidden" name="offer_products[]" value="<?php echo e($combo_offer->product->id); ?>">
                                          </li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                      <?php endif; ?>
                                    <?php endif; ?>
                                </ul>
                              </div>
                            </div>
                            <div class="card card-default" id="free-product-wrapper" <?php if($obj->type != 'Free'): ?> style="display: none;" <?php endif; ?>>
                              <div class="card-header">
                                Free Products
                                <div class="pull-right">
                                  <a href="javascript:void(0)" class="btn btn-sm btn-success" id="free-destination-btn"><i class="fa fa-angle-double-left"></i> Remove from Free</a>
                                  <input type="hidden" name="free_check" id="free_check" <?php if($obj->type == 'Free' && $obj->free_products): ?> value="1" <?php endif; ?>>
                                  <span class="error"></span>
                                </div>
                              </div>
                              <div class="card-body">
                                <ul class="list-group" id="free-destination">
                                  <?php if($obj->type == 'Free' && $obj->free_products): ?>
                                    <?php $__currentLoopData = $obj->free_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $free_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <li class="list-group-item"  id="<?php echo e($free_product->product->id); ?>">
                                          <label><?php echo e($free_product->product->name); ?></label>
                                          <input type="hidden" name="free_products[]" value="<?php echo e($free_product->product->id); ?>">
                                      </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                  <?php endif; ?>
                                </ul>
                              </div>
                            </div>
                            <div class="card card-default" id="combo-free-product-wrapper" <?php if($obj->type != 'Combo' || ($obj->type == 'Combo' && count($obj->free_products)<=0)): ?> style="display: none;" <?php endif; ?>>
                              <div class="card-header">
                                Offer Products
                                <div class="pull-right">
                                  <a href="javascript:void(0)" class="btn btn-sm btn-success" id="combo-free-destination-btn"><i class="fa fa-angle-double-left"></i> Remove from Free</a>
                                  <input type="hidden" name="combo_free_check" id="combo_free_check" <?php if($obj->type == 'Combo' && count($obj->free_products)>0): ?> value="1" <?php endif; ?>>
                                  <span class="error"></span>
                                </div>
                              </div>
                              <div class="card-body">
                                <ul class="list-group" id="combo-free-destination">
                                  <?php if($obj->type == 'Combo' && $obj->free_products): ?>
                                    <?php $__currentLoopData = $obj->free_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $free_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <li class='list-group-item' id='<?php echo e($free_product->product->id); ?>'>
                                        <div>
                                          <label><?php echo e($free_product->product->name); ?></label>
                                          <div class="row column-seperation padding-5 combo-free-checkbox">
                                            <div class="checkbox check-success">
                                              <input type="checkbox" value="1" name="free[]" id="checkbox-<?php echo e($free_product->product->id); ?>" <?php if($free_product->type == 'Free'): ?> checked="checked" <?php endif; ?>><label for="checkbox-<?php echo e($free_product->product->id); ?>">Free</label>
                                            </div>
                                          </div>
                                          <div <?php if($free_product->type == 'Free'): ?> style="display: none;" <?php endif; ?> class="combo-free-discount">
                                            <div class="row column-seperation padding-5">
                                              <div class="form-group form-group-default form-group-default-select2"><label>Discount Type</label>
                                                <?php
                                                  $free_discount_type = (!$free_product->type || $free_product->type == 'Free')?'Discount Percentage':$free_product->type;
                                                ?>
                                                <select class="full-width select2-dropdown combo-free-discount-type" name="free_discount_type[]" >
                                                  <option value="Discount Percentage" <?php if($free_product->type == 'Discount Percentage'): ?> selected="selected" <?php endif; ?>>Discount Percentage</option>
                                                  <option value="Fixed Price" <?php if($free_product->type == 'Fixed Price'): ?> selected="selected" <?php endif; ?>>Fixed Price</option>
                                                  <option value="Discount Price" <?php if($free_product->type == 'Discount Price'): ?> selected="selected" <?php endif; ?>>Discount Price</option>
                                                </select>
                                              </div>
                                            </div>
                                            <div class="row column-seperation padding-5 combo-free-discount-amount" <?php if($free_discount_type == 'Discount Percentage'): ?> style="display:none;" <?php endif; ?>>
                                              <div class="form-group form-group-default"><label>Amount</label>
                                                <input class="form-control amount" name="free_discount_amount[]" type="text"  <?php if($free_product->type == 'Fixed Price'): ?> value="<?php echo e($free_product->fixed_price); ?>" <?php elseif($free_product->type == 'Discount Price'): ?> value="<?php echo e($free_product->discount_amount); ?>" <?php endif; ?>>
                                              </div>
                                            </div>
                                            <div class="row column-seperation padding-5 combo-free-discount-percentage" <?php if($free_discount_type != 'Discount Percentage'): ?> style="display:none;" <?php endif; ?>>
                                              <div class="form-group form-group-default required"><label>Percentage</label>
                                                <input class="form-control numeric" name="free_discount_percentage[]" type="text" value="<?php echo e($free_product->discount_percentage); ?>">
                                              </div>
                                            </div>
                                            <div class="row column-seperation padding-5">
                                              <div class="form-group form-group-default required"><label>Maximum discount amount</label>
                                                <input class="form-control numeric" name="free_max_discount_amount[]" type="text" value="<?php echo e($free_product->max_discount_amount); ?>">
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <input type='hidden' name='combo_free_products[]' value='<?php echo e($free_product->product->id); ?>'/>
                                      </div>
                                    </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                  <?php endif; ?>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" align="right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
                <?php echo e(Form::close()); ?>

            </div>
        </div>
    </div>
    <?php echo $__env->make('admin.product_filter_modal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom'); ?>
    <script src="<?php echo e(asset('js/offer.js')); ?>"></script>
    ##parent-placeholder-c03e9099aad17cb58e4fff1d93d751105735c9c2##
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.common.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/pittappillil/resources/views/admin/offers/form.blade.php ENDPATH**/ ?>