window.fbAsyncInit = function() {
    // FB JavaScript SDK configuration and setup
    FB.init({
        appId      : appid, // FB App ID
        cookie     : true,  // enable cookies to allow the server to access the session
        xfbml      : true,  // parse social plugins on this page
        version    : 'v3.1' // use graph api version 2.8
    });

    // Check whether the user already logged in
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            //display user data
            getFbUserData();
        }
    });
};

// Load the JavaScript SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Facebook login with JavaScript SDK
function fbLogin() {
    FB.login(function (response) {
        if (response.authResponse) {
            // Get and display the user profile data
            getFbUserData();
        } else {
            document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';
        }
    }, {scope: 'email'});
}

// Fetch the user profile data from facebook
function getFbUserData(){
    FB.api('/me', {locale: 'en_US', fields: 'id,name,email,link,gender,locale,picture'},
        function (response) {
            console.log(response);
            $.post(login_callback, {provider:'facebook',data: response, _token:csrf}, function(data){
                location.reload(true);
            });
        });
}

function onLoadGoogleCallback(){
    gapi.load('auth2', function() {
        auth2 = gapi.auth2.init({
            client_id: client_id,
            cookiepolicy: 'single_host_origin',
            scope: 'profile'
        });

        auth2.attachClickHandler(element, {},
            function(googleUser) {
                var profile = googleUser.getBasicProfile();
                var data = { "id" : profile.getId(), "name"  : profile.getName(), "email" : profile.getEmail() };
                $.post(login_callback, {provider:'google',data: data, _token:csrf}, function(data){
                    location.reload(true);
                });

            }, function(error) {
                document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';
            }
        );

        auth2.attachClickHandler(element_signup, {},
            function(googleUser) {
                var profile = googleUser.getBasicProfile();
                var data = { "id" : profile.getId(), "name"  : profile.getName(), "email" : profile.getEmail() };
                $.post(login_callback, {provider:'google',data: data, _token:csrf}, function(data){
                    location.reload(true);
                });

            }, function(error) {
                document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';
            }
        );

    });

    element = document.getElementById('googleSignIn');
    element_signup = document.getElementById('googleSignUp');


}