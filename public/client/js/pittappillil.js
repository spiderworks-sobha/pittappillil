svg4everybody();
//$(function(){
$('#loginForm').validate({
    rules:
        {
            login:
                {
                    required:true,
                },
            password:
                {
                    required:true
                }
        },
    messages:
        {
            email:
                {
                    required:"Please enter an email address.",
                    email:"Please enter a valid email address."
                },
            password:
                {
                    required:"Please enter a password."
                }
        },
    errorPlacement: function (error, element) {
        error.insertAfter($(element).parent('div'));
    },
    submitHandler:function(form)
    {
        $('#login-btn').text('Processing...');
        $('#login-btn').prop('disabled', true);
        var formurl = $('#loginForm').attr('action');
        $.ajax({
            url: formurl ,
            type: "POST",
            data: new FormData(form),
            cache: false,
            processData: false,
            contentType: false,
            success: function(data) {
                location.reload();
            },
            error:function(xhr){
                $('#login-btn').text('Login');
                $('#login-btn').prop('disabled', false);
                var response = $.parseJSON(xhr.responseText);
                if(response.errors) {
                    if(response.errors['email'][0] != "undefined")
                        $('.login-errors').html(response.errors['email'][0]);
                    else
                        $('.login-errors').html(response.message);
                }
            }
        });
    }
});




$('#registerForm').validate({
    ignore: [],
    rules:{
        first_name:{
            required:true
        },
        phone_email:{
            required:true
        },
        email:{
            email:true,
        },
        username:{
            digits:true
        },
        password:{
            required:true
        },
    },
    messages:{
        first_name:{
            required:"First name cannot be blank"
        },
        phone_email:{
            required:"Please enter your email address or phone number",
        },
        email:{
            email:"Please enter a valid email address"
        },
        username:{
            digits:"Please enter a valid phone number",
        },
        password:{
            required:"Please enter a password"
        }
    },
    submitHandler:function(form){
        $('#register-btn').text('Processing...');
        $('#register-btn').prop('disabled', true);
        var formurl = $('#registerForm').attr('action');
        $.ajax({
            url: formurl ,
            type: "POST",
            data: new FormData(form),
            cache: false,
            processData: false,
            contentType: false,
            success: function(data) {
                var response = $.parseJSON(data);
                if (typeof response.errors != "undefined") {
                    $('#register-btn').text('Register');
                    $('#register-btn').prop('disabled', false);
                    var errors = JSON.parse(JSON.stringify(response.errors));
                    console.log(errors);
                    $.each(errors, function (key, val) {
                        $(".register-errors").html(val);
                    });
                }
                else{
                    location.reload();
                }
            },
            error:function(xhr){
                $('#register-btn').text('Register');
                $('#register-btn').prop('disabled', false);
                var response = $.parseJSON(xhr.responseText);
                if(response.errors) {
                    if(response.errors['email'][0] != "undefined")
                        $('.register-errors').html(response.errors['email'][0]);
                    else if(response.errors['username'][0] != "undefined")
                        $('.register-errors').html(response.errors['username'][0]);
                    else
                        $('.register-errors').html(response.message);
                }
            }
        });
    }
});

$('#forgotPasswordForm').validate({
        rules:
        {
            email:
            {
                required:true,
                email:true
            },
        },
        messages:
        {
            email:
            {
                required:"Please enter an email address.",
                email:"Please enter a valid email address."
            },
        },
        submitHandler:function(form)
        {
            $('.reset-btn').html('SENDING...');
                var formurl = $('#forgotPasswordForm').attr('action');
                $.ajax({
                    url: formurl , 
                    type: "POST", 
                    data: new FormData(form),
                    cache: false, 
                    processData: false,
                    contentType: false, 
                    success: function(data) {
                      if(typeof data.success != "undefined")
                      {
                        $('#forgot-password').modal('hide');
                        Toast.fire({
                            title: 'Success!',
                            text: 'A password reset link sent to your mail id, please check.',
                            icon: 'success',
                        });
                      }
                      else
                      {
                        $('.reset-btn').html('Send Password Reset Link');
                        $('#forgot-password-error').show().text('Please check email Id');
                      }
                    },
                    error:function(xhr){
                      $('.reset-btn').html('Send Password Reset Link');
                      var response = $.parseJSON(xhr.responseText);
                      if(response.errors) {
                        if(response.errors['email'][0] != "undefined")
                          alert(response.errors['email'][0]);
                        else
                          alert(response.message);
                      }
                    }
                });
        }
    });

$('#newsletterFrm').validate({
    rules:
        {
            email:
                {
                    required: true,
                    email: true,
                }
        },
    messages:
        {
            email:
                {
                    required:"Please enter an email address.",
                    email:"Please enter a valid email address."
                },
        },
    errorPlacement: function (error, element) {
        $("#email_error").html('');
        $(element).parent('form').next('.error').remove();
        error.addClass('text-danger').insertAfter($(element).parent('form'));
    },
    submitHandler:function(form)
    {
        $('#subscribe-btn').text('Processing...');
        $('#subscribe-btn').prop('disabled', true);
        var formurl = $('#newsletterFrm').attr('action');
        $.ajax({
            url: formurl ,
            type: "POST",
            data: new FormData($('#newsletterFrm')[0]),
            cache: false,
            processData: false,
            contentType: false,
            success: function(data) {
                $('#subscribe-btn').text('Subscribe');
                $('#subscribe-btn').prop('disabled', false);
                $('.footer-newsletter').find('#footer-newsletter-address-error').remove();
                if (typeof data.errors != "undefined") {
                    var errors = JSON.parse(JSON.stringify(data.errors))
                    $.each(errors, function (key, val) {
                        $("#email_error").html(val);
                    });
                }
                else
                {
                    $("#email_error").html('');
                    $('#footer-newsletter-address').val('');
                    Toast.fire({
                        title: 'Success!',
                        text: 'You have successfully subscribed!',
                        icon: 'success',
                    });
                }
            },
            error:function(xhr){
                $('#subscribe-btn').text('Subscribe');
                $('#subscribe-btn').prop('disabled', false);
                var errors = $.parseJSON(xhr.responseText);
                $.each(errors, function (key, val) {
                    $("#" + key + "_error").text(val[0]);
                });
            }
        });
    }
});

//})
function validateAddress()
{
    $('#AddressFrm').validate({
        rules:
            {
                full_name: "required",
                mobile_number: {
                    required : true,
                    digits  : true,
                },
                pincode: {
                    required : true,
                    digits  : true,
                },
                address1: "required",
                city: "required",
                state: "required",
                type: "required",
            },
        messages:
            {
                full_name: "Name cannot be blank",
                mobile_number: {
                    required: "Mobile number cannot be blank",
                },
                pincode: {
                    required: "Pincode cannot be blank",
                },
                address1: "Address cannot be blank",
                city: "City cannot be blank",
                state: "Select a state",
                type: "Select an address type",
            },
        errorPlacement: function (error, element) {
            error.insertAfter($(element).parent('div'));
        }
    });
}

$(document).on('click', '#open-register', function(){
    $('.reg-cntr').show();
    $('.login-cntr').hide();
    $('#login-register').modal('show');
});

$(document).on('click', '#open-login', function(){
    $('.login-cntr').show();
    $('.reg-cntr').hide();
    $('#login-register').modal('show');
});

$(document).on('click', '.open-forgot', function(e){
    e.preventDefault();
    $('#login-register').modal('hide');
    $('#forgot-password').modal('show');
});

$(document).on('keyup', '#phone_email', function(){
    ep_emailval = $('#phone_email').val();
    var intRegex = /^\d+$/;
    if(intRegex.test(ep_emailval)) {
        $('#phone-holder').show();
        $('#hidden_phone').val(ep_emailval);
        $('#hidden_email').val('');
        $(this).attr('maxLength', 10);
    }
    else
    {
        $('#phone-holder').hide();
        $('#hidden_email').val(ep_emailval);
        $('#hidden_phone').val('');
        $(this).attr('maxLength', 50);
    }
});

$(document).on('click', '.show-modal', function(e){
    e.preventDefault();
    var url = $(this).attr("href");
    console.log(url);
    var target = $(this).data('target');
    $(target).find(".modal-content").load(url, function (){
        $(target).modal('show');
        display_select2();
    });
});

$("#common-modal").on("show.bs.modal", function(e) {
    var link = $(e.relatedTarget);

});

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
});

$(document).on('click', '#address-save-btn', function(){
    var obj = $(this);
    validateAddress();
    var frmValid = $('#AddressFrm').valid();
    if(frmValid)
    {
        obj.prop('disabled', true);
        obj.html('Saving..');
        var formurl = $('#AddressFrm').attr('action');
        $.ajax({
            url: formurl,
            type: "POST",
            data: new FormData($('#AddressFrm')[0]),
            cache: false,
            processData: false,
            contentType: false,
            success: function(data) {
                obj.prop('disabled', false);
                obj.html('Save');

                if (typeof data.errors != "undefined") {
                    var errors = JSON.parse(JSON.stringify(data.errors))
                    $.each(errors, function (key, val) {
                        $("#" + key + "_error").text(val[0]);
                    });
                }
                else
                {
                    $('#common-modal').modal('hide');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    if(data.location == 'home')
                    {
                        $('#address-home-new-add').hide();
                        $('#address-home-display').show().html(data.html);
                    }
                    else{
                        if(data.is_edit != null)
                        {
                            $('#address-list-item-'+data.is_edit).html(data.html);
                        }
                        else{
                            var html = '<div class="addresses-list__divider"></div>';
                            html += '<div class="addresses-list__item card address-card">';
                            html += data.html;
                            html += '</div>';
                            $(html).insertAfter($('#add-new-address-btn'));
                        }
                    }
                    Toast.fire({
                        title: 'Success!',
                        text: 'Address saved successfully!',
                        icon: 'success',
                    });
                }
            },
            error:function(reject){
                obj.prop('disabled', false);
                obj.html('Save');
                if( reject.status === 422 ) {
                    var errors = $.parseJSON(reject.responseText);
                    $.each(errors, function (key, val) {
                        $("#" + key + "_error").text(val[0]);
                    });
                }
            }
        });
    }
});

var search_type = "stores/search";
var search_slug = "";

$(document).ready(function() {

    $("#reg-btn").click(function(){
        $(".login-cntr").css("display","none");
        $(".reg-cntr").css("display","flex");
    });

    $("#log-btn").click(function(){
        $(".login-cntr").css("display","flex");
        $(".reg-cntr").css("display","none");
    });

    $(".toggle-password").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });



    $( "#search" ).autocomplete({

        source: function(request, response) {
            $.ajax({
                url: url_autocomplete,
                data: {
                    term : request.term
                },
                dataType: "json",
                success: function(data){
                    var resp = $.map(data,function(obj){
                        return {
                            label: obj.name,
                            value: obj.id,
                            type: obj.type,
                        }
                    });

                    response(resp);
                }
            });
        },
        select: function (event, ui) {
            search_type = ui.item.type;
            search_slug = ui.item.value;
            $('#search').val(ui.item.label)
            do_search();
        },
        minLength: 1
    });


    $(document).on('keyup', '#search', function(e){
        if(e.which === 13){
            do_search();
        }
        else{
            search_type = "stores/search";
            search_slug = "";
        }
    });

    $(document).on('click', '#GlobalSearchBtn', function(){
        do_search();
    });
});

function do_search()
{
    var keyword = $('#search').val();
    var url = baseUrl;
    if(search_type == 'product')
    {
        url += '/'+search_slug;
    }
    else{
        url += '/'+search_type;
        if(search_slug != '')
            url += '/'+search_slug;
    }
    url += '?keyword='+keyword;
    window.location = url;
}
